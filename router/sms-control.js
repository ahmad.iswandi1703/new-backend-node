var express=require('express');

var router=express.Router();


const smsController=require('../controllers/sms-control');

//get sms broadcast data;
//router.get('/broadcast',smsController.getSmsBroadcast);

router.post('/report',smsController.getReport);


router.post('/print-report',smsController.download);

module.exports=router;