var expres=require('express');

var router=expres.Router();


const smsReportController=require('../controllers/sms-report-controller');


router.post('/get',smsReportController.getReport);

router.post('/download',smsReportController.download);
router.post('/list-broadcast',smsReportController.getBroadcast);

module.exports=router;