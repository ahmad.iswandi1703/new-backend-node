var express=require('express');

var router=express.Router();


const emaiController=require('../controllers/email/email-controller');
const emaiReport=require('../controllers/email/email-report-controller');
//get sms broadcast data;
//router.get('/broadcast',smsController.getSmsBroadcast);
router.post('/subscriber',emaiController.getEmailSubscriber);
router.post('/broadcast',emaiController.getEmailBroadcast);
router.get('/delete-broadcast/:broadcast_id',emaiController.deleteEmailBroadcast);

///report Section
router.post('/get',emaiReport.getReport);
router.post('/download',emaiReport.download);
router.post('/list-broadcast',emaiReport.getBroadcast);

var EmailTmplCtrl=require('../controllers/email/email-template-setting.controller');
//add template
router.post('/template',EmailTmplCtrl.addEmailTemplate);
router.post('/all-template',EmailTmplCtrl.queryPagingEmailTemplate);

router.get('/get-template/:template_id',EmailTmplCtrl.getEmailTemplate);
router.post('/edit-template',EmailTmplCtrl.editTemplate);
router.get('/delete-template/:template_id', EmailTmplCtrl.deleteTemplate);
//edit
//delete
router.get('/subscriber-by-template/:email_template_id',EmailTmplCtrl.getSubscriberByTemplate);
router.post('/add-subscriber',EmailTmplCtrl.addSubscriber);
router.post('/edit-subscriber',EmailTmplCtrl.editSubscriber);
router.get('/template-per-user/:user_id',EmailTmplCtrl.getTemplateBySubscriber);
module.exports=router;