var express=require('express');

var router=express.Router();


var SettingCtrl=require('../controllers/setting-controller');
var SettingBySubscriber=require('../controllers/call/call-setting-subscriber');
router.post('/set',SettingCtrl.setSetting);

///get
router.post('/all-master-setting',SettingBySubscriber.queryPagingCallSetting);


router.get('/subscriber-by-setting/:call_setting_id',SettingBySubscriber.getSubscriberByTemplate);
router.post('/add-subscriber',SettingBySubscriber.setSetting);
router.post('/edit-subscriber',SettingBySubscriber.editSubscriber);


module.exports=router;