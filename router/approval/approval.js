var express=require('express');

var router=express.Router();
const apCtrl=require('../../controllers/approval/approval-scheme-controller');
const task=require('../../controllers/approval/approval-task');

const request=require('../../controllers/approval/approval-request');

// router.get('/get-list-schema');
// router.post('/schema-paging');
router.post('/add-schema',apCtrl.addSchema);


//approving
router.post('/task-paging',task.requestPaging);
router.get('/approving-task/:task_id/:status',task.approving);

//request
//for query pagin
router.post('/request-paging',request.requestPaging);
router.post('/edit-request',request.editRequest);
router.get('/delete-request/:request_id',request.deleteRequest);
router.post('/add-request',request.addRequest);

//
router.get('/schema-list',request.getSchema);


module.exports=router;