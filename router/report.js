var express=require('express');

var router=express.Router();


var ReportCtrl=require('../controllers/survey-report');
router.post('/survey',ReportCtrl.download);

router.post('/call-detail-records',ReportCtrl.getCallRecords);
router.post('/cdr-print',ReportCtrl.downloadCallDetail);





module.exports=router;