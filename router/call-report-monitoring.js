var express=require('express');

var router=express.Router();

var CRMonitoring=require('../controllers/call/call-monitoring-report');
router.get('/office-branch/:userID',CRMonitoring.getOfficeBranch);
router.post('/office-users',CRMonitoring.getBroadcastByAllUser);
router.post('/office-user-download',CRMonitoring.downloadBroadcastByAllUser);
router.post('/get-report-survey',CRMonitoring.getReportSurvey);

router.post('/survey-download',CRMonitoring.download);
module.exports=router;