var express=require('express');

var router=express.Router();


var SmsTmplCtrl=require('../controllers/sms/sms-template-setting-controller');
//add template
router.post('/template',SmsTmplCtrl.addSmsTemplate);
router.post('/all-template',SmsTmplCtrl.queryPagingSmsTemplate);

router.get('/get-template/:template_id',SmsTmplCtrl.getSmsTemplate);
router.post('/edit-template',SmsTmplCtrl.editTemplate);
router.get('/delete-template/:template_id', SmsTmplCtrl.deleteTemplate);
//edit
//delete
router.get('/subscriber-by-template/:sms_template_id',SmsTmplCtrl.getSubscriberByTemplate);
router.post('/add-subscriber',SmsTmplCtrl.addSubscriber);
router.post('/edit-subscriber',SmsTmplCtrl.editSubscriber);
router.get('/template-per-user/:user_id',SmsTmplCtrl.getTemplateBySubscriber);


module.exports=router;