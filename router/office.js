var express=require('express');

var router=express.Router();


var OfficeCtrl=require('../controllers/office-controller');
router.post('/create-office',OfficeCtrl.createOffice);

router.post('/edit-office',OfficeCtrl.editOffice);

router.post('/delete-office',OfficeCtrl.deleteOffice);

router.post('/all-office',OfficeCtrl.getOffice);

router.get('/get-detail/:uid',OfficeCtrl.getDetailOffice);

module.exports=router;