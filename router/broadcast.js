var express=require('express');

var router=express.Router();


var BroadcastCtrl=require('../controllers/broadcast-controller');
router.post('/group',BroadcastCtrl.getContactGroup);
router.post('/list-group',BroadcastCtrl.getContactGroupAll);

router.post('/contact-list',BroadcastCtrl.getContactList);

router.post('/send',BroadcastCtrl.sendCall);
router.post('/get',BroadcastCtrl.getBroadcast);

router.post('/broadcast-detail',BroadcastCtrl.getBroadcastDetail);
router.post('/detail-records',BroadcastCtrl.getDetailRecords);
router.post('/survey-report',BroadcastCtrl.surveyReport);

router.get('/change-state/:broadcast_id/:status',BroadcastCtrl.changeStatusScheduler);



module.exports=router;