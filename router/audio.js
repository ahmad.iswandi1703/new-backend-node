var express=require('express');

var router=express.Router();
const audio=require('../controllers/audio-stream-controller');

router.get('/stream/:audioName',audio.audio);


router.post('/list',audio.getAudio);
router.get('/common/audio_file',audio.getCommonAudio);


module.exports=router;