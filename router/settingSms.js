var express=require('express');

var router=express.Router();


var SettingCtrl=require('../controllers/Sms-setting.controller');
var SettingBilling=require('../controllers/sms/sms-billing-controller');
router.post('/set',SettingCtrl.setSetting);
//
router.post('/set-billing',SettingBilling.setSetting);
router.get('/get-billing-info/:user_id',SettingBilling.getBillingHistory);

module.exports=router;