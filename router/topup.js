var express=require('express');

var router=express.Router();

const topUpCtrl=require("../controllers/topup/topup");

router.get('/topup/:user_id/:type',topUpCtrl.getQuotaInfo);
router.post('/topup',topUpCtrl.topup);


module.exports=router;