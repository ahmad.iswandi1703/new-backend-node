var express=require('express');
var app=express();
var server= require('http').createServer(app);
const redis = require('socket.io-redis');
require('dotenv').config();
// var io=require('socket.io')(server,{
//     transports: [ 'websocket', 'polling' ]
// })
var io=require('socket.io')(server)
// // // Add the redis adapter
io.adapter(redis({ host: process.env.REDIS_SERVER, port: 6379 }));
require('./controllers/socket/broadcast.monitoring')(io);

let redisQ=require('redis');
var clientRedis=redisQ.createClient(6379,process.env.REDIS_SERVER);

clientRedis.on('connect', function() {
    console.log('connected with redis server');
});
global.redisClient=clientRedis;

var fs = require('fs');
var exec = require('child_process').exec;
var util = require('util');
var path = require('path');
const uuid=require('uuid');
// TWILIO_ACCOUNT_SID=AC25af1373f00dd7d5658f6f2c95f29c0a
// TWILIO_AUTH_TOKEN=e7b54214c8dd4ffe2ba401eec21d7333
// TWILIO_NUMBER=+17372487799
// TWILIO_MESSAGING_SERVICE_SID=MG9a2222d0fbae031abc8414204db4bc3e
var detect = require('detect-file-type');
var path = require('path');
global.appRoot = path.resolve(__dirname);


const sms=require('./router/sms-broadcast');
const twilioCtrl=require('./router/twilio');
var Files={};

const bodyParser = require('body-parser');

  const moment=require('moment');
  const mTimezone=require('moment-timezone');


// Set up mongoose connection
var Broadcast=require('./model/smsbroadcast.model');
const mongoose = require('mongoose');
let dev_db_url = 'mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB,{ useNewUrlParser: true,autoReconnect:true,poolSize:100});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
const cors = require('cors');

var corsOptions = {
  origin: 'https://app.ocatelkom.co.id',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
}
var corsList=['https://app.ocatelkom.co.id','http://app.ocatelkom.co.id','http://localhost:4200'];
// app.use(cors({
//     origin: function(origin, callback){
//       // allow requests with no origin 
//       // (like mobile apps or curl requests)
//       if(!origin) return callback(null, true);
//       if(corsList.indexOf(origin) === -1){
//         var msg = 'The CORS policy for this site does not ' +
//                   'allow access from the specified Origin.';
//         return callback(new Error(msg), false);
//       }
//       return callback(null, true);
//     }
//   }));

//enable all cors


app.use(cors());
//app.use(cors(corsOptions))
app.use(express.static(__dirname+'/dist'));
app.use(express.json({limit: '50mb'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// app.use(bodyParser());
// app.use(bodyParser({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb'}));

app.get('/home/*',function(req,res,next){
    res.sendFile(path.join(__dirname+'/dist/index.html'));
})

//authorization

require('./model/user');
require('./config/passport');
require('./model/whatsapp.contact-group.model');
require('./model/whatsap.contact-list.model');
require('./model/whatsapp-media.model');
require('./model/whatsapp.broadcast.model');
require('./model/whatsap.broadcast.subcscriber.model');
require('./model/Broadcast.contact-group.model');
require('./model/broadcast.contact-list.model');
require('./model/Broadcast.call.model');
require('./model/IvrSubscriber.model');
//office setting
require('./model/Office.model');
//require setting mode
require('./model/setting.call.model');
require('./model/SmsSetting.model');
require('./model/EmailSetting.model');

//email broadcast model
// require('./model/Broadcast.email.model');
// require('./model/Broadcast.email.subscriber.model');

//One page call setting
require('./model/OcallMonSetting.model');
require('./model/Ocallmon.model');
//sms template 
require('./model/Sms-template');
require('./model/Sms-subscriber-template');
require('./model/Smsbilling.model');
require('./model/Smsbillinghistory');

//whataspp template
require('./model/whatsapp/whatsapp-template.model');
require('./model/whatsapp/whatsapp-template-subscriber.model');
require('./model/whatsapp/whatsapp-billing.model');
require('./model/whatsapp/whatsapp-billing-history.model');
require('./model/whatsapp/whatsapp-setting.model');

//approval model
require('./model/approval_engine/Approval.scheme.model');
require('./model/approval_engine/Approval.request.model');
require('./model/approval_engine/Approval.task.model');
require('./model/approval_engine/Approval.user.model');

require('./model/setting-call.subscriber');

//email
require('./model/email/Email-Broadcast');
require('./model/email/Email-subscriber');
require('./model/EmailAttachments');
require('./model/email/Email-template');
require('./model/email/Email-template-subscriber');

// smart profiling
require('./model/smart-profiling/SmartProfilingModel');
require('./model/smart-profiling/SmartProfilingSchedule');
require('./model/smart-profiling/SmartProfilingScheduleIterator');


app.use('/api/sms',sms);

const audio=require('./router/audio');
app.use('/audio',audio);
const userManagement=require('./router/user-management');
app.use('/api/user',userManagement);
//this route for sms report;
const reporting=require("./router/sms-report");
app.use('/api/report',reporting);
const whatsapp=require("./router/whatsapp");
app.use('/api/whatsapp',whatsapp);
//this route for twilio callback
// app.use('/api/twilio',twilioCtrl);

app.use('/api/webhook',twilioCtrl);

const broadcastCtrl=require("./router/broadcast");
app.use('/api/broadcast',broadcastCtrl);

const broadcastReport=require("./router/report");
app.use('/api/broadcast-report',broadcastReport);

//for setting
const settingCtrl=require("./router/settingCall");

app.use('/api/setting',settingCtrl);

const officeCtrl=require('./router/office');
app.use('/api/office',officeCtrl);

const ivrCtrl=require('./router/ivr');
app.use('/api/ivr',ivrCtrl);

const dashCtrl=require('./router/dashboard');
app.use('/api/dashboard',dashCtrl);
const smsSetting=require('./router/settingSms');
app.use('/api/sms-setting',smsSetting)

// const emailSetting=require('./router/settingEmail');
// app.use('/api/email-setting',emailSetting);


// var adjust=require('./job/adjusting_data');
//                         adjust.running().catch(error=>{
//                           console.log(error);  
//                         });

const smsControl=require('./router/sms-control');
app.use('/api/sms-control',smsControl);

const ocallmonRouter=require('./router/ocallmon');
app.use('/api/ocall',ocallmonRouter);


const callReportMonitoring=require('./router/call-report-monitoring');
app.use('/api/call-monitoring-report',callReportMonitoring);

const smsTemplate=require('./router/sms-template');
app.use('/api/sms-template',smsTemplate);

const waTemplate= require('./router/whatsapp/whatsapp-template-router');
app.use('/api/wa-template',waTemplate);

const waBilling=require('./router/whatsapp/whatsapp-setting-router');
app.use('/api/wa-setting',waBilling);

const approval=require('./router/approval/approval');
app.use('/api/approval',approval);


const email=require('./router/email-router');
app.use('/api/email-broadcast',email);

const topup=require('./router/topup');
app.use('/api',topup);
//sokcet 
require('./controllers/socket/one-call-monitoring')(io);


var Agendash = require('agendash');
global.agenda=require('./job/agenda');

// or provide your own mongo client:
// var agenda = new Agenda({mongo: myMongoClient})

app.use('/dash', Agendash(agenda));
//error handling
app.use(function (err, req, res, next) {
    console.log(err);
    if (err.name === 'UnauthorizedError') {
      res.status(401).json({
        status:'false',
        errors: {
          message: 'Unauthorized User',
          code:'UN_ERR'
        },
      });
    }else{
        res.status(401).json({
            status:'false',
            errors: {
              message:err,
              code:'UNDEF_ERR'
            },
        });
    }
});


//audio stream
let ctrl=require('./controllers/audio-streams/audio-stream');
app.get('/api/audio',ctrl.getAudioStream);
  
server.listen(4300,function(){
    console.log("Listening on port 4300");
    //agenda.start();

    //connect to mongodb and check if any job that not handling properly
});

/**
 * Smart Profiling Router
 * 
 */
const smartProfilingRouter=require('./router/smart-profiling/profiling.router');
app.use('/api/profiling',smartProfilingRouter);

server.on('listening', function () {
    // server ready to accept connections here
    console.log('run old job');
    agenda.on("ready", async function () {
        
        // const jobs = await agenda.jobs({});


        //cek apakah ada job yang next run at nya tidak null
        //jika next runat kecil dari waktu sekarang lakukan schedule ulang
        //
        // await Promise.all(jobs.map((val)=>{ 
        //     let schedule=new Date(Date.now()+1000);
        //     if(val.attrs.nextRunAt!=null){
        //         var agendad=agenda.create(val.attrs.name,val.attrs.data);
        //         console.log(agendad);
        //         agendad.save();
        //     }
        // }));
       
        // var newJob = agenda.create('jobCoba', {}).save();
        // const mongoosed = require('mongoose');
       
        // mongoosed.connection.on('open', () => {
        //     mongoosed.connection.db.collection('agendaJobs', (err, collection) => {
        //       collection.updateMany({ lockedAt: { $exists: true }, lastFinishedAt: { $exists: false } }, {
        //         $unset: {
        //           lockedAt: undefined,
        //           lastModifiedBy: undefined,
        //           lastRunAt: undefined
        //         },
        //         $set: { nextRunAt: new Date() }
        //       }, { multi: true }, (e, numUnlocked) => {
        //         if (e) { console.error(e); }
        //         console.log(`Unlocked #{${numUnlocked}} jobs.`);
        //       });
        //     });
        //   });
          
        //   mongoosed.connect("mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/jobspecialize?retryWrites=true", {
        //     socketTimeoutMS: 0,
        //     keepAlive: true,
        //     reconnectTries: 10,
        //   });

        // await agenda.define('jobCoba1',{lockLifetime: 10000},function(job,done){

        //     var data=job.attrs.data.id;
        //     console.log("cobaLage1"+data);
        // })

        // await agenda.every('2 seconds', 'jobCoba1',{id:1});
        // await agenda.schedule(new Date(Date.now() + 2000), 'jobCoba1',{id:2});
        // await agenda.start();
        console.log("Restarting agenda scheduler...");
    })

});
//socket upload
require('./controllers/socket/sms.broadcast.upload')(io);
require('./controllers/socket/sms.broadcast.manual')(io);
require('./controllers/socket/email.broadcast.upload')(io);
require('./controllers/socket/whatsapp.broadcast.upload')(io);


io.on('connection', function (socket) {

    var dataBroadcast={};
    var dataWhatsappContact={}
    var dataBroadcastContact={};
    var dataMedia={};
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });




    //contact upload
    socket.on('StartContact', function (data) { //data contains the variables that we passed through in the html file
        var Name = data['Name'];
        var Message=data['Data'].message;
        var ContentDesc=data['Data'].contentDesc;
       
        //fetch file like filename, broadcastname etc and save to database
    
        Files[Name] = {  //Create a new Entry in The Files Variable
            FileSize : data['Size'],
            Data   : "",
            Downloaded : 0
        }
        var Place = 0;
        try{
            var Stat = fs.statSync('temp/' +  Name);
            if(Stat.isFile())
            {
                Files[Name]['Downloaded'] = Stat.size;
                Place = Stat.size / 524288;
            }
        }
        catch(er){} //It's a New File
        fs.open("temp/" + Name, "a", 0755, function(err, fd){
            if(err)
            {
                console.log(err);
            }
            else
            {

                
                var extName=path.extname("./temp/"+Name);            
                if(extName==".csv" || extName==".txt"){
                  
                    dataWhatsappContact=data['Data'];
                    socket.emit('passessContact',{'passes':true,'data':""});
                    Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                    socket.emit('MoreDataContact', { 'Place' : Place, Percent : 0 });
                       
                }else{
                    socket.emit('passessContact',{'passes':false,'data':""});
                }
               
            }
        });

        // detect.fromFile('./Temp/'+Name, function(err, result) {

        //     if (err) {
        //       return console.log(err);
        //     }
        //     console.log(result); // { ext: 'jpg', mime: 'image/jpeg' }
        //     if(result.ext=='csv'){
        //         socket.emit('passess',true);
        //     }else if(result.ext=='txt'){
        //         socket.emit('passess',true);
        //     }else{
        //         socket.emit('passess',false);
        //     }
        // });


    });

    socket.on('UploadContact', function (data){
  
        var Name = data['Name'];
        Files[Name]['Downloaded'] += data['Data'].length;
        Files[Name]['Data'] += data['Data'];
        if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
        {
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                //Get Thumbnail Here

                var extName=path.extname("./temp/"+Name);          

                var newName=uuid.v4()+extName;
                var inp = fs.createReadStream("temp/" + Name);
                var out = fs.createWriteStream("contact/" + newName);
                inp.pipe(out);
                inp.on("end",function(){
                    fs.unlinkSync("temp/" + Name, function () { //This Deletes The Temporary File
                        //Moving File Completed
                        console.log("work");
                    });
                    dataWhatsappContact.file_name=newName;

                    let ctrl=require('./controllers/whatsapp-controller');
                    ctrl.uploadContact(dataWhatsappContact);
                    socket.emit('Done', {'data' : 'Succes'});


                })


            });
        }
        else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                Files[Name]['Data'] = ""; //Reset The Buffer
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreDataContact', { 'Place' : Place, 'Percent' :  Percent});
            });
        }
        else
        {

            var Place = Files[Name]['Downloaded'] / 524288;
            var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;

            socket.emit('MoreDataContact', { 'Place' : Place, 'Percent' :  Percent});
        }
    });



    // upload socket for audio file
    socket.on('StartAudio', function (data) { //data contains the variables that we passed through in the html file
        var Name = data['Name'];
        var NewName=data['NewName'];
        //fetch file like filename, broadcastname etc and save to database
        Files[Name] = {  //Create a new Entry in The Files Variable
            FileSize : data['Size'],
            Data   : "",
            Downloaded : 0
        }
        var Place = 0;
        try{
            var Stat = fs.statSync('temp/' +  Name);
            if(Stat.isFile())
            {
                Files[Name]['Downloaded'] = Stat.size;
                Place = Stat.size / 524288;
            }
        }
        catch(er){} //It's a New File
        fs.open("temp/" + Name, "a", 0755, function(err, fd){
            if(err)
            {
                console.log(err);
            }
            else
            {
                
                var extName=path.extname("./temp/"+Name);            
                if(extName==".wav"){
                    //check if file exist
                    try
                    {
                        
                        fs.statSync('./audio/'+Name).isFile();
                        socket.emit('passessAudio',{'passes':true,'data':"file_exists"})
                    }
                    catch (err)
                    {

                      
                        socket.emit('passessAudio',{'passes':true,'data':""});
                        Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                        socket.emit('MoreDataAudio', { 'Place' : Place, Percent : 0 });
                        
                    }
                 
                }else{
                    socket.emit('passessAudio',{'passes':false,'data':""});
                }
               
            }
        });

        // detect.fromFile('./Temp/'+Name, function(err, result) {

        //     if (err) {
        //       return console.log(err);
        //     }
        //     console.log(result); // { ext: 'jpg', mime: 'image/jpeg' }
        //     if(result.ext=='csv'){
        //         socket.emit('passess',true);
        //     }else if(result.ext=='txt'){
        //         socket.emit('passess',true);
        //     }else{
        //         socket.emit('passess',false);
        //     }
        // });


    });

    socket.on('UploadAudio', function (data){
  
        var Name = data['Name'];
        var NewName=data['NewName'];
     
        Files[Name]['Downloaded'] += data['Data'].length;
        Files[Name]['Data'] += data['Data'];
        if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
        {
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                //Get Thumbnail Here
                var extName=path.extname("./temp/"+Name);          
                var newName=NewName;
                var inp = fs.createReadStream("temp/" + Name);
                var out = fs.createWriteStream("audio/" + Name);
                inp.pipe(out);
                inp.on("end",function(){
                    fs.unlinkSync("temp/" + Name, function () { //This Deletes The Temporary File
                        //Moving File Completed
                        console.log("work");
                    });
                    // exec("ffmpeg -i Video/" + Name  + " -ss 01:30 -r 1 -an -vframes 1 -f mjpeg Video/" + Name  + ".jpg", function(err){
                    //     socket.emit('Done', {'Image' : 'Video/' + Name + '.jpg'});
                    // })


                    var nDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Jakarta'
                      });
                   

                    //save data to database
                    let {saveAudio}=require('./controllers/audio-stream-controller');

                    try{
                        saveAudio(Name,NewName);
                        socket.emit('DoneAudio', {'data' : 'Succes'});
                    }catch{
                        socket.emit('DoneAudio', {'data' : 'error'});
                        fs.unlinkSync("audio/" + Name, function () { //This Deletes The Temporary File
                            //Moving File Completed
                            console.log("work");
                        });

                    }
                    //save data to database
                })


            });
        }
        else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                Files[Name]['Data'] = ""; //Reset The Buffer
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreDataAudio', { 'Place' : Place, 'Percent' :  Percent});
            });
        }
        else
        {

            var Place = Files[Name]['Downloaded'] / 524288;
            var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;

            socket.emit('MoreDataAudio', { 'Place' : Place, 'Percent' :  Percent});
        }
    });

    socket.on('StartMedia', function (data) { //data contains the variables that we passed through in the html file
        var Name = data['Name'];
        var NewName=data['NewName'];

        //fetch file like filename, broadcastname etc and save to database
    
        Files[Name] = {  //Create a new Entry in The Files Variable
            FileSize : data['Size'],
            Data   : "",
            Downloaded : 0
        }
        var Place = 0;
        try{
            var Stat = fs.statSync('temp/' +  Name);
            if(Stat.isFile())
            {
                Files[Name]['Downloaded'] = Stat.size;
                Place = Stat.size / 524288;
            }
        }
        catch(er){} //It's a New File
        fs.open("temp/" + Name, "a", 0755, function(err, fd){
            if(err)
            {
                console.log(err);
            }
            else
            {
                
                var extName=path.extname("./temp/"+Name);  
                     
                if(extName==".jpg" || extName==".png" || extName==".pdf" || extName==".xls" || extName==".xlsx" || extName==".doc" || extName==".docx"){
                
                    //check if file exist
                    try
                    {
                        
                        fs.statSync('./dist/media/'+Name).isFile();
                        socket.emit('passessMedia',{'passes':true,'data':"file_exists"})
                    }
                    catch (err)
                    {

                        dataMedia=data['Data'];
                        socket.emit('passessMedia',{'passes':true,'data':""});
                        Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                        socket.emit('MoreDataMedia', { 'Place' : Place, Percent : 0 });
                        
                    }
                 
                }else{
                    socket.emit('passessMedia',{'passes':false,'data':"File Format Must Be .jpg|.png|.pdf|.xls|.xlsx|.doc|.docx"});
                }
               
            }
        });



    });

    socket.on('UploadMedia', function (data){
  
        var Name = data['Name'];
        var NewName=data['NewName'];
     
        Files[Name]['Downloaded'] += data['Data'].length;
        Files[Name]['Data'] += data['Data'];
        if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
        {
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                //Get Thumbnail Here
                var extName=path.extname("./temp/"+Name);          
                var newName=uuid.v4()+extName;
                var inp = fs.createReadStream("./temp/" + Name);
                var out = fs.createWriteStream("./dist/media/" + newName);
                inp.pipe(out);
                inp.on("end",function(){
                    fs.unlinkSync("./temp/" + Name, function () { //This Deletes The Temporary File
                        //Moving File Completed
                        console.log("work");
                    });
                    // exec("ffmpeg -i Video/" + Name  + " -ss 01:30 -r 1 -an -vframes 1 -f mjpeg Video/" + Name  + ".jpg", function(err){
                    //     socket.emit('Done', {'Image' : 'Video/' + Name + '.jpg'});
                    // })

                    var nDate = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Jakarta'
                      });
                      
                    //save data to database
                    let {addMedia}=require('./controllers/whatsapp-controller');
                    let data={};
                    data.file_name=newName;
                    data.media_name=dataMedia.media_name;
                    data.user_id=dataMedia.user_id
                    try{
                        addMedia(data);
                        socket.emit('Done', {'data' : 'Succes'});
                    }catch{
                        fs.unlinkSync("./dist/media/" + Name, function () { //This Deletes The Temporary File
                            //Moving File Completed
                            console.log("work");
                        });
                        socket.emit('Done', {'data' : 'Error'});
                    }
                    //save data to database
                })


            });
        }
        else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                Files[Name]['Data'] = ""; //Reset The Buffer
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreDataMedia', { 'Place' : Place, 'Percent' :  Percent});
            });
        }
        else
        {

            var Place = Files[Name]['Downloaded'] / 524288;
            var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;

            socket.emit('MoreDataMedia', { 'Place' : Place, 'Percent' :  Percent});
        }
    });

    //broadcast contact upload
    socket.on('StartContactBroadcast', function (data) { //data contains the variables that we passed through in the html file
        var Name = data['Name'];
        var Message=data['Data'].message;
        var ContentDesc=data['Data'].contentDesc;
       
        //fetch file like filename, broadcastname etc and save to database
    
        Files[Name] = {  //Create a new Entry in The Files Variable
            FileSize : data['Size'],
            Data   : "",
            Downloaded : 0
        }
        var Place = 0;
        try{
            var Stat = fs.statSync('temp/' +  Name);
            if(Stat.isFile())
            {
                Files[Name]['Downloaded'] = Stat.size;
                Place = Stat.size / 524288;
            }
        }
        catch(er){} //It's a New File
        fs.open("temp/" + Name, "a", 0755, function(err, fd){
            if(err)
            {
                console.log(err);
            }
            else
            {
                var extName=path.extname("./temp/"+Name);
                //checking if the file more than 524288
               // console.log(data['Size']);
                //get mb
                let mb=data['Size']/1000000;
                if(mb>5){
                    socket.emit('passessContactBroadcast',{'passes':false,'data':"Your File Is Too Big Max Size (5 Mb), Please upload again!"});
                }else{
                    if(extName==".csv" || extName==".txt"){
                        dataBroadcastContact=data['Data'];
                        socket.emit('passessContactBroadcast',{'passes':true,'data':""});
                        Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                        socket.emit('MoreDataContactBroadcast', { 'Place' : Place, Percent : 0 });
                    }else{
                        socket.emit('passessContactBroadcast',{'passes':false,'data':"Format is not accepted"});
                    }
                }

            }
        });

        // detect.fromFile('./Temp/'+Name, function(err, result) {

        //     if (err) {
        //       return console.log(err);
        //     }
        //     console.log(result); // { ext: 'jpg', mime: 'image/jpeg' }
        //     if(result.ext=='csv'){
        //         socket.emit('passess',true);
        //     }else if(result.ext=='txt'){
        //         socket.emit('passess',true);
        //     }else{
        //         socket.emit('passess',false);
        //     }
        // });


    });

    socket.on('UploadContactBroadcast', function (data){
        var Name = data['Name'];
        Files[Name]['Downloaded'] += data['Data'].length;
        Files[Name]['Data'] += data['Data'];
        if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
        {
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                //Get Thumbnail Here

                var extName=path.extname("./temp/"+Name);          

                var newName=uuid.v4()+extName;
                var inp = fs.createReadStream("temp/" + Name);
                var out = fs.createWriteStream("contact/" + newName);
                inp.pipe(out);
                inp.on("end",function(){
                    fs.unlinkSync("temp/" + Name, function () { //This Deletes The Temporary File
                        //Moving File Completed
                        console.log("work");
                    });
                    dataBroadcastContact.file_name=newName;
                    let ctrl=require('./controllers/broadcast-controller');
                    ctrl.uploadContact(dataBroadcastContact);
                    socket.emit('Done', {'data' : 'Succes'});

                })


            });
        }
        else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                Files[Name]['Data'] = ""; //Reset The Buffer
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreDataContactBroadcast', { 'Place' : Place, 'Percent' :  Percent});
            });
        }
        else
        {

            var Place = Files[Name]['Downloaded'] / 524288;
            var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;

            socket.emit('MoreDataContactBroadcast', { 'Place' : Place, 'Percent' :  Percent});
        }
    });
});




async function graceful() {
    console.log("matiin");
    await agenda.stop();
    process.exit(0);
}


  
process.on('SIGTERM', graceful);
process.on('SIGINT' , graceful);

// var nDate = new Date().toLocaleString('en-US', {
//     timeZone: 'Asia/Jakarta'
//   });


//   console.log(moment().format('MMMM Do YYYY, h:mm:ss a'));

//   console.log(nDate)


//Errror Code
// UN_ERR -> Unauthorized
// NOT_FOUND-> Not Found