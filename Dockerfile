FROM keymetrics/pm2:latest-alpine
# Bundle APP files
COPY . /node/src

WORKDIR /node/src


# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install 

# Expose the listening port of your app
EXPOSE 4300

# Show current folder structure in logs
RUN ls -al -R
# Change current user to www

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]