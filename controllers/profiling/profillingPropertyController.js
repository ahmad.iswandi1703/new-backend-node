var mongoose=require('mongoose');
var Profilling=mongoose.model('SmartProfilling');
var Schedule=mongoose.model('SmartProfillingSchedule');
var SchdIterator=mongoose.model('SmartProfillingScheduleIterator');

const fs = require('fs');
const axios=require('axios');
const moment=require('moment-timezone');
let amqp = require('amqplib/callback_api');
let amqpurl= process.env.AMQP;
var _=require('lodash');
let schMaker=require('../../lib/schedule-maker');
//you have to do adjust id of each component inside the data profiling-filter.json
const urlSmartProfiling=process.env.SMART_URL;
var componentMaster=[
    {
        id:'1',
        name:'LOCATION_TARGET',
        type:'selectors'
    },{
        id:'2', //adjust to id of component data in profiling-filter.json
        name:'LOCATION_TARGET_EXCLUDE',
        type:'selectors'
    },{
        id:'3', //adjust to id of component data in profiling-filter.json
        name:'WEB_INTEREST',
        type:'selectors'
    },{
        id:'4', //adjust to id of component data in profiling-filter.json
        name:'TV_INTEREST',
        type:'selectors'
    },{
        id:'5', //adjust to id of component data in profiling-filter.json
        name:'CALL_INTEREST',
        type:'selectors'
    },{
        id:'6', //adjust to id of component data in profiling-filter.json
        name:'INTERNET_USAGE',
        type:'selectors'
    },{
        id:'7', //adjust to id of component data in profiling-filter.json
        name:'TV_USAGE',
        type:'selectors'
    },{
        id:'8',
        name:'GENDER',
        type:'radio'
    },{
        id:'9',
        name:'AGE_GROUP',
        type:'checkbox'
    },{
        id:'10',
        name:'LENGTH_OF_STAY',
        type:'checkbox'
    },{
        id:'11',
        name:'ARPU',
        type:'checkbox'
    },{
        id:'12',
        name:'INDIHOME_TYPE',
        type:'selector',
    },{
        id:'13',
        name:'INDIHOME_SPEED',
        type:'selector',
    },{
        id:'14',
        name:'CABLE_TECHNOLOGY',
        type:'selector',
    },{
        id:'15',
        name:'STATUS',
        type:'selector',
    },{
        id:'16',
        name:'PAYMENT DATE',
        type:'selector'
    }
]
// -- make endpoint to send filtering dataset
// -- make save and edit profiling
// -- get data detail for edit and view
// -- request to get data sum of profiling data
exports.getFilterData=async function(req,res,next){
    try{
        let rawdata = fs.readFileSync(appRoot+'/controllers/profiling/profiling-filter.json');  
        let data = JSON.parse(rawdata); 
        let component= data.data.components;
        let returnData=[];
        componentMaster.forEach(element => {
            //find the document inside component in profiling data json
            let valueFound=component.find(el=>{
                return el.id==element.id;
            })
            let newObj={...element,...valueFound};
            if(valueFound){
                //set to new Data and fork join
                returnData.push(newObj)
            }
        });

        return res.json({
            status:true,
            errors:{
             
            },data:returnData
        }); 
    }catch(err){
        console.log(err);
        return res.json({
            status:false,
            errors:{
                code:"DB ERROR",
                message:"Data Cannot Be Found"
            },data:{}
        }); 
    }
}



//add data apply
exports.getTotalProspect=async function(req,res){
    let data=req.body;
    //get data filter from form filter
    let filter=data.filter;
    //let newFilterData=[];
    //fetch the data filter
    let newForm={};
    _.each(filter,function(filter,index,filters){
    
        newForm[filter.id]=[];
        _.each(filter.values,function(val,index,values){
            newForm[filter.id].push(val.id);
        })
       
        
    })

    try{
        let totalProspect=await axios.get(urlSmartProfiling+'/api/prospects', {
            params: newForm
        });

        if(totalProspect){
            return res.json({
                status:true,
                errors:{
                },data:totalProspect
            });
        }
    }catch(err){
        return res.json({
            status:false,
            errors:{
                code:"RESOURCE ERROR",
                message:"Cannot COUNT TOTAL PROSPECT"
            },data:{}
        });
    }

}

exports.createCampaign=async function(req,res){

    let id=req.params.id;
    //send to queue download data; send profiling id.
    //and get data and store to database;
    //change status of profiling data
    //running scheduler 
    try{
        let prof=await Profilling.findById(id);
        let schd=await Schedule.find({'profiling':id});
        //change status of profilling to running
        if(prof){

            if(schd){
                //get data schedule function and parse and validate the data
                let dataScheduler=parseSchedulerCollection(schd);
                saveScheduleIterator(dataScheduler).then((val)=>{

                }).catch(err=>{

                });
            }

        }else{

        }
        
    }catch(err){
        return res.json({
            status:false,
            errors:{
                code:"DB ERROR",
                message:"Data Cannot Be Found"
            },data:{}
        }); 
    }
}   

//add data draft if data already exist then edit this draft
exports.addData=async function(req,res){
    //collect data post property
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    //check if save as draft or not
    let data=req.body;
    let id=req.body.id;
    /// if not draft then save schedule time and start to run scheduler
    try{
        let checkProfiling=await Profilling.findById(id);
        if(checkProfiling){
            //edit this data profile;
            try{
                checkProfiling.campaign_name=data.campaign_name;
                checkProfiling.campaign_type=data.campaign_type;
                checkProfiling.general_setting=data.general_setting; //
                checkProfiling.filter=data.filter;
                //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                checkProfiling.status=data.status;
                checkProfiling.total_prospect=data.total_prospect?data.total_prospect:0;
                checkProfiling.updated_at=nDate;
                //delete all scheduler
                await deleteAllScheduler(checkProfiling._id);
                await checkProfiling.save(function(err){
                    if(error){
                        return res.json({
                            status:false,
                            errors:{
                                code:"DB ERROR",
                                message:"Data Cannot Be Found"
                            },data:{}
                        }); 
                    };
                    //save data schedule time
                    //generate and save data scheduler time
                    //let schedulerParse=parseSchedulerCollection(data.schedule);
                    //save data scheduler

                    //save the date 
                    saveSchedule(data.schedule).then(function(val){
                        //get the detail of profiling by providing schedule time from schedule collection
                        let newValue={...checkProfiling};
                        newValue['schedule']=val
                        return res.json({
                            status:true,
                            errors:{
                            },data:newValue
                        });   
                    }).catch(err=>{
                        return res.json({
                            status:false,
                            errors:{
                                code:"SCHEDULE_CANNOT_BE_MADE",
                                message:"CANNOT SAVE SCHEDULE TIME"
                            },data:{}
                        }); 
                    })

  
                })
            }catch(err){
                return next(err);
            }
            //edit this schedule time
        }else{
            let newData=new Profilling({
                campaign_name:data.campaign_name,
                campaign_type:data.campaign_type,
                general_setting:data.general_setting, //
                filter:data.filter,
                //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                // location:data.location,
                // //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                // behavior:data.behavior,
                // //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                // demographic:data.demographic,
                // //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                // product_subscription:data.product_subscription,
                //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                status:data.status,
                total_prospect:data.total_prospect?data.total_prospect:0,
                //draft, active, completed
                user:data.user,
                created_at:nDate,
                //[{id:value_id,title:value_title,order:value_order},{},{},{}]
                updated_at:nDate
                //[{id:value_id,title:value_title,order:value_order},{},{},{}]
            })

            newData.save(function(error){
                if(error){
                    console.log(error);
                    return res.json({
                        status:false,
                        errors:{
                            code:"DB ERROR",
                            message:"Data Cannot Be Found"
                        },data:{}
                    }); 
                };
                //save schedule time
                saveSchedule(data.schedule).then(function(val){
                    //get the detail of profiling by providing schedule time from schedule collection
                    let newValue={...newData};
                    newValue['schedule']=val

                    //run the scheduler if status == active;
                    return res.json({
                        status:true,
                        errors:{
                        },data:newValue
                    });   
                }).catch(err=>{
                    return res.json({
                        status:false,
                        errors:{
                            code:"SCHEDULE_CANNOT_BE_MADE",
                            message:"CANNOT SAVE SCHEDULE TIME"
                        },data:{}
                    }); 
                })

            })
        }
    }catch(err){
        return res.json({
            status:false,
            errors:{
                code:"DB ERROR",
                message:"Please Try Again Later"
            },data:{}
        }); 
    }

}

function saveSchedule(dataSchedule,profiling){
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    //fetch array of schedule
    return new Promise((resolve,reject)=>{
    //validate date collection if any data that has same date
        let scheduleCollection=[];
        _.each(dataSchedule,function(schedule,index,schedules){
            let startDate=new Date()
            let schedule=new Schedule();
            //let's validate the value;
            //check the value
            //set default value if it doesnt have proper characteristic
            schedule.schedule_date_from=schedule.schedule_date_from;
            schedule.schedule_date_to=schedule.schedule_date_to;
            schedule.schedule_start_time=schedule.schedule_start_time;
            schedule.schedule_end_time=schedule.schedule_end_time;
            schedule.schedule_timezone=schedule.schedule_timezone; 
            schedule.profiling=profiling;
            schedule.created_at=nDate;
            schedule.updated_at=nDate;
            scheduleCollection.push(schedule);
            schedule=null;
        })

        Schedule.collection.insertMany(scheduleCollection,function(err,docs){
            if(err){
                reject();
            }

            resolve(docs);
        })
    //save
    })

}


function saveScheduleIterator(dataSchedule,profilingID){
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    //fetch array of schedule
    return new Promise((resolve,reject)=>{
    //validate date collection if any data that has same date
        let scheduleCollection=[];
        _.each(dataSchedule,function(schedule,index,schedules){
            let schedule=new SchdIterator();
            schedule.schedule_date=schedule.date;
            schedule.schedule_start_time={
                hours:parseInt(schedule.startTime.hour),
                minutes:parseInt(schedule.startTime.minute)
            };
            schedule.schedule_end_time={
                hours:parseInt(schedule.endTime.hour),
                minutes:parseInt(schedule.endTime.minute)
            };
            schedule.schedule_running_time_in='1 minutes';
            schedule.schedule_timezone=schedule.timezone;
            schedule.scheduler_order=index;
            schedule.schedule_status='waiting';//waiting, running, completed
            schedule.created_at=nDate;
            schedule.updated_at=nDate;
        })

        SchdIterator.collection.insertMany(scheduleCollection,function(err,docs){
            if(err){
                reject();
            }
            resolve(docs);
        })
    //save
    })
}
//validate


//this function will received value of scheduler date and time that passed to parameter from request body
function parseSchedulerCollection(scheduleData){
    // get all object array and iterate the data
    let schedulerCollections=[];
    _.each(scheduleData,function(sch,index,schedules){
        //generate date schedule collection from scheduler maker
        let schNewCollection=schMaker.parseScheduler(sch);
        console.log(index);
       
        if(index>0){
            schNewCollection=schMaker.validateScheduler(schedulerCollections,schNewCollection);
        }   
        schedulerCollections=[...schedulerCollections,...schNewCollection];
    })  


    let returnvalue=schMaker.sorting(schedulerCollections,['date','startTime.hour','startTime.minute'],'asc');
    return returnvalue;
}

function deleteAllScheduler(profilingID){
    return new Promise((resolve,reject)=>{
        Schedule.findByIdAndRemove(profilingID,function(err,media){
            if (err){
                reject();
            }
            resolve();
        }); 
    });
}



exports.editData=async function(req,res,next){
    //check if data status is other than completed, active, return back if data has been actived or completed
    let id=req.body.id;
    let reqData=req.body;
    try{
        Profilling.findById(id,function(err,data){
            data.campaign_name=reqData.campaign_name;
            data.campaign_type=data.campaign_type;
            data.general_setting=data.general_setting; //
            data.filter=data.filter;
            //[{id:value_id,title:value_title,order:value_order},{},{},{}]
            data.status=data.status;
            data.total_prospect=data.total_prospect;
            data.updated_at=DateNow.format();
            data.save(function(err){
                if(error){
                    return res.json({
                        status:false,
                        errors:{
                            code:"DB ERROR",
                            message:"Data Cannot Be Found"
                        },data:{}
                    }); 
                };
                //save data schedule time
                return res.json({
                    status:true,
                    errors:{
                    },data:data
                });     
            })
        })
    }catch(err){
        return next(err);
    }
}
exports.deleteData=async function(req,res,next){
    let id=req.params.id;
    //check if data was running, profiling status with active cannot be deleted
    try{

        //check if any children
        Profilling.findById(id,function(err,profiling){
            if (err) {
                return res.json({
                    status:false,
                    errors:{
                        code:"DB ERROR",
                        message:"Data Cannot Be Found"
                    },data:{}
                }); 
            }
            if(profiling){
                if(profiling.status=='active'){
                    return res.json({
                        status:false,
                        errors:{
                            code:"DATA HAS BEEN RUNNING",
                            message:""
                        },data:{}
                    }); 
                }else{
                    Profilling.findByIdAndRemove(id,function(err,media){
                        if (err){
                            return res.json({
                                status:false,
                                errors:{
                                    code:"DB ERROR",
                                    message:"Data Cannot Be Found"
                                },data:{}
                            }); 
                        }
                        return res.json({
                            status:true,
                            errors:{
                            },data:{}
                        });
                    }); 
                }
            }else{
                return res.json({
                    status:false,
                    errors:{
                        code:"DB ERROR",
                        message:"Data Cannot Be Found"
                    },data:{}
                }); 
            }

    
        })
    }catch(err){
        return next(err);
    }
}

exports.getDataDetail=async function(req,res,next){
        //check if data status is other than completed, active, return back if data has been actived or completed
        let id=req.params.id;
        try{
            Profilling.findById(id,function(err,data){
                if(data){
                    return res.json({
                        status:true,
                        errors:{
                        },data:data
                    });   
                }else{
                    return res.json({
                        status:false,
                        errors:{
                            code:"DB ERROR",
                            message:"Data Cannot Be Found"
                        },data:{}
                    }); 
                }
            })
        }catch(err){
            return res.json({
                status:false,
                errors:{
                    code:"DB ERROR",
                    message:"Data Cannot Be Found"
                },data:{}
            }); 
        }
}