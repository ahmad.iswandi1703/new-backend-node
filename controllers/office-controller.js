var mongoose=require('mongoose');
var Office=mongoose.model('Office');
var User=mongoose.model('User');
const moment=require('moment-timezone');
var arrayToTree = require('array-to-tree');
var ObjectId = require('mongoose').Types.ObjectId; 
let DateNow=moment().tz('Asia/Jakarta');
exports.createOffice=function(req,res,next){
    var office=new Office();
    office.name=req.body.name;
    office.address=req.body.address;
    office.phone_number=req.body.phone_number;
    office.whatsapp=req.body.whatsapp;

    if(req.body.parents=="null"){
        office.parents=null;
    }else{
        office.parents=req.body.parents;
    }
  
    office.created_at=DateNow.format();
    office.updated_at=DateNow.format();
    office.save(function(err){
        if (err) {
            return next(err);
        }
        return res.json({ success:"true",message:office});     
    })
}

exports.editOffice=async function(req,res,next){
    //find model and then save;

    let id=req.body.id;

    console.log(req.body);

    try{
        Office.findById(id,function(err,office){
            console.log(office);
            office.name=req.body.name;
            office.address=req.body.address;
            office.phone_number=req.body.phone_number;
            office.whatsapp=req.body.whatsapp;
        
            if(req.body.parents=="null" ||req.body.parents==null){
                office.parents=null;
            }else{
                office.parents=req.body.parents;
            }
    
            office.updated_at=DateNow.format();
        
            office.save(function(err){
                if (err) {
                    return next(err);
                }
                return res.json({ success:"true",message:office});     
            })
        })
    }catch(err){
        return next(err);
    }
}

exports.deleteOffice=function(req,res,next){
 
    let id=req.body.uid;
    try{

        //check if any children


        //check if any users active
        var checkChildren=new Promise(function(resolve,reject){
            Office.find({parents:ObjectId(id)},function(err,resp){
                resolve(resp);
            })
        });

        checkChildren.then(function(val){
            if(val.length>0){
                return res.json({ success:false,message:"There are office branch in this office"});  
            }else{
                User.find({office:ObjectId(id)},function(err,resp){
                    if (err) return handleError(err);
                
                    if(resp.length>0){
                        return res.json({ success:false,message:"There are user exists in this office"});  
                    }else{
                        // Office.remove({ _id: id }, function(err) {
                        //     if (!err) {
                        //         return res.json({ success:"true",message:"succes to remove data"});  
                        //     }
                        //     else {
                        //         return next(err);
                        //     }
                        // });
                        Office.findByIdAndRemove(id,function(err,media){
                            if (err)
                                return handleError(err);
                            res.send({ success: true, message: "Deleted" });
                        });
                    }
        
                })
            }
        }).catch((err)=>{
            return handleError(err);
        })




    }catch(err){
        return next(err);
    }
}

exports.getDetailOffice=function(req,res,next){
    try{
        Office.findById(req.params.uid,function(err,office){
            if (err) {
                return next(err);
            }
            return res.json({ success:"true",message:office});     
        })
    }catch(err){
        return next(err);
    }

}

exports.getOffice=function(req,res,next){


    //get all office with branching;
    //get data children

    //set from parent (super root)
    try{
        Office.find(function(err,resp){

            // console.log(arrayToTree(resp));
    
            let newDate=JSON.stringify(resp);
    
            let newOne=JSON.parse(newDate);
    
            return res.json({"status":true,message:arrayToTree(newOne, {
                parentProperty: 'parents',
                customID: '_id'
              })});
          
        })
    }catch(err){
        return next(err);
    }

    var dataTwo = [
        {
          _id: 'ec654ec1-7f8f-11e3-ae96-b385f4bc450c',
          name: 'Portfolio',
          parents: null
        },
        {
          _id: 'ec666030-7f8f-11e3-ae96-0123456789ab',
          name: 'Web Development',
          parents: 'ec654ec1-7f8f-11e3-ae96-b385f4bc450c'
        },
        {
          _id: 'ec66fc70-7f8f-11e3-ae96-000000000000',
          name: 'Recent Works',
          parents: 'ec666030-7f8f-11e3-ae96-0123456789ab'
        },
        {
          _id: '32a4fbed-676d-47f9-a321-cb2f267e2918',
          name: 'About Me',
          parents: null
        }
      ];
       

    
}



//get office recursively 
function getOfficeRecursively(parent){
    //get all data that parents is null
    //do some loop

    //and get all data that have parents_id is same as root parents

    //
    var nodes = {};    
    return arr.filter(function(obj){
        var id = obj["name"],
            parentId = obj["parent"];

        nodes[id] = _.defaults(obj, nodes[id], { children: [] });
        parentId && (nodes[parentId] = (nodes[parentId] || { children: [] }))["children"].push(obj);

        return !parentId;
    });

}


