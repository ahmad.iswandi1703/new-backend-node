/*add to each user in call setting*/
const mongoose = require('mongoose');
const Setting= mongoose.model('CallSetting');
const Subscriber=mongoose.model('CallSettingSubscriber');
const ObjectId=mongoose.Types.ObjectId;
const uuid=require('uuid');

const moment=require('moment-timezone');
let ERROR_CODE={
    UNDEFINED_VARIABLE:"UNDEFINED_VARIABLE",
    DB_ERROR:"CANNOT FIND DATA",
    FORMAT_ERROR:"FORMAT_ERROR",
}

exports.setSetting=async function(req,res,next){
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    let user_id=req.body.subscriber; ///expect array;
    let call_setting=req.body.call_setting;

    let setting;
    //check if call_setting_exist by id;
    await getCallSetting(call_setting).then(val=>{
        setting=val;
    }).catch(err=>{
        return res.json({
            status:false,
            errors:{
                code:"INVLDPRM",
                message:{
                    property:"call_setting",
                    error:"cannot find call setting"
                },data:{}
            },
        })
    })
    //if type == user then check if user exist (if any user exist dont set data) 

    if(Array.isArray(user_id)){
        console.log("")
        if(user_id.length>0){
            let data=[];

            //check setting if already exist dont do this task
            let existSetting=0;
          
            await Promise.all(user_id.map(async function(resp){
                await Subscriber.find({user:resp.user_id}).then(val=>{
                   
                    if(val.length>0){
                        existSetting++;
                    }else{
                        let subs=new Subscriber();
                        subs.user=resp.user_id;
                        subs.setting=setting._id;;
                        subs.created_at=nDate;
                        subs.updated_at=nDate;
                        data.push(subs);
                    }
                })

            }))
            // save multiple documents to the collection referenced by Book Model
            if(data.length>0){
                Subscriber.collection.insert(data, function (err, docs) {
                    if (err){
                        console.log(err); 
                        throw Error("err");
                    } else {      
                        return res.json({
                            status:true,
                            errors:{},data:docs
                        });
                    }
                });
            }else{
                return res.json({
                    status:false,
                    errors:{},data:{}
                })
            }

        }else{
            return res.json({
                status:false,
                errors:{
                    code:"INVLDPRM",
                    message:{
                        property:"user_id",
                        error:"cannot be null"
                    },data:{}
                },
            })
        }
    }else{
        //if user_id is string
        let parameter={user:user_id};
        Subscriber.find(parameter).then((val)=>{
            //set data
            if(val.length>0){
                return res.json({
                    status:false,
                    errors:{
                        code:"SETTINGEXIST",
                        message:"Cannot set data, because setting already exist"
                    },
                    data:{}
                })
            }else{
                let data=new Subscriber();
                data.created_at=nDate;
                data.updated_at=nDate;
                
                data.user=user_id;
                data.setting=setting._id;
          
                data.save(function(err){
                    if (err) {
                        return next(err);
                    }
                    return res.json({
                        status:true,
                        errors:{
                        },
                        data:data
                    })        
                })
    
            }
    
        }).catch(err=>{
                return res.json({
                    status:false,
                    errors:{
                        code:"DBERROR",
                        message:"Error In Db"
                    },
                    data:{}
                })    
        })
    }

}

exports.editSubscriber=async function(req,response,next){
  
    let subscriber=req.body.subscriber; ///expect array
    let call_setting=req.body.call_setting;
    let dateNow=moment().tz('Asia/Jakarta').format();
    if(subscriber.length>0){
        let data=[];
        await Promise.all(subscriber.map(async function(res){
            if(res.status=="new"){
                await Subscriber.find({user:res.user_id}).then(val=>{
                    if(val.length>0){
                      
                        existSetting++;
                    }else{
                        console.log("hererer");
                        let subs=new Subscriber();
                        subs.user=res.user_id;
                        subs.setting=call_setting;
                        subs.created_at=dateNow;
                        subs.updated_at=dateNow;
                        data.push(subs);
                    }
                })
     
            }else if(res.status=="delete"){
     
                Subscriber.findOneAndRemove({setting:ObjectId(call_setting),user:ObjectId(res.user_id)},function(err){
                    if(err){
                        throw Error("err");
                    }
                });
            }

        }));


        // save multiple documents to the collection referenced by Book Model

        if(data.length >0){
            console.log("Mkebu Gerer");
            Subscriber.collection.insertMany(data, function (err, docs) {
                if (err){ 
                    throw Error("err");
                } else {
                    return response.json({
                        status:true,
                        errors:{},data:{}
                    });
                }
            });
        }else{
            return response.json({
                status:true,
                errors:{},data:{}
            });
           
        }

       
    }else{
        return response.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"Error When Inserting Data"
            },data:{}
        });  
    }
}

exports.getSubscriberByTemplate=async function(req,res,next){

    let call_setting=req.params.call_setting_id;
    try{
        Subscriber.find({setting:call_setting}).populate('user').exec(function(err,resp){
         if(resp.length>0){
            return res.json({
                status:true,
                errors:{},data:resp
            });
          }else{
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.DB_ERROR,
                    message:"Cannot Find Data"
                },data:{}
            });  
          }
        })
    }catch(err){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"Cannot Find Data"
            },data:{}
        });  
    }


}

exports.getCallSetting=async function(user_id){
    let returnData={
        status:false,
        data:""
    }

    await getCallSettingByuser(user_id).then(val=>{
        returnData.status=true;
        returnData.data=val;
    }).catch(err=>{
        console.log(err);
    })

    return returnData;
}

exports.queryPagingCallSetting=async function(req,res,next){
    let options={
        sort:     { created_at:-1},
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit
      };

    let created_by=req.body.data.user_id;
    
    
      Setting.paginate({}, options)
      .then(response => {
        /**
         * Response looks like:
         * {
         *   docs: [...] // array of Posts
         *   total: 42   // the total number of Posts
         *   limit: 10   // the number of Posts returned per page
         *   page: 2     // the current page of Posts returned
         *   pages: 5    // the total number of pages
         * }
        */
          if(response.docs.length >0){

            return res.json({
                status:true,
                errors:{},data:response
            }); 
            
          }else{
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.DB_ERROR,
                    message:"data not found"
                },data:{}
            });  
          }
         
      })
      .catch(function(err){
          //handle query error;
          // set loggin
          console.log(err);
          return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"data not found"
            },data:{}
        });  
      });
}

function getCallSetting(id){
    return new Promise((resolve,reject)=>{
        Setting.findById(id).then(val=>{
            if(val){
                resolve(val);
            }else{
                reject("val is null");
            }
        }).catch(err=>{
            reject(err);
        })
    })
}

function getCallSettingByuser(user_id){
    return new Promise((resolve,reject)=>{
        Setting.findOne({user:user_id}).then(val=>{
            if(val){
                resolve(val);
            }else{
                reject("val is null");
            }
        }).catch(err=>{
            reject(err);
        })
    })  
}