
var fs = require('fs');
var path = require('path');
const uuid=require('uuid');

var moment = require('moment-timezone');

let EmailController=require('../../controllers/email/email-controller');

exports=module.exports=async function(io){
    io.sockets.on("connection",async function(socket){
        let Files={};
        socket.on('StartEmail', function (data) { //data contains the variables that we passed through in the html file
            let Name = data['Name'];
            var Message=data['Data'].message;
            var ContentDesc=data['Data'].contentDesc;
            //get data template by template ID
            console.log(appRoot+"/temp");
            Files[Name] = {  
                FileSize : data['Size'],
                Data   : "",
                Downloaded : 0
            }
            let Place = 0;
            try{
                var Stat = fs.statSync(appRoot+'/temp/' +  Name);
                if(Stat.isFile())
                {
                    Files[Name]['Downloaded'] = Stat.size;
                    Place = Stat.size / 524288;
                }
            }
            catch(er){} //It's a New File
            fs.open(appRoot+"/temp/" + Name, "a", 0755, function(err, fd){
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    var extName=path.extname(appRoot+"/temp/"+Name);            
                    if(extName==".csv" || extName==".txt"){
                        //checking format
                        var explodeStr=Message.split(' ');
                        var newText="";
                        var notSameContent=0;
                        explodeStr.forEach(el=>{
                      
    
                            var b= el.match(/\[(.*?)\]/g);
                            if(b!=null){
                                
                                var a= el.replace(/\[(.*?)\].*/g,"$1");
                                if(ContentDesc.indexOf(a) < 0){
                                    newText+="<i class='text-danger'>"+el+"</i>";
                                    notSameContent+=1;
                                }else{
                                    newText+="<i class='text-success'>"+el+"</i>";
                                }
                            }else{
                                newText+=el;
                                
                            }
                            newText+=" ";
                            
                        })
    
                        if(notSameContent > 0){
                            socket.emit('passessEmail',{'passes':true,'data':newText});
                        }else{
                            dataBroadcast=data['Data'];
                            socket.emit('passessEmail',{'passes':true,'data':""});
                            Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                            socket.emit('MoreDataEmail', { 'Place' : Place, Percent : 0 });
                        }
                     
                           
                    }else{
                        socket.emit('passessEmail',{'passes':false,'data':""});
                    }
                }
            });
    
            // detect.fromFile('./Temp/'+Name, function(err, result) {
    
            //     if (err) {
            //       return console.log(err);
            //     }
            //     console.log(result); // { ext: 'jpg', mime: 'image/jpeg' }
            //     if(result.ext=='csv'){
            //         socket.emit('passess',true);
            //     }else if(result.ext=='txt'){
            //         socket.emit('passess',true);
            //     }else{
            //         socket.emit('passess',false);
            //     }
            // });
    
    
        });
    
        socket.on('UploadEmail', function (data){
            let dataBroadcast=data["AdditionalData"];
            console.log(dataBroadcast);
            let Name = data['Name'];
            Files[Name]['Downloaded'] += data['Data'].length;
            Files[Name]['Data'] += data['Data'];
            if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
            {
                fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                    //Get Thumbnail Here
                    var extName=path.extname(appRoot+"/temp/"+Name);          
                    var newName=uuid.v4()+extName;
                    var inp = fs.createReadStream(appRoot+"/temp/" + Name);
                    var out = fs.createWriteStream(appRoot+"/contact/" + newName);
                    inp.pipe(out);
                    inp.on("end",function(){
                        fs.unlinkSync(appRoot+"/temp/" + Name, function () { //This Deletes The Temporary File
                            //Moving File Completed
                            console.log("work");
                        });
                        // exec("ffmpeg -i Video/" + Name  + " -ss 01:30 -r 1 -an -vframes 1 -f mjpeg Video/" + Name  + ".jpg", function(err){
                        //     socket.emit('Done', {'Image' : 'Video/' + Name + '.jpg'});
                        // });
    
    
                        //checking if the data doesnt have proper format
                        if (dataBroadcast.broadcastName==""){
                            dataBroadcast.broadcastName="Broadcast "+Date.now();
                        }
                    


                        EmailController.makeBroadcast(dataBroadcast).then(async (dataB)=>{
                            //save data attachments file
                           
                            if(dataBroadcast.attachments.length>0){
                                await EmailController.makeAttachmentsBroadcast(dataB._id,dataBroadcast.attachments,dataBroadcast.user_id).then(val=>{
                                    console.log("success make email Attachments")
                                }).catch(err=>{
                                    console.log(err);
                                })
                            }
        
                            let fileName=newName;
                            let template={
                                template_id:"default",
                                message:"",
                                variable:""
                            }
                            console.log(dataBroadcast);
                            if(dataBroadcast.template=='default' || dataBroadcast.template==undefined){
                                let messageBody=dataBroadcast.message;
                                template.message=messageBody;
                                template.variable=dataBroadcast.contentDesc;
                            }else{
                        
                                template.template_id=dataBroadcast.template;
                            }

                            console.log(dataB);
                            EmailController.sendManualBroadcastToMessageQueue(dataB,fileName,template);
                            socket.emit('DoneEmail', {'data' : 'Succes'});
                        }).catch(err=>{
                            console.log(err);
                            socket.emit('DoneEmail', {'data' : 'failed'});
                        })
                        
                       
           
                    })
    
                });
            }
            else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
                fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                    Files[Name]['Data'] = ""; //Reset The Buffer
                    var Place = Files[Name]['Downloaded'] / 524288;
                    var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                    socket.emit('MoreDataEmail', { 'Place' : Place, 'Percent' :  Percent});
                });
            }
            else
            {
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreDataEmail', { 'Place' : Place, 'Percent' :  Percent});
            }
        });
    })
    

}