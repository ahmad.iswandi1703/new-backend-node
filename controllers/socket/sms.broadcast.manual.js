var fs = require('fs');
var path = require('path');
const uuid=require('uuid');
var Broadcast=require('../../model/smsbroadcast.model');
var moment = require('moment-timezone');

let SmsController=require('../../controllers/sms-controller');


exports=module.exports=async function(io){
    io.sockets.on("connection",async function(socket){
        let Files={};
        var dataBroadcast={};
        socket.on('Start', function (data) { //data contains the variables that we passed through in the html file
            var Name = data['Name'];
            var Message=data['Data'].message;
            var ContentDesc=data['Data'].contentDesc;
           
            //fetch file like filename, broadcastname etc and save to database
        
            Files[Name] = {  //Create a new Entry in The Files Variable
                FileSize : data['Size'],
                Data   : "",
                Downloaded : 0
            }
            var Place = 0;
            try{
                var Stat = fs.statSync(appRoot+'/temp/' +  Name);
                if(Stat.isFile())
                {
                    Files[Name]['Downloaded'] = Stat.size;
                    Place = Stat.size / 524288;
                }
            }
            catch(er){} //It's a New File
            fs.open(appRoot+"/temp/" + Name, "a", 0755, function(err, fd){
                if(err)
                {
                    console.log(err);
                }
                else
                {
    
                    
                    var extName=path.extname(appRoot+"/temp/"+Name);            
                    if(extName==".csv" || extName==".txt"){
                       
                        //checking format
                        var explodeStr=Message.split(' ');
                        var newText="";
                        var notSameContent=0;
                        
                        explodeStr.forEach(el=>{
                      
    
                            var b= el.match(/\[(.*?)\]/g);
                            if(b!=null){
                                
                                var a= el.replace(/\[(.*?)\].*/g,"$1");
                                
                                
                                if(ContentDesc.indexOf(a) < 0){
                                    newText+="<i class='text-danger'>"+el+"</i>";
                                    notSameContent+=1;
                                }else{
                                    newText+="<i class='text-success'>"+el+"</i>";
                                }
                            }else{
                                newText+=el;
                                
                            }
                            newText+=" ";
                            
                        })
    
                        if(notSameContent > 0){
                            socket.emit('passess',{'passes':true,'data':newText});
                        }else{
                            dataBroadcast=data['Data'];
                            socket.emit('passess',{'passes':true,'data':""});
                            Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                            socket.emit('MoreData', { 'Place' : Place, Percent : 0 });
                        }
                     
                           
                    }else{
                        socket.emit('passess',{'passes':false,'data':""});
                    }
                   
                }
            });
    
            // detect.fromFile('./Temp/'+Name, function(err, result) {
    
            //     if (err) {
            //       return console.log(err);
            //     }
            //     console.log(result); // { ext: 'jpg', mime: 'image/jpeg' }
            //     if(result.ext=='csv'){
            //         socket.emit('passess',true);
            //     }else if(result.ext=='txt'){
            //         socket.emit('passess',true);
            //     }else{
            //         socket.emit('passess',false);
            //     }
            // });
    
    
        });
    
        socket.on('Upload', function (data){
 
            var Name = data['Name'];
            Files[Name]['Downloaded'] += data['Data'].length;
            Files[Name]['Data'] += data['Data'];
            if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
            {
                fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                    //Get Thumbnail Here
    
                    var extName=path.extname(appRoot+"/temp/"+Name);          
    
                    var newName=uuid.v4()+extName;
                    var inp = fs.createReadStream(appRoot+"/temp/" + Name);
                    var out = fs.createWriteStream(appRoot+"/contact/" + newName);
                    inp.pipe(out);
                    inp.on("end",function(){
                        fs.unlinkSync(appRoot+"/temp/" + Name, function () { //This Deletes The Temporary File
                            //Moving File Completed
                            console.log("work");
                        });
                        // exec("ffmpeg -i Video/" + Name  + " -ss 01:30 -r 1 -an -vframes 1 -f mjpeg Video/" + Name  + ".jpg", function(err){
                        //     socket.emit('Done', {'Image' : 'Video/' + Name + '.jpg'});
                        // });
    
    
                        //checking if the data doesnt have proper format
                        if (dataBroadcast.broadcastName==""){
                            
                            dataBroadcast.broadcastName="Broadcast "+Date.now();
                        }
                        // var nDate = new Date().toLocaleString('en-US', {
                        //     timeZone: 'Asia/Jakarta'
                        //   });
                        let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
                        console.log(dataBroadcast);
                        var br=new Broadcast ({
                            broadcast_name:dataBroadcast.broadcastName,
                            broadcast_status:'starting',
                            broadcast_date:nDate,
                            updated_at:nDate,
                            created_at:nDate,
                            broadcast_subscriber:0,
                            user_id:dataBroadcast.user_id
                        });
                        br.save(function(error){
                            if(error) throw error;
                            var broadcast_id=br._id;
                            //send to message queueu;
                            //var a=require('./job/broadcast');
                            let fileName=newName;
                            // a.running(dataBroadcast,broadcast_id).catch(error=>{
                            //   console.log(error);  
                            // });
                            let messageBody=dataBroadcast.message;
                            let template={
                                message:"",
                                variable:""
                            }

                            template.message=messageBody;
                            template.variable=dataBroadcast.contentDesc;
                            SmsController.sendManualBroadcastToMessageQueue(br,fileName,template);
                            //doing scheduler
                            socket.emit('Done', {'data' : 'Succes'});

                        })
    
                    })
    
    
                });
            }
            else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
                fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                    Files[Name]['Data'] = ""; //Reset The Buffer
                    var Place = Files[Name]['Downloaded'] / 524288;
                    var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                    socket.emit('MoreData', { 'Place' : Place, 'Percent' :  Percent});
                });
            }
            else
            {
    
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
    
                socket.emit('MoreData', { 'Place' : Place, 'Percent' :  Percent});
            }
        });
    })
    

}