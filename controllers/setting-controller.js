const mongoose = require('mongoose');
const Setting= mongoose.model('CallSetting');
const uuid=require('uuid');
const moment=require('moment-timezone');
let DateNow=moment().tz('Asia/Jakarta');
const Subscriber=mongoose.model('CallSettingSubscriber');

exports.setSetting=function(req,res,next){

    let setting=new Setting();
    setting.caller_id_name=req.body.caller_id_name;
    setting.caller_id_number=req.body.caller_id_number;
    setting.dialing_timeout=req.body.dialing_timeout;
    setting.max_call_duration=req.body.max_call_duration;
    setting.concurent_limit=req.body.concurent_limit;//default 10
    setting.schedule_minute_per=req.body.schedule_minute_per; //run schedule every one minute max 60 minutes
    setting.retries_call=req.body.retries_call;
    setting.retries_call_between=req.body.retries_call_between; //min 10 minutes
    setting.daily_start_time=req.body.daily_start_time;
    setting.status=req.body.status;
    setting.daily_end_time=req.body.daily_end_time;
    setting.weekdays=req.body.weekdays;
    setting.type=req.body.type; /// all user(default), specific user
    setting.user_id=req.body.user_id;
    setting.updated_id=req.body.updated_id;
    setting.updated_at=DateNow.format();
    setting.created_at=DateNow.format();
    setting.gateway_id=req.body.gateway_id;
    setting.timezone=req.body.timezone;
    setting.setting_name=req.body.setting_name;

    ///add form gateway
    ///ada form timezone


    if(setting.concurent_limit<10 ){
        setting.concurent_limit=10;
    }

    if(setting.schedule_minute_per>60){
        setting.schedule_minute_per=60;
    }else if(setting.schedule_minute_per<1){
        setting.schedule_minute_per=1;
    }

    if(setting.retries_call_between<10){
        setting.retries_call_between=10;
    }

    setting.save(function(err){
        if (err) {
            return next(err);
        }
        return res.json({ success:"true",message:setting});     
    })
               

}


exports.getCallSetting=function(user_id){
    return new Promise(function(resolve,reject){
  
        //check in the CallSetting if the user_id matches and type of setting is specific user if not then get default setting
        Setting.find({},function(err,response){
            if (err){
                reject(err);
            }

            let userFind=response.filter(function(value){
                return value.user_id==user_id && value.type=="specific"
            });

            if(userFind.length>0){
                resolve(userFind[0]);
            }else{
                let defaultSetting=response.filter(function(value){
                    return value.type="all"; //this type all == default
                });
                
                if(defaultSetting.length>0){
                    resolve(defaultSetting[0]);
                }else{
                    reject("setting not found");
                }

            }
        })
    });
}

exports.getCallSettingByUser=function(user_id){
    return new Promise(function(resolve,reject){
  
        //check in the CallSetting if the user_id matches and type of setting is specific user if not then get default setting
        Subscriber.find({user:user_id}).populate('setting').then(function(response){
            if(response.length>0){
                resolve(response[0]);
            }else{
                reject("setting not found");
            }
        }).catch(err=>{
            reject(err);
        })
    });
}

    
    
