const Broadcast=require('../model/smsbroadcast.model');
const Subscriber=require('../model/subscriberSms.model');
const mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId; 

var moment=require("moment");
const Json2csvParser=require('json2csv').Parser;


const User=mongoose.model('User');
// Load the full build.
var _ = require('lodash');
//using hook schema for setting delete children if the parent have deleted
//
const Json2csvTransform=require('json2csv').Transform;

exports.getBroadcast=function(req,res){
      
    Broadcast.find({user_id:req.body.user_id,broadcast_status:{$ne:"delete"}}).sort({broadcast_date:-1}).exec(function(err,result){
        if (err) throw err;
        res.send(result);
    })

}
exports.getReport=function(req,res){

    let options={
        sort:     { created_at: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit
    };
    let user_id=req.body.data.user_id;
    //let status=req.body.data.status;
    let fromDate=moment(req.body.data.from_date).format();
    let toDate=moment(req.body.data.to_date).format();

    var query={};
    query.user_id={$ne:null};
    if(user_id!="all"){
        query.user_id=user_id;
        //get data all user and get broadcast
    }

    query.created_at={$gte: fromDate, $lt: toDate};
    query.broadcast_status={$ne:"delete"};

    //add redis in the future
    Broadcast.paginate(query,options).then(response=>{
        //get all broadcast per user;
        //        
        //count failed 
        //count deliverd
        //count processing
        let newVal=[];
        //res.send(response);
        Promise.all(response.docs.map(async function(val){
            let failed;
            let delivered;
            let user_name;
            
            try{
                failed=await Subscriber.countDocuments({broadcast:ObjectId(val._id),status:{$ne:"delivered"}}).exec();
                delivered=await Subscriber.countDocuments({broadcast:ObjectId(val._id),status:"delivered"}).exec();

                //get user name
                let usr=await User.findOne({_id:ObjectId(val.user_id)});

                user_name=usr.email;

            }catch(err){
                failed=0;
                delivered=0;
                user_name="";
            }
            val.failed=failed;
            val.delivered=delivered;
            val.user_name=user_name;
            newVal.push(val);
        })).then((val)=>{
            response.docs=newVal;
            res.send(response);

        })
        
        //count sent
        //count undelivered

        //count processing
    });


    //if user (all);
    //return data in array
}


exports.download=function(req,res){
    let user_id=req.body.user_id;
    //let status=req.body.data.status;
    let fromDate=moment(req.body.from_date).format();
    let toDate=moment(req.body.to_date).format();
    let nameOfFile;
    nameOfFile='sms_broadcast_'+req.body.tanggal_name.replace(/[ ,.]/g,'_')+'.csv';
   
    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename='+nameOfFile);
    res.set('Content-Type', 'text/csv');
    res.type('blob');

    var query={};
    query.user_id={$ne:null};
    if(user_id!="all"){
        query.user_id=user_id;
        //get data all user and get broadcast
    }

    query.created_at={$gte: fromDate, $lt: toDate};
    query.broadcast_status={$ne:"delete"};

    //add redis in the future
    Broadcast.find(query).sort({created_at:-1}).then(response=>{
        //get all broadcast per user;
        //        
        //count failed 
        //count deliverd
        //count processing
        let newVal=[];
        //res.send(response);
        Promise.all(response.map(async function(val){
            let failed;
            let delivered;
            let user_name;
            
            try{
                failed=await Subscriber.countDocuments({broadcast:ObjectId(val._id),status:{$ne:"delivered"}}).exec();
                delivered=await Subscriber.countDocuments({broadcast:ObjectId(val._id),status:"delivered"}).exec();

                //get user name
                let usr=await User.findOne({_id:ObjectId(val.user_id)});

                user_name=usr.email;

            }catch(err){
                failed=0;
                delivered=0;
                user_name="";
            }
 
            
            let newData={
                user_name:"",
                campaign_name:"",
                total_sms:"",
                failed:0,
                delivered:0,
                tanggal:""
            }
            newData.user_name=user_name;
            newData.campaign_name=val.broadcast_name;
            newData.total_sms=val.broadcast_subscriber;
            newData.failed=failed;
            newData.delivered=delivered;
            newData.tanggal=moment(val.created_at).format("YYYY-MM-DD");
        
            newVal.push(newData);
        })).then((val)=>{
            let data=newVal;
          

            
            const fields=['user_name','campaign_name','total_sms','failed','delivered','tanggal'];


            const json2csvParser = new Json2csvParser({ fields });
            const csv = json2csvParser.parse(data);
        
        
            res.send(csv);

           
            
        })
        
        //count sent
        //count undelivered

        //count processing
    }).catch(err=>{
        console.log(err);
        const fields=['user_name','campaign_name','total_sms','failed','delivered','tanggal'];
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse([]);
        res.send(csv);
    })
    // var cursor=Subscriber.find(query).exec(function(err,response){
    //     res.send(response);
    // });


     
    // You can also listen for events on the conversion and see how the header or the lines are coming out.
    // json2csv
    //   .on('header', header => console.log(header))
    //   .on('line', line => console.log(line))
    //   .on('error', err => console.log(err));


    // cursor.pipe(res.type('json'));
   
    // cursor.on('data',function(data){
    //     // phone:String,
    //     // name:String,
    //     // messsage:String,
    //     // status:String,
        
    //     const fields=['phone','name','status'];
    //     const dataCsv= new json2csv({fields});
    //     let parse=dataCsv.parse(data);
    //     dataReport=parse;
    //     console.log(dataReport);

    // });


    // cursor.on('close',function(data){
    //     res.status(200).send(dataReport);
    // });
    
}