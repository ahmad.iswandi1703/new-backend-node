const mongoose = require('mongoose');
const UserSchema= mongoose.model('ApprovalUser');
const Schema=mongoose.model('ApprovalSchema');
const ObjectId=mongoose.Types.ObjectId;
const uuid=require('uuid');

const moment=require('moment-timezone');


exports.addSchema=function(req,res,next){

    
    let schema_name=req.body.schema_name;
    let schema_description=req.body.schema_description;
    let approval_level=req.body.approval_level;
    let created_by=req.body.created_by;
    let user_approvers=req.body.user_approvers;

    //model for user_approvers:
    // {
    //     user_id:
    //     level:0
    // }

    //check for approval_level 
    if(approval_level=="" || approval_level==null){
        return res.json({
            status:false,
            errors:{
                code:"NEEDPARAM",
                message:"approval level cannot be null",
            },
            data:{}
        })
    }

    //check for user approvers
    let checkUsers;
    if(typeof user_approvers=="object"){
        if(user_approvers.length >0){
            checkUsers=fetchingUserApprovers(user_approvers);
           
            if(!checkUsers.status){
                return res.json({
                    status:false,
                    errors:{
                        code:"INVLDPRM",
                        message:checkUsers.data
                    },data:{}
                })
            }else{
                if(approval_level!= checkUsers.data.length){
                    return res.json({
                        status:false,
                        errors:{
                            code:"ERRORLOGIC",
                            message:"Sum of users must be same as approval level"
                        },data:{}
                    })
                }
            }
        }else{
            return res.json({
                status:false,
                errors:{
                    code:"NEEDPARAM",
                    message:"Parameter cannot be null"
                },data:{}
            })
        }
    }
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();

    let schema=new Schema();
    schema.schema_name=schema_name;
    schema.schema_description=schema_description;
    schema.approval_level=approval_level;
    schema.created_by=created_by;
    schema.created_at=nDate;
    schema.updated_at=nDate;
    //save data to db
    schema.save((err)=>{
        if(err){

        }
        let userCollection=[];
        checkUsers.data.map((val)=>{
            let user=new UserSchema();
            user.user=val.user_id;
            user.level=val.level;
            user.approval=schema._id;
            user.created_at=nDate;
            user.updated_at=nDate;
            user.created_by=created_by;
            userCollection.push(user);

        });

        UserSchema.collection.insert(userCollection,(err,docs)=>{
            if(err){
                return res.json({
                    status:false,
                    errors:{
                        code:"DBERROR",
                        message:"Cannot create approval users"
                    },data:{}
                })
            }else{
                return res.json({
                    status:true,
                    errors:{
                        
                    },data:{
                        schema:schema,
                        users:docs
                    }
                })
            }
        })
        //save data user
    })
}

function fetchingUserApprovers(userCollection){
    let notValid;
    let returnData={
        status:false,
        data:[]
    }
    if(userCollection.length>0){
        let pembandingLevel=0;
        userCollection.map((val)=>{
     
            let formatUserId=true;
            let formatLevel=true;
            let errorMessage=[];
            let user_id="";
            let level="";
            if(!val.hasOwnProperty("user_id")){
                formatUserId=false;
                user_id=val.user_id;
                errorMessage.push("there is no user id property");
            }else{
                user_id=val.user_id;
            }
      
            if(val.hasOwnProperty("level")){
                
                if(val.level<=pembandingLevel){
                    formatLevel=false;
                    level=val.level;
                    errorMessage.push("level is incorrect");
                }else{
                    level=val.level
                }
                pembandingLevel=val.level
            }else{
                formatLevel=false;
                errorMessage.push("there is no level property");
            }

            if(formatLevel && formatUserId){
                returnData.status=true;
            }else{
                returnData.status=false;
            }
            returnData.data.push({
                user_id:user_id,
                level:level,
                error:errorMessage
            })
           
        })


    }

    return returnData;
}

exports.editSchema=function(req,res,next){

}

exports.deleteSchema=function(req,res,next){
    
}