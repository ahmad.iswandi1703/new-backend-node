const mongoose = require('mongoose');
const UserSchema= mongoose.model('ApprovalUser');
const Schema=mongoose.model('ApprovalSchema');
const Request=mongoose.model('ApprovalRequest');
const Task=mongoose.model('ApprovalTask');
const ObjectId=mongoose.Types.ObjectId;
const uuid=require('uuid');

let engine=require('./approval-engine');

const moment=require('moment-timezone');
exports.getSchema=async function(req,res,next){
    Schema.find({},(err,resp)=>{
        if(err){
            return res.json({
                status:false,
                errors:{
                    message:"cannot find data"
                },
                data:{}
            })
        }

        return res.json({
            status:true,
            errors:{

            },data:resp
        })
    })
}

exports.addRequest=async function(req,res,next){
    let user=req.body.user;
    let approval=req.body.approval;
    let comment=req.body.comment;
    let status="processing";
    let approval_state="wait for approving";
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    let created_at=nDate;
    let updated_at=nDate;

    //insert to DB if success making task 

    let request=new Request();

    request.user=user;
    request.approval=approval;
    request.comment=comment;
    request.status=status;
    request.approval_state=approval_state;
    request.created_at=created_at;
    request.updated_at=updated_at;

    request.save((err)=>{
        if(err){
            console.log(err);
            return res.json({
                status:false,
                errors:{
                    code:"DBERROR",
                    message:"Cannot make request approval"
                },data:{}
            })
        }
        //making task in task engin
        engine.add_task(request._id,approval).then((val)=>{
            console.log(val);
            if(val.status){
                return res.json({
                    status:true,
                    errors:{
                        
                    },data:request
                })   
            }else{
                request.status="error"
                request.save((err)=>{
                    if(err){
                        console.log(err);
                    }
                })
                return res.json({
                    status:false,
                    errors:{
                        code:"DBERROR",
                        message:"Cannot maker request approval"
                    },data:{}
                })

               
            }
        }).catch(err=>{
            request.status="error"
            request.save((err)=>{
                if(err){
                    console.log(err);
                }
            })
            return res.json({
                status:false,
                errors:{
                    code:"DBERROR",
                    message:"Cannot maker request approval"
                },data:{}
            })
        })
    })

}

exports.requestPaging=function(req,res,next){

    let options={
        sort:     { created_at: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit,
        populate:'approval'
    };

    let status=req.body.data.status;
    var query={};
    if(status!="all"){
        query.status=status;
    }
    query.user=req.body.data.user_id;
 
    Request.paginate(query,options).then(response=>{
        if(response.docs.length >0){
            res.send({status:true,data:response});
        }else{
            res.send({status:false,data:"Data not found"});
        }
    }).catch(function(err){
        //doing nothing if any error;
        return next(err);
    });
}

//edit delete

exports.editRequest=function(req,res,next){
    let request_id=req.body.request_id;
    let comment=req.body.comment;

    Request.findById(request_id,(err,res)=>{
        if(err){
            return res.json({
                status:false,
                errors:{
                    message:"cannot find data request",
                    code:"ERRORINDB"
                },data:{}
            })
        }

        res.comment=comment;
        res.save((err)=>{
            if(err){
                return res.json({
                    status:false,
                    errors:{
                        message:"cannot edit data",
                        code:"ERRORINDB"
                    },data:{}
                })
            }

            return res.json({
                status:true,
                errors:{},data:{}
            })
        })
    })
}


exports.deleteRequest=function(req,res,next){

    let request_id=req.params.request_id;
    Request.findOneAndRemove({_id:request_id},(err,ress)=>{
        if(err){
            return res.json({
                status:false,
                errors:{
                    message:"error deleted data"
                }
            })
        }

        Task.findOneAndRemove({approval_request:request_id},(err,resp)=>{
            return res.json({
                status:true,
                errors:{
                  
                }
            })
        })


    })
    // Request.findById(request_id).then((val)=>{
    //     if(val){
    //         val.status="delete";
    //         val.save((err)=>{
    //             if(err){
    //                 return res.json({
    //                     status:false,
    //                     errors:{
    //                         code:"DB ERROR",
    //                         message:"CANNOT DELETE DATA"
    //                     },data:{}
    //                 });  

    //             }
    //             return res.json({
    //                 status:true,
    //                 errors:{

    //                 },data:{}
    //             });  
    //         });
    //     }
    // }).catch(err=>{
    //     return res.json({
    //         status:false,
    //         errors:{
    //             code:"DB ERROR",
    //             message:"CANNOT FIND BROADCAST"
    //         },data:{}
    //     });  
    // })
}

