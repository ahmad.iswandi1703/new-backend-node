const mongoose = require('mongoose');
const ContactGroup=mongoose.model('BroadcastGroup');
const ContactList=mongoose.model('BroadcastContactList');
const BroadcastCall=mongoose.model('BroadcastCall');
const job=require('../job/broadcast-upload-contact');
const uuid=require('uuid');
var moment = require('moment-timezone');
const ObjectId=mongoose.Types.ObjectId;


const {Pool, Client} =require('pg'); 


const SettingController=require('../controllers/setting-controller');
exports.uploadContact=function(data){
    //data represent structure
    // {
    //     contact_group_name:
    //     user_id:localStorage.getItem('user_id'),
    //     contact_group_name:'',
    //     file_name:'',
    //     content_desc:[
    //       'Nomor',
    //       'Nama'
    //     ]
    // }

    // let DateNow=new Date().toLocaleString('en-US', {
    //     timeZone: 'Asia/Jakarta'
    //   });
    let DateNow=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    let group =new ContactGroup();
    group.contact_group_name=data.contact_group_name;
    group.contact_group_file=data.file_name;
    group.content_desc=data.content_desc;
    group.created_at=DateNow;
    group.status="Processing";
    group.updated_at=DateNow;
    group.user_id=data.user_id;
    //run job from here
 
    group.save(function(err){
        if (err) throw err;
        job.running(data,group._id);
    });

}


//this method will execute by job
exports.saveContactList=function(data){

    // {
    //     phone:String,
    //     name:String,
    //     messsageError:String,
    //     status:String, //not whatsapp number, or anything
    //     enabled:Boolean,
    // }
    let ObjectId=mongoose.Types.ObjectId;
    //check if contact already exist to avoid duplication
    return new Promise((resolve,reject)=>{
        ContactList.findOne({contact_group:data.contact_group,phone:data.phone}).then((val)=>{
            
            if(val){
                console.log("contact exist");
            }else{
                let contact=new ContactList();
                contact.phone=data.phone;
                contact.name=data.name;
                contact.messageError=data.messageError;
                contact.status=data.status;
                contact.enabled=data.enabled;
                let DateNow=new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Jakarta'
                  });
                contact.created_at=DateNow;
                contact.updated_at=DateNow;
                contact.contact_group=data.contact_group;
                contact.user_id=data.user_id;
                contact.content_data=data.content_data;
                contact.save(function(error){
                    //send exception
                    if(error) reject(err);

                    resolve();
                });
            }
        }).catch(err=>{
            console.log("cannot write contact");
            reject(err);
        })
    })

}

exports.getContactList=function(req,res){

    let options={
        sort:     { created_at: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit
    };
    let group_id=req.body.data.group_id;
    let status=req.body.data.status;

    var query={};
    if(group_id!="all"){
        query.contact_group=group_id;

    }


    ContactList.paginate(query,options).then(response=>{
    
        res.send(response);
    }).catch(function(){
        //doing nothing if any error;
    });
}

exports.getContactGroup=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { created_at: -1 },
        page:   req.body.offset, 
        limit:    req.body.limit,
    };

    // ContactGroup.aggregate([
    //     {$match:{user_id:req.body.user_id}},
    //     {$limit:options.limit},
    //     {$skip:options.page},
    //     {$sort:{created_at:-1}}

    // ]).then((val)=>{
    //     console.log(val);
    // })
    ContactGroup.paginate({user_id:req.body.user_id}, options)
    .then((resp) => {
       
        //add sum of list contact
        
        // resp.docs.map(function(val){
            
        //     return 
        // })


        async function a(){
            count=0;
            var newData=[];

            await Promise.all(resp.docs.map(async (val)=>{
                let data={
                    _id:val._id,
                    content_desc:val.content_desc,
                    contact_group_name:val.contact_group_name,
                    contact_group_file:val.contact_group_file,
                    created_at:val.created_at,
                    updated_at:val.updated_at,
                    status:val.status,
                    user_id:val.user_id,
                    __v:val.__v,
                    id:val.id,
                    count_contact_list:0
                }

              data.count_contact_list=await ContactList.countDocuments({"contact_group":val.id}).exec()          
              // console.log(data.count_contact_list);
               await newData.push(data);
            }));

            resp.docs=[];
            resp.docs=newData;
           
            await res.send(resp);
    
        }

        a();

     



    
        


      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
      
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};

exports.getContactGroupAll=function(req,res){
    ContactGroup.find({user_id:req.body.user_id}).sort({created_at:-1}).exec(function(err,result){
        if (err) throw err;
        if(result.length>0){
            res.json({success:true,value:result});
           
        }else{
            res.json({success:false,value:[]});
        }

    })
}

exports.deleteContactGroup=function(req,res){
    //check if contact group has beend used by another process (Call broadcast);

    //delete

    //send back response to the client
}


//set code error;
exports.sendCall=function(req,res,next){
    //check if contact list is not null
    //get call setting by user id condition ;
    // let job=require('../job/test-job-call');

    // job.runnig({setting:"dfdfadsfadfaf"});
    // return res.json({ success:"true"});

    //lock phone group

    SettingController.getCallSettingByUser(req.body.user_id).then(async function(val){
        // if (err){
        //     next(err);
        // }     
        console.log(val);
        if(!val){
            return res.json({ success:"false",message:"There is no setting call please call administrator "});
        }
        //set data request to Database
        let call=new BroadcastCall();
        call.uid=uuid.v4();
        call.presence_id=call.uid;
        call.campaign_name=req.body.campaign_name; //this campaign id
        call.application=req.body.application;
        call.account_code=call.uid;
        call.phonebook=req.body.phonebook;
        call.call_setting=val.setting._id;
        call.call_immediately=req.body.call_immediately;

        //set call schedule
        let date=req.body.date;
        let time=req.body.time;
  
        var toLocalTime = function(time) {
            var d = new Date(time);
            var offset = (new Date().getTimezoneOffset() / 60) * -1;
            var n = new Date(d.getTime() + offset);
            return n;
        };
        if(!req.body.call_immediately){
            let nexDate=new Date(date.year,date.month-1,date.day,time.hour,time.minute);
          //  let nexDate=date.year+"-"+date.month+"-"+date.day+" "+time.hour+":"+time.minute;
           // nexDate=toLocalTime(nexDate);
            let dNewNow=moment(nexDate);
            let newNa=dNewNow.clone().tz(val.setting.timezone);
            call.call_schedule=newNa.format();
            console.log("Date start to calll thew newOn");
            console.log(call.call_schedule);
        }

        user_id=req.body.user_id;
        //Preparation, Starting , Proggress, Finish, Error
        call.call_status="Preparation";
        call.user_id=req.body.user_id;
        var nDate = moment().tz(val.setting.timezone); 
        call.created_at=nDate.format();
        call.updated_at=nDate.format();
    
        let contactList;
        //get contact rows;
        let rowContact= await ContactList.countDocuments({"contact_group":call.phonebook}).exec();
        call.contact_count=rowContact;

        if(rowContact<0){
            return res.json({ success:"false",message:"Contact Group Doesnt Have List of contacts"});
        }
        //get concurent limit from setting

        call.call_estimated_time=calculateEstimatedTime(rowContact,val.setting.concurent_limit,val.setting.schedule_minute_per);
        try{
            await call.save(function(err){
                if(err) {throw(err)};

                let job=require('../job/first-initiate-call');
            
                job.runnig(call,agenda);
                //running job here;
                return res.json({ success:"true",message:call});
            })
        }catch(err){
            return next(err);
        }
    }).catch((err)=>{
        //send request error data setting
        return next(err);
    })

}

// exports.sendCall=function(req,res,next){
//     //check if contact list is not null
//     //get call setting by user id condition ;
//     // let job=require('../job/test-job-call');

//     // job.runnig({setting:"dfdfadsfadfaf"});
//     // return res.json({ success:"true"});

//     //lock phone group

//     SettingController.getCallSettingByUser(req.body.user_id).then(async function(val){
//         // if (err){
//         //     next(err);
//         // }     
//         if(!val){
//             return res.json({ success:"false",message:"There is no setting call please call administrator "});
//         }
//         //set data request to Database
//         let call=new BroadcastCall();
//         call.uid=uuid.v4();
//         call.presence_id=call.uid;
//         call.campaign_name=req.body.campaign_name; //this campaign id
//         call.application=req.body.application;
//         call.account_code=call.uid;
//         call.phonebook=req.body.phonebook;
//         call.call_setting=val._id;
//         call.call_immediately=req.body.call_immediately;

//         //set call schedule
//         let date=req.body.date;
//         let time=req.body.time;
  
//         var toLocalTime = function(time) {
//             var d = new Date(time);
//             var offset = (new Date().getTimezoneOffset() / 60) * -1;
//             var n = new Date(d.getTime() + offset);
//             return n;
//         };
//         if(!req.body.call_immediately){
//             let nexDate=new Date(date.year,date.month-1,date.day,time.hour,time.minute);
//           //  let nexDate=date.year+"-"+date.month+"-"+date.day+" "+time.hour+":"+time.minute;
//            // nexDate=toLocalTime(nexDate);
//             let dNewNow=moment(nexDate);
//             let newNa=dNewNow.clone().tz('Asia/Jakarta');
//             call.call_schedule=newNa.format();
//             console.log("Date start to calll thew newOn");
//             console.log(call.call_schedule);
//         }

//         user_id=req.body.user_id;
//         //Preparation, Starting , Proggress, Finish, Error
//         call.call_status="Preparation";
//         call.user_id=req.body.user_id;
//         var nDate = moment().tz('Asia/Jakarta'); 
//         call.created_at=nDate.format();
//         call.updated_at=nDate.format();
    
//         let contactList;
//         //get contact rows;
//         let rowContact= await ContactList.countDocuments({"contact_group":call.phonebook}).exec();
//         call.contact_count=rowContact;

//         if(rowContact<0){
//             return res.json({ success:"false",message:"Contact Group Doesnt Have List of contacts"});
//         }
//         //get concurent limit from setting
    
//         call.call_estimated_time=calculateEstimatedTime(rowContact,val.concurent_limit,val.schedule_minute_per);
//         try{
//             await call.save(function(err){
//                 if(err) {throw(err)};

//                 let job=require('../job/test-job-call');
            
//                 job.runnig(call,agenda);
//                 //running job here;
//                 return res.json({ success:"true",message:call});
//             })
//         }catch(err){
//             return next(err);
//         }
//     }).catch((err)=>{
//         //send request error data setting
//         return next(err);
//     })

// }

exports.getBroadcast=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { created_at: -1 },
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit
    };
   
    BroadcastCall.paginate({'user_id':req.body.user_id,call_status:{$ne:"one_app_call"}}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};

exports.getBroadcastDetail=async function(req,res){
    let broadcastCall=req.body.broadcast_call_id;
    var getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.findById(broadcastCall,function(err,res){
            resolve(res);
        })
    })
    //get data broadcass
    let broadcastCalls;
    try{
        let dataBroadcast=await getBroadcastCall;
        broadcastCalls=dataBroadcast;
    }catch(err){
        console.log("cannot find broadcast call data");
        res.send({success:false,data:{}});
    }


    res.send({success:true,data:broadcastCalls});
}

exports.getDetailRecords=async function(req,res){
    const pool = new Pool();
    const client=await pool.connect();
    //account code same as call_broadcast_uuid;
    const accountcode=req.body.broadcast_call_id;
    let data=[];
    let answersCount=0;
    //get data answers
    let dataModel={
        title:"",
        status_data:"",
        message:"",
        data:""
    }
    try{
        let query="";


        query+="SELECT COUNT(*) FROM v_xml_cdr";
        query+=" where accountcode=$1";
        query+=" and hangup_cause in ('NORMAL_CLEARING','MEDIA_TIMEOUT')";
        query+=" and billmsec!=0"
        const dataAnswer=await client.query(query, [accountcode]);
        dataModel.title="ANSWER";
        dataModel.status_data=true;
        dataModel.message="oke";
        dataModel.data=dataAnswer.rows[0].count;
        answersCount=dataAnswer.rows[0].count;
        console.log(dataAnswer.rows);

        data.push(dataModel);
    }catch(err){
        console.log(err);
        console.log("cannot fetch data call answers2");
        dataModel.title="ANSWER";
        dataModel.status_data=false;
        dataModel.call_answer.message="failed to fetch data";
        dataModel.call_answer.data=0;
        data.push(dataModel);
    }

    //get data RING NOT ANSWER
    dataModel={
        title:"",
        status_data:"",
        message:"",
        data:""
    }
    try{
        var newQuery="";
        newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
        newQuery+=" where accountcode=$1";
        //add criteria, NORMAL CLEARING AND BILL MSEC == 0
        newQuery+=" and hangup_cause in ('NO_USER_RESPONSE','NORMAL_CLEARING')";
        newQuery+=" and billmsec=0 "
        const dataNoAnswer=await client.query(newQuery, [accountcode]);
        dataModel.title="RING NOT ANSWER";
        dataModel.status_data=true;
        dataModel.message="oke";
        dataModel.data=dataNoAnswer.rows[0].count;
        data.push(dataModel);
    }catch(err){
        console.log("cannot fetch data call answers3");
        dataModel.title="RING NOT ANSWER";
        dataModel.status_data=false;
        dataModel.call_answer.message="failed to fetch data";
        dataModel.call_answer.data=0;
        data.push(dataModel);
    }

    //not ringing
    dataModel={
        title:"",
        status_data:"",
        message:"",
        data:""
    }
    try{
        var newQuery="";
        newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
        newQuery+=" where accountcode=$1";
        newQuery+=" and hangup_cause not in ('NO_USER_RESPONSE','NORMAL_CLEARING','MEDIA_TIMEOUT')";
        const dataNoAnswer=await client.query(newQuery, [accountcode]);
        dataModel.title="NOT RINGING";
        dataModel.status_data=true;
        dataModel.message="oke";
        dataModel.data=dataNoAnswer.rows[0].count;
        data.push(dataModel);
    }catch(err){
        console.log("cannot fetch data call answers3");
        dataModel.title="NOT RINGING";
        dataModel.status_data=false;
        dataModel.call_answer.message="failed to fetch data";
        dataModel.call_answer.data=0;
        data.push(dataModel);
    }
    let answerNotInteract=0;

     //to get data of remains call substract sum of cdr with contact list
    //not ringing
    dataModel={
        title:"",
        status_data:"",
        message:"",
        data:""
    }
     try{
        //fetch all data survey report
        var querySurveyReport="";

        var newQuery="";
        // newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
        // newQuery+=" where accountcode=$1";
        // newQuery+=" and hangup_cause=$2"
        // newQuery+=" and caller_id_number not in (";
        // newQuery+=" SELECT destination_number from v_ivr_survey_report where call_broadcast_uuid=$1)";
        newQuery+=" SELECT COUNT(destination_number) from v_ivr_survey_report";
        newQuery+=" where call_broadcast_uuid=$1";
        newQuery+=" group by destination_number";

        const allCalls=await client.query(newQuery, [accountcode]);
        dataModel.title="ANSWER INTERACT";
        dataModel.status_data=true;
        dataModel.message="oke";
        dataModel.data=allCalls.rows.length;
        answerNotInteract=allCalls.rows.length;
        //console.log(contactList-allCalls.rows[0].count);
        data.push(dataModel);

    }catch(err){
        console.log(err);
        console.log("cannot fetch data call no Interaction");
        dataModel.title="ANSWER NOT INTERACT";
        dataModel.status_data=false;
        dataModel.call_answer.message="failed to fetch data";
        dataModel.call_answer.data=0;
        data.push(dataModel);
    }

    //not ringing
    dataModel={
        title:"",
        status_data:"",
        message:"",
        data:""
    }
    dataModel.title="ANSWER NOT INTERACT";
    dataModel.status_data=true;
    dataModel.message="oke";
    dataModel.data=answersCount-answerNotInteract;
    data.push(dataModel);
    await client.release();
    await pool.end();
    res.send({success:true,data:data});
}

exports.getDetailRecords1=async function(req,res){
    const pool = new Pool();
    const client=await pool.connect();
    //account code same as call_broadcast_uuid;
    const accountcode=req.body.broadcast_call_id;
    //data model to be send
    let data=
    {
        call_answer:{
            status_data:false,
            message:"",
            data:0,
        },
        no_answer:{
            status_data:false,
            message:"",
            data:0
        },
        no_interaction:{
            status_data:false,
            message:"",
            data:0,
        }
    };

 
    //fetch data from db xml cdr table
    //get data from cdr where call hangup == answer'
    //Hangup cause

    //NORMA_CLEARING
    //NORMAL_CIRCUIT_CONGESTION
    //NO_ANSWER
    //NO_USER_RESPONSE
    //DESTINATION_OUT_OF_ORDER
    //ORIGINATOR_CANCEL
    //USER_NOT_REGISTERED
    try{
        let query="";

        query+="SELECT COUNT(*) FROM v_xml_cdr";
        query+=" where accountcode=$1";
        query+=" and hangup_cause in ('NORMAL_CLEARING','NORMAL_CIRCUIT_CONGESTION')";
    
        const dataAnswer=await client.query(query, [accountcode]);
        data.call_answer.status_data=true;
        data.call_answer.message="oke";
        data.call_answer.data=dataAnswer.rows[0].count;
    }catch(err){
        console.log(err);
        console.log("cannot fetch data call answers2");
        data.call_answer.message="failed to fetch data";
        data.call_answer.data=0;
    }



  

    //get data from cdr where call hangup == no answer
    try{
        var newQuery="";
        newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
        newQuery+=" where accountcode=$1";
        newQuery+=" and hangup_cause in ('NO_ANSWER','NO_USER_RESPONSE','CALL_REJECTED','DESTINATION_OUT_OF_ORDER','ORIGINATOR_CANCEL','USER_NOT_REGISTERED')";
        const dataNoAnswer=await client.query(newQuery, [accountcode]);
      

        data.no_answer.status_data=true;
        data.no_answer.message="oke";
        data.no_answer.data=dataNoAnswer.rows[0].count;
       
       

    }catch(err){
        console.log("cannot fetch data call answers3");
        data.no_answer.message="failed to fetch data";
        data.no_answer.data=0;
    }

     //to get data of remains call substract sum of cdr with contact list
     try{
        //fetch all data survey report
        var querySurveyReport="";

        var newQuery="";
        newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
        newQuery+=" where accountcode=$1";
        newQuery+=" and hangup_cause=$2"
        newQuery+=" and caller_id_number not in (";
        newQuery+=" SELECT destination_number from v_ivr_survey_report where call_broadcast_uuid=$1)";

        const allCalls=await client.query(newQuery, [accountcode,"NORMAL_CLEARING"]);
      

        data.no_interaction.status_data=true;
        data.no_interaction.message="oke";
        data.no_interaction.data=allCalls.rows[0].count;
        //console.log(contactList-allCalls.rows[0].count);

    }catch(err){
        console.log(err);
        console.log("cannot fetch data call no Interaction");
        data.no_interaction.message="failed to fetch data";
        data.no_interaction.data=0;
    }

    await client.release();
    await pool.end();

    res.send({success:true,data:data});
}



function calculateEstimatedTime(contactRows,concurrentLimit, callMinuteSchedule){
    //
    let callPerminute=Math.ceil(contactRows/concurrentLimit);
    let divide= callPerminute;

    //add estimated time
    let minuteTotal=divide * callMinuteSchedule ;
    var hours = (minuteTotal / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);

    return rhours + " hour(s) and " + rminutes + " minute(s).";
}

exports.surveyReport=async function(req,res){
    const pool = new Pool();
    const client=await pool.connect();
    const application=req.body.application;
    const accountcode=req.body.broadcast_call_id;
    //data model to be send
    //get data from cdr where call hangup == no answer
    let dataPush=[];
    try{
        var newQuery="";
        newQuery+="SELECT * FROM v_ivr_survey_pivot";
        newQuery+=" where uuid=$1";
        newQuery+=" order by created_at asc";
 
        const ivrSurvey =await client.query(newQuery, [application]);
    

       // console.log(ivrSurvey.rows);

        await Promise.all(ivrSurvey.rows.map(async function(val){
            try{
    
               
                var newQuery="";
                newQuery+="SELECT * FROM v_ivr_menus";
                newQuery+=" where ivr_menu_uuid=$1";
                newQuery+=" and ivr_type='MULTI_CHOICE'";
                let ivrMenu=await client.query(newQuery, [val.ivr_menu_uuid]);
                console.log("masuk sini");


                if(ivrMenu.hasOwnProperty('rows')){
                    await Promise.all(ivrMenu.rows.map(async function(data){
                        let dataObject={menu_name:'',menu_id:'',survey:[]};
                        dataObject.menu_name=data.ivr_menu_description;
                        dataObject.menu_id=data.ivr_menu_uuid
                        var query="";
                  
                        //join with 
                        query+="SELECT v_ivr_menu_options.*,coalesce(options.jml,0) as survey_count  FROM v_ivr_menu_options";
                        query+=" left join (select ivr_menu_option_uuid, count(*) as jml ";
                        query+="from v_ivr_survey_report where call_broadcast_uuid='"+accountcode+"' group by ivr_menu_option_uuid)";
                        query+=" options on CAST(options.ivr_menu_option_uuid as uuid)=v_ivr_menu_options.ivr_menu_option_uuid";
                        query+=" where v_ivr_menu_options.ivr_menu_uuid=$1";
                        try{
                            let ivrOptions=await client.query(query,[data.ivr_menu_uuid]);
                            if(ivrOptions.hasOwnProperty('rows')){
                                dataObject.survey=ivrOptions.rows;
                            }
                        }catch(err){
                            console.log("error here");
                            console.log(err);
                        }

                        dataPush.push(dataObject);
                    }))
                }

            }catch(err){
                console.log(err);
            }
        }))

        
    }catch(err){
        console.log(err);
        console.log("cannot fetch data call answers3");

    }



    await client.release();
    await pool.end();
    res.send({success:true,data:dataPush});
}

exports.changeStatusScheduler=async function(req,res){
    let broadcast_id=req.params.broadcast_id;
    let status=req.params.status;

    //get data agenda scheduler from broadcast_id
    if(status=='pause'){
        let jobs = await agenda.jobs({'data.additional._id':ObjectId(broadcast_id) });
        if(jobs.length>0){
            jobs[0].attrs.data.status="Paused";
            await jobs[0].save();

            await BroadcastCall.findById(broadcast_id).then(async (val)=>{
                val.call_status="Paused";
                await val.save((err)=>{
                    if(err){
                        console.log(err);
                    }
                })
            }).catch(err=>{
                console.log(err);
            })
            return res.json({
                status:true,
                errors:{
                },data:{}
            }); 
        }else{
            return res.json({
                status:false,
                errors:{
                    message:"Cannot find scheduler"
                },data:{}
            }); 
        }
    }

    if(status=='start'){
            //re-schedule the broadcast scheduler to the next time 5 seconds from now
            //agenda create,
            // pre requistes
            // if only the status of scheduler is paused or finish
            let jobss = await agenda.jobs({'data.additional._id':ObjectId(broadcast_id) });
            if(jobss.length>0){
                jobss[0].attrs.data.status="Starting";
                await jobss[0].schedule(new Date(Date.now() + 5000));
                await jobss[0].save();
                await BroadcastCall.findById(broadcast_id).then(async (val)=>{
                    val.call_status="Processing";
                    await val.save((err)=>{
                        if(err){
                            console.log(err);
                        }
                    })
                }).catch(err=>{
                    console.log(err);
                })
                return res.json({
                    status:true,
                    errors:{
                    },data:{}
                }); 
            }else{
                return res.json({
                    status:false,
                    errors:{
                        message:"Cannot find scheduler"
                    },data:{}
                }); 
            }
    }

    if(status=='refresh'){

        let jobs = await agenda.jobs({'data.additional._id':ObjectId(broadcast_id) });
        if(jobs.length>0){
            try{
                
                jobs[0].attrs.data.status='Starting';
                await agenda.schedule('in 2 minutes', jobs[0].attrs.name, jobs[0].attrs.data);
                await jobs[0].remove();
                await BroadcastCall.findById(broadcast_id).then(async (val)=>{
                    val.call_status="Processing";
                    await val.save((err)=>{
                        if(err){
                            console.log(err);
                        }
                    })
                }).catch(err=>{
                    console.log(err);
                })
                return res.json({
                    status:true,
                    errors:{
                    },data:{}
                });
            }catch(err){
                console.log(err);
                return res.json({
                    status:false,
                    errors:{
                    },data:{}
                });
            }
 
        }else{
            return res.json({
                status:false,
                errors:{
                    message:"Cannot find scheduler"
                },data:{}
            }); 
        }
    }

}



