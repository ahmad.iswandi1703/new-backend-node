const mongoose = require('mongoose');
const BroadcastEmail=mongoose.model("EmailBroadcast");
const BroadcastSubscriber=mongoose.model("EmailSubscriber");
const uuid=require('uuid');
var moment = require('moment-timezone');

const {Pool, Client} =require('pg'); 

const job=require('../job/agenda-run-email.js');
const SettingController=require('../controllers/email-setting-controller.js');
exports.sendEmail=function(data){

 
    let DateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
      });

      //set broadcast email and run the job scheduler
    let Broadcast =new BroadcastEmail();
    Broadcast.broadcast_name=data.broadcast_name;
    Broadcast.broadcast_status="Preparation";
    Broadcast.broadcast_date=DateNow;
    Broadcast.broadcast_subscriber=0;
    Broadcast.created_at=DateNow;
    Broadcast.updated_at=DateNow;
    Broadcast.file_name=data.file_name;
    Broadcast.content_desc=data.content_desc;
    Broadcast.content_message=data.content_message;
    Broadcast.content_from=data.content_from;
    Broadcast.content_subject=data.content_subject;
    Broadcast.user_id=data.user_id;
    //run job from here
 
    Broadcast.save(async function(err){
        if (err) throw err;
        broadcast_id=Broadcast._id;

        //get setting data;
        try{
            let settingData=await SettingController.getEmailSetting(user_id);
            job.running(Broadcast,settingData);
        }catch(err){
            //update broadcast that this broadcast mail is broken
            console.log("there is no email setting");

            Broadcast.findById(Broadcast._id,function(err,br){
                if (err){
                    console.log(err);
                }
      
                br.broadcast_status="Failed";
           
                br.updated_at=new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Jakarta'
                  });
                
                br.save(function(err){
                    if (err) console.log(err);
                    console.log("success update sms broadcast");
                })
            })
        }
        
    });

}


exports.saveSubscriber=function(data){
    let subscriber=new BroadcastSubscriber();
    let DateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
    
    
    });


    subscriber.email=data.email,
    subscriber.name=data.name,
    subscriber.messsage=data.messsage,
    subscriber.status=data.status,
    subscriber.created_at=DateNow,
    subscriber.updated_at=DateNow,
    subscriber.broadcast=data.broadcast,
    subscriber.user_id=data.user_id

    return new Promise((resolve,reject)=>{
        subscriber.save(function(error){
            //send exception
            if(error){
                console.log(error);
                reject(error);
            }
            resolve({success:true});
        });
    })

}