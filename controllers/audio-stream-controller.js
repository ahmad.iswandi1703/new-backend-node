var fileSystem = require('fs');

const {Pool, Client} =require('pg'); 
const uuid=require('uuid');

const axios=require('axios');

exports.audio=async function(req,res){

    var audioName=req.params.audioName;
    console.log(audioName);
    //var filePath=__dirname+"../../../var/lib/freeswitch/recordings/178.128.215.77/"+audioName;
    var filePath=__dirname+"/../audio/"+audioName;
    //var filePath = __dirname+'/../Slank - Mars Slankers [nex4music.com].mp3';
    try{
      var stat = fileSystem.statSync(filePath);
    }catch(err){
      console.log(err);
    }
    

    res.writeHead(200, {
        'Content-Type': 'audio/wav',
        'Content-Length': stat.size
    });

    var readStream = fileSystem.createReadStream(filePath);
    await readStream.pipe(res);
}

exports.audioList=function(res,res){
    const pool = new Pool()

    pool.query('SELECT * FROM v_recordings WHERE id = $1', [1])
      .then(res => console.log('user:', res.rows[0]))
      .catch(e => setImmediate(() => { throw e }));

    pool.query('select * from a',[1],(err,resp)=>{
        if(err) throw new Error();
        console.log(resp);
    })
    pool.end();

    // (async () => {
    //     // note: we don't try/catch this because if connecting throws an exception
    //     // we don't need to dispose of the client (it will be undefined)
    //     const client = await pool.connect()
      
    //     try {
    //       await client.query('BEGIN')
    //       const { rows } = await client.query('INSERT INTO users(name) VALUES($1) RETURNING id', ['brianc'])
      
    //       const insertPhotoText = 'INSERT INTO photos(user_id, photo_url) VALUES ($1, $2)'
    //       const insertPhotoValues = [res.rows[0].id, 's3.bucket.foo']
    //       await client.query(insertPhotoText, insertPhotoValues)
    //       await client.query('COMMIT')
    //     } catch (e) {
    //       await client.query('ROLLBACK')
    //       throw e
    //     } finally {
    //       client.release()
    //     }
    //   })().catch(e => console.error(e.stack))
}


exports.saveAudio=async function(name,data){
        const pool = new Pool()
        const client = await pool.connect();
        try {
          await client.query('BEGIN')
          let id=uuid.v4();
          let aydu=uuid.v4();
          var nDate = new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
          });
          const insertAudio='INSERT INTO audio_recordings(audio_recording_uuid,recording_uuid,user_id,created_at,updated_at) VALUES($1,$2,$3,$4,$5)';
          const insertAudioValue=[aydu,id,data.user_id,nDate,nDate];
          const insertRecordings = 'INSERT INTO v_recordings(recording_uuid,domain_uuid,recording_filename,recording_name,recording_description) VALUES ($1, $2,$3,$4,$5)'
          const insertRecordingsValue = [id, '1668e7e6-cb68-4967-9da5-399684f7e465',name,data.audioName,data.audioName];
          await client.query(insertAudio,insertAudioValue);
          await client.query(insertRecordings, insertRecordingsValue);
          await client.query('COMMIT')
        } catch (e) {
          console.log(e);
          await client.query('ROLLBACK')
          throw e
        } finally {
          client.release();
          await pool.end();
        }

        
}

exports.getAudio=async function(req,res,next){
  try{
    //get url frfcom environment
    let postStatus=await axios.post(process.env.PHP_BACKEND+'/audio/list',req.body, {
      headers: {
          'Content-Type': 'application/json',
      }
    } );

    return res.json(postStatus.data);   
  }catch(err){
    //  console.log(err);
    return next(err);
  }
}

exports.getCommonAudio=async function(req,res,next){
 
  try{
    //get url frfcom environment
    let postStatus=await axios.get(process.env.PHP_BACKEND+'/common/audio_file', {
      headers: {
          'Content-Type': 'application/json',
      }
    } );
   

    return res.json(postStatus.data);   
  }catch(err){
    //  console.log(err);
    console.log(err);
    return next(err);
  }
}