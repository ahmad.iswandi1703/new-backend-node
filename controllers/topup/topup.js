const mongoose = require('mongoose');
const WaBilling=mongoose.model("WhatsappBilling");
const SmsBilling=mongoose.model("SmsBilling");
const User=mongoose.model("User");
const ObjectID=mongoose.Types.ObjectId;
var moment = require('moment-timezone');
let code={
    "BILL":"BILLING IS NOT EXIST",
    "USER":"USER IS NOT EXIST",
    "TYPE":"TYPE IS UNDEFINED"
}
exports.topup=async function(req,res,next){
    let user_id=req.body.user_id;
    let valueToken=req.body.topup;
    let type=req.body.type;

    let returnData={
        success:false,
        data:{},
        error:{
            code:"",
            message:""
        }
    }
    //check user if exist
    try{
        let user=await User.findById(user_id);

        if(user){
            switch(type){
                case "whatsapp":
                    let waQuota=await WaBilling.findOne({user:ObjectID(user_id),"quota.tipe":"limited"});
                    if(waQuota){
                        let remainQuota=waQuota.quota.token;
                        waQuota.quota.token=remainQuota + parseInt(valueToken);
                        waQuota.updated_at=moment();
                        await waQuota.save();


                        returnData.success=true;
                        returnData.data=waQuota;
                    }else{
                        returnData.error.code=code.BILL;
                    }

                    break;
                case "sms":
                    let smsQuota=await SmsBilling.findOne({user:ObjectID(user_id),"quota.tipe":"limited"});
                    if(smsQuota){
                        let remainQuota=smsQuota.quota.token;
                        smsQuota.quota.token=remainQuota + parseInt(valueToken);
                        smsQuota.updated_at=moment();
                        await smsQuota.save();
                        returnData.success=true;
                        returnData.data=smsQuota;

                    }else{
                        returnData.error.code=code.BILL;
                    }
                    break;
                default:
                    returnData.error=code.TYPE;
            }

        }else{
            returnData.error=code.USER;
           
        }
    }catch(err){
        console.log(err);
        returnData.success=false;
        returnData.error="INTERNAL SERVER ERROR";
    }

    return res.send(returnData);
    //check value is character or not
}

exports.getQuotaInfo=async function(req,res,next){

    let user_id=req.params.user_id;
    let type=req.params.type;
    let returnData={
        success:false,
        data:{},
        error:{
            code:"",
            message:""
        }
    }

    //check user if exist
    try{
        let user=await User.findById(user_id);
     
        if(user){
            switch(type){
                case "whatsapp":
                    let waQuota=await WaBilling.findOne({user:ObjectID(user_id)});
                    if(waQuota){
                        returnData.success=true;
                        returnData.data=waQuota;
                    }else{
                        returnData.error.code=code.BILL;
                    }

                    break;
                case "sms":
                    let smsQuota=await SmsBilling.findOne({user:ObjectID(user_id)});
                    if(smsQuota){
                        
                        returnData.success=true;
                        returnData.data=smsQuota;

                    }else{
                        returnData.error.code=code.BILL;
                    }
                    break;
                default:
                    returnData.error=code.TYPE;
            }

        }else{
            returnData.error=code.USER;
           
        }
    }catch(err){
        console.log(err);
        returnData.success=false;
        returnData.error="INTERNAL SERVER ERROR";
    }

    return res.send(returnData);
}