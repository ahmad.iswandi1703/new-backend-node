const mongoose = require('mongoose');
const Setting= mongoose.model('WhatsappBilling');
const BillingHistory=mongoose.model('WhatsappBillingHistory');
const ObjectId=mongoose.Types.ObjectId;
const uuid=require('uuid');

const moment=require('moment-timezone');
// character:{
//     tipe:{ type: String, lowercase: true, trim: true }, //limited,unlimited
//     length:{type:Number}
// },
// quota:{
//     tipe:{type:String}, //limited, unlimited
//     token:{Number}
// },
// expired:{
//     tipe:{type:String}, //limited, unlimited
//     date:{type:Date}
// },
// status:String, //enabled,disabled
// type:String, /// all user (default) specific user if all user (global, user);
// user_id:String,
// created_by:String,
// created_at:Date,
// updated_at:Date

exports.getQueryPagingSetting=function(req,res,next){

    let request_data="";
    // data: {user_id: "5c69e411f0f40003fcb4b828",quota:"",operator:"<"}
    // limit: 10
    // offset: 1
    // sort: {field: "created_at", sort: "desc"}

    

}
exports.setSetting=function(req,res,next){
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    let requiredForm=[{name:"type",required:true},{name:"user_id",required:true},{name:"created_by",required:true},{name:"status",required:true}];
    let check=checkParam(requiredForm,req.body);

    if(!check.passed){
        return res.json({
            status:false,
            errors:{
                code:"PRM01",
                message:check.dataReject[0]
            },
            data:{}
        })
    }

    let type=req.body.type;
    let user_id=req.body.user_id || "";
    let created_by=req.body.created_by;
    let status=req.body.status;
    let expiredType=req.body.expiredType || "unlimited"; //default limited;
    let quotaType=req.body.quotaType || "unlimited";
    let characterType=req.body.characterType || "unlimited";

    // if(expiredType!="unlimited" || expiredType!="limited"){
    //     //set to default
    //     expiredType="unlimited";
    // }

    // if(quotaType!="unlimited" || quotaType!="limited"){
    //     //set to default
    //     quotaType="unlimited";
    // }

    // if(characterType!="unlimited" || characterType!="limited"){
    //     //set to default
    //     characterType="unlimited";
    // }

   
    // if(type!="global" || type!="specific"){
    //     type="global";
    // }

    let characterCount=req.body.characterCount || 0;
    let quotaCount=req.body.quotaCount || 0;
    let expiredDate=req.body.expiredDate || new Date();
    let effective_date=req.body.effective_date ;
    //check if type limited
    if(characterType=="limited"){
        //need to get character length
        if(characterCount == 0){
            return res.json({
                status:false,
                errors:{
                    code:"INVLDPRM",
                    message:{
                        property:"characterCount",
                        error:"must be greater than 0"
                    },data:{}
                },
            })
        }
    }

    if(quotaType=="limited"){
        //need to get character length
        if(quotaCount == 0){
            return res.json({
                status:false,
                errors:{
                    code:"INVLDPRM",
                    message:{
                        property:"quotaCount",
                        error:"must be greater than 0"
                    },data:{}
                },
            })
        }
    }
    let parameter;

    if(type=="specific"){
        parameter={user:ObjectId(user_id)};
    }else{
        parameter={type:"global"};
    }

    //if type == user then check if user exist (if any user exist dont set data) 
    Setting.find(parameter).then((val)=>{
        //set data

        if(val.length>0){
            return res.json({
                status:false,
                errors:{
                    code:"SETTINGEXIST",
                    message:"Cannot set data, because setting already exist"
                },
                data:{}
            })
        }else{
            let data=new Setting();
            data.created_at=nDate;
            data.updated_at=nDate;
            data.created_by=created_by;

            if(type!="global"){
                data.user=user_id;
            }else{
                data.user=null;
            }
      
            data.type=type; //global or specific user
            data.status=status;
            data.character.tipe=characterType;
            data.character.length=characterCount;
            data.quota.tipe=quotaType;
            data.quota.token=quotaCount;
            data.expired.tipe=expiredType;
            let dNewNow1=moment(expiredDate);
            let newNa1=dNewNow1.clone().tz('Asia/Jakarta');
            data.expired.date=newNa1.format();

            let dNewNow=moment(effective_date);
            let newNa=dNewNow.clone().tz('Asia/Jakarta');
            data.effective_date=newNa.format();
        
            data.save(function(err){
                if (err) {
                    return next(err);
                }
                return res.json({
                    status:true,
                    errors:{
                    },
                    data:data
                })        
            })

        }

    }).catch(err=>{
            return res.json({
                status:false,
                errors:{
                    code:"DBERROR",
                    message:"Error In Db"
                },
                data:{}
            })    
    })
}

exports.getSettingBilling=function(req,res,next){
    let user_id=req.params.user_id;
    getSetting(user_id).then((val)=>{
        return res.json({
            status:true,
            errors:{
            },
            data:val
        })   
    }).catch(err=>{

        if(err=="setting not found"){
            return res.json({
                status:false,
                errors:{
                    code:"DATANOTFOUND",
                    message:"DATANOTFOUND"
                },
                data:{}
            }) 
        }else{
            return res.json({
                status:false,
                errors:{
                    code:"DBERROR",
                    message:"Error In Db"
                },
                data:{}
            }) 
        }

    })
}
exports.getBillingHistory=function(req,res,next){
    let user_id=req.params.user_id;
    console.log(user_id);

    let dataReturn={
        effective_date:"",
        quota:"",
        quotaType:"",
        delivered:0,
        failed:0,
    }
    // BroadcastCall.aggregate([
    //     {$match:{
    //         user_id:userID,
    //         created_at:{$gte:fromDate,$lt:toDate},
    //         call_status:'Finish'
    //     }},{$group:{
    //         _id:null,
    //         count:{$sum:1},
    //         total_contacting_call:{$sum:"$contact_count"},
    //         accountcode:{
    //             $addToSet:"$_id"
    //         }
    //     }}])
    //get setting and then get history data (failed and delivered); from date
    getSetting(user_id).then(async(val)=>{
        if(val){
            //
        
            
            let d=moment(val.effective_date).tz('Asia/Jakarta').toDate();
            let dateNow=moment().tz('Asia/Jakarta').add(1,'days').toDate();
            dataReturn.effective_date=val.effective_date;
            dataReturn.quotaType=val.quota.tipe;
            if(val.quota.tipe=="limited"){
                dataReturn.quota=val.quota.token
            }else{
                dataReturn.quota=0;
            }
           await  BillingHistory.aggregate([{
                $match:{
                    user_id:user_id,
                    created_at:{$gte: d,$lt:dateNow}
                }
            },{
                
                "$facet": {
                  "states": [
                    {
                      "$group": {
                        "_id": "$status",
                        "total": {
                          "$sum": 1
                        }
                      }
                    },
                    
                  ]
                }
              }]).then((val)=>{
                  let dataStates=val[0].states;
              
                  if(dataStates.length>0){
               
                    dataStates.map((valK)=>{
                     
                    
                        if(valK._id=="failed"){
                           
                            dataReturn.failed=valK.total;
                        }
                        if(valK._id=="delivered"){
                            
                            dataReturn.delivered=valK.total;
                        }
                    })
                 
                  }

            })


        }

        return res.json({
            status:true,
            errors:{},
            data:dataReturn
        })
    }).catch(err=>{
        console.log(err);
        return res.json({
            status:false,
            errors:{
                code:"DBERROR",
                message:"Error In Db"
            },
            data:{}
        }) 
    })

   
}
exports.getSmsSetting=getSetting;
function getSetting(user_id){
    return new Promise(function(resolve,reject){
        //check in the CallSetting if the user_id matches and type of setting is specific user if not then get default setting
        Setting.find({}).then(function(response){
            let userFind=response.filter(function(value){
                return value.user==user_id && value.type=="specific"
            });
            if(userFind.length>0){
                resolve(userFind[0]);
            }else{
                let defaultSetting=response.filter(function(value){
                    return value.type="global"; //this type all == default
                });
                if(defaultSetting.length>0){
                    resolve(defaultSetting[0]);
                }else{
                    reject("setting not found");
                }
            }
        }).catch(err=>{
            if (err){
                reject(err);
            }
        })
    });
}


function checkParam(requiredForm, data){

    let paramRejected=0;
    let dataReturn={
        passed:false,
        dataReject:[]
    }
    if(requiredForm.length >0){
        for(let i=0; i<requiredForm.length; i++){
            let form=requiredForm[i];
            if(data.hasOwnProperty(form.name)){
                if(data[form.name]=="" || data[form.name]==null || data[form.name]==undefined ){
                    if(form.required){
                        paramRejected++;
                        let infoReject={
                            property:"",
                            error:"",
                        };

                        infoReject.property=form.name;
                        infoReject.error="Property cannot be null";
                        dataReturn.dataReject.push(infoReject);
                    }
                }
            }else{

                if(form.required){
                    paramRejected++;
                    paramRejected++;
                    let infoReject={
                        property:"",
                        error:"",
                    };
                    infoReject.property=form.name;
                    infoReject.error="Property required";
                    dataReturn.dataReject.push(infoReject);
                }
            }
        }
    }

    if(paramRejected >0){
        dataReturn.passed=false;
    }else{
        dataReturn.passed=true;
    }

    
    return dataReturn;

}
    
    
