
let amqp = require('amqplib/callback_api');
let amqpurl= process.env.AMQP;

exports.sendBroadcastToMessageQueue=function(broadcastData,templateId,filename){
    amqp.connect(amqpurl, function(err, conn) {
        conn.createChannel(function(err, ch) {
          var q = 'wabroadcast';
          ch.assertQueue(q, {durable: true});

          let data={
              broadcast_data:broadcastData,
              filename:filename,
              template_id:templateId
          }

          let string=JSON.stringify(data);
          ch.sendToQueue(q, Buffer.from(string));
          console.log(" [x] Sent 'Broadcast Message!'");
        });
        setTimeout(function() { conn.close();}, 1500);
      });
}