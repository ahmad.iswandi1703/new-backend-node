const mongoose = require('mongoose');
const Setting= mongoose.model('WhatsappSetting');
const uuid=require('uuid');

const moment=require('moment-timezone');

let DateNow=moment().tz('Asia/Jakarta');

exports.setSetting=function(req,res,next){

    let setting=new Setting();
    // gateway_name:String,
    // gateway_host:String,
    // gateway_key:String,
    // gateway_secret:String,
    // gateway_number:String,
    // gateway_app_sid:String,
    // gateway_private_key_path:String,
    setting.gateway_name=req.body.gateway_name;
    setting.gateway_host=req.body.gateway_host;
    setting.gateway_key=req.body.gateway_key;
    setting.gateway_secret=req.body.gateway_secret;
    setting.gateway_number=req.body.gateway_number;
    setting.gateway_app_sid=req.body.gateway_app_sid;
    setting.gateway_private_key_path=req.body.gateway_private_key_path;
    setting.status=req.body.status;
    setting.type=req.body.type; /// all user(default), specific user
    setting.user_id=req.body.user_id;
    setting.function_name=req.body.function_name;
    setting.created_by=req.body.created_by;
    setting.updated_at=DateNow.format();
    setting.created_at=DateNow.format();

    setting.save(function(err){
        if (err) {
            return next(err);
        }
        return res.json({ success:"true",message:setting});     
    })
               

}

exports.getWhatsappSetting=function(user_id){
    return new Promise(function(resolve,reject){
        //check in the CallSetting if the user_id matches and type of setting is specific user if not then get default setting
        Setting.find({},function(err,response){
            if (err){
                reject(err);
            }

            let userFind=response.filter(function(value){
                return value.user_id==user_id && value.type=="specific"
            });

            if(userFind.length>0){
                resolve(userFind[0]);
            }else{
                let defaultSetting=response.filter(function(value){
                    return value.type="all"; //this type all == default
                });
                
                if(defaultSetting.length>0){
                    resolve(defaultSetting[0]);
                }else{
                    reject("setting not found");
                }
            }
        })
    });
}

    
    
