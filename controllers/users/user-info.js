const mongoose = require('mongoose');
let ObjectId=mongoose.Types.ObjectId
const passport = require('passport');
const User=mongoose.model('User');

module.exports={
    userByOffice:function(officeID){


        let param={};
      

        return new Promise(async(resolve,reject)=>{
            let returnData={
                status:false,
                data:null,
            }
            await User.find({"office":ObjectId(officeID)}).sort({created_at:1})
            .then(response => {

                if(response.length >0){
                    returnData.status=true;
                    returnData.data=response;
                }
               
            })
            .catch(function(err){
                //handle query error;
                // set loggin
                return reject(returnData);
            });
            return resolve(returnData);
        })

    },
    userDetail:function(userID){
        let id=userID;
        return new Promise(async (resolve,reject)=>{
            let returnData={
                status:false,
                data:null,
            }
     
            await User.findById(id).populate('office')
            .then(response => {
                
                if(!isEmpty(response)){
                    returnData.status=true;
                    returnData.data=response;
                }
            })
            .catch(function(err){
               
                //handle query error;
                // set loggin
                return reject(returnData);
            });
          
            return resolve(returnData);
        })
    }
}


function isEmpty(data){
    for(var key in data) {
        if(data.hasOwnProperty(key))
            return false;
    }
    return true;
  
}