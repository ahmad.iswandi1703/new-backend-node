var mongoose=require('mongoose');
var ObjectId=mongoose.Types.ObjectId;
var Setting=mongoose.model('OcallMonSetting');
var User=mongoose.model('User');
var Subscriber=mongoose.model('BroadcastSubscriber');

var BroadcastCall=mongoose.model('OCallMon');
const BroadcastCallMaster=mongoose.model('BroadcastCall');
const moment=require('moment-timezone');
var arrayToTree = require('array-to-tree');
var ObjectId = require('mongoose').Types.ObjectId; 
let DateNow=moment().tz('Asia/Jakarta');

var csv=require('csvtojson');
var parseData=require('../lib/parse-number');
const uuid=require('uuid');

const SettingController=require('../controllers/setting-controller');



exports.createSetting=function(req,res,next){
    var setting=new Setting();
    setting.ivr_survey_id=req.body.ivr_survey_id;
    setting.ivr_survey_name=req.body.ivr_survey_name;
    setting.ivr_menu_options=req.body.ivr_menu_options;
    setting.status="active";
    setting.user_id=req.body.user_id
    setting.call_per_campaign=req.body.call_per_campaign;

    setting.created_at=DateNow.format();
    setting.updated_at=DateNow.format();
    setting.save(function(err){
        if (err) {
            return next(err);
        }
        //update all setting user_id==user_id and not equal id of new setting
        Setting.updateMany({user_id:setting.user_id,_id:{$ne:ObjectId(setting._id)}},{status:"in_active"}).exec((err,res)=>{
            if(err){
                console.log(err);
            }
        })

        return res.json({ success:"true",message:setting});  
        
    })
}

exports.getSetting=async function(req,res,next){
    var user_id=req.params.user_id;
    var status=req.params.status;
    //doing validation for body request
    try{
        let setting=await getSetting(user_id,status);
        return res.json({ success:"true",message:setting});     
    

    }catch(err){
        return next(err);
    }
}

exports.makeCampaign=async function(req,res,next){

    const KODE={
        ERROR_GENERAL_SETTING:"ERROR_GENERAL_SETTING",
        ERROR_IN_FORM:"ERROR_IN_FORM",
        ERROR_IN_DB:"ERROR_IN_DB",
        SUCCESS:"SUCCESS"
    }
    let number=req.body.phone_number;
    let campaign_name=req.body.campaign_name;
    let user_id=req.body.user_id;
    let dateNow=moment().tz('Asia/Jakarta');

    let returnDataSuccess={
        error_number_format:0,

    }

    if(campaign_name==null || campaign_name=="" || campaign_name=="empty"){
        campaign_name="campaign in "+dateNow.toString();
    }

    if(number==null || number==""){
        return res.json({ status:"false",
        errors:{
            message:"cannot find any number",
            code:KODE.ERROR_IN_FORM
        },
        data:null}); 
    }


    //get setting
    try{    
        let setting=await getSetting(user_id,'active');
        let collectPhoneNumber=[];
        let phoneNumberSum=0;
        let errorNumberSum=0;
        if(setting){
            console.log(setting);
            //parse and Count the number of phone;
            const converter=csv({
                noheader:true,
                trim:false,
                delimiter:";",
                quote:"off"
            });
          
            await converter.fromString(number).on("data",async function(data){
                const jsonStr= data.toString('utf8')
                const json=JSON.parse(jsonStr);
               
                let colDesc=["Nomor","Nama"];
                parseData(json,"field",colDesc,async (err,returnData)=>{
                    
                    if(err){
                        errorNumberSum++;
                    }else{
                        collectPhoneNumber.push(returnData.phone);
                    }
                    phoneNumberSum++;
                });
            });

            if(phoneNumberSum>setting.call_per_campaign){
                return res.json({ status:"false",
                errors:{
                    message:"Phone number is too many, it is only "+setting.call_per_campaign+" numbers per campaign",
                    code:KODE.ERROR_IN_FORM
                },
                data:null}); 
            }else if(phoneNumberSum==errorNumberSum){

                return res.json({ status:"false",
                errors:{
                    message:"Error in all Phone Number",
                    code:KODE.ERROR_IN_FORM
                },
                data:null}); 
            }
            else{

                SettingController.getCallSettingByUser(user_id).then(async function(val){
                    // if (err){
                    //     next(err);
                    // }
                    
                    if(!val){

                        return res.json({ status:"false",
                        errors:{
                            message:"There is no setting call please call administrator ",
                            code:KODE.ERROR_GENERAL_SETTING
                        },
                        data:null}); 
                      
                    }
                    let call_setting_id=val.setting._id;

                    await saveBroadcastMaster(campaign_name,setting.ivr_survey_id,val.setting._id).then(async (idMaster)=>{
                        //set data request to Database
                        let call=new BroadcastCall();
                        console.log(idMaster);
                        call.uid=idMaster.uuid;
                        call.presence_id=idMaster.id;
                        call.campaign_name=campaign_name; //this campaign id
                        call.application=setting.ivr_survey_id;
                        call.account_code=idMaster.id;
                        call.call_setting=val._id;
                        //Preparation, Starting , Proggress, Finish, Error
                        call.call_status="running";
                        call.user_id=user_id;
                        var nDate = moment().tz('Asia/Jakarta'); 
                        call.created_at=nDate.format();
                        call.updated_at=nDate.format();
                        call.number=collectPhoneNumber;
                        //get concurent limit from setting
                        try{
                            await call.save(async function(err){
                                if(err) {throw(err)};
                                //save data number to broadcast;
                                let successInsert=0;
                                let errorInsert=0;
                                await Promise.all(collectPhoneNumber.map(async function(val){
                                    //save data to db
                                    try{
                                        let insertSubcriber=await insertSubscriber({phone:val},idMaster.id,idMaster.uuid,call_setting_id);
                                        successInsert+=1;
                                    }catch(err){
                                        errorInsert+=1;
                                        console.log(err);
                                    }
                                }))

                                return res.json({ status:"true",errors:{},message:"success make campaign",success:successInsert,error:errorInsert});

                            })
                        }catch(err){

                            return res.json({ status:"false",
                            errors:{
                                message:"Error When Insert TO Db",
                                code:KODE.ERROR_IN_DB
                            },
                            data:null}); 
                        }
                       
                    }).catch(err=>{
                        return res.json({ status:"false",
                        errors:{
                            message:"Error When Insert TO Db",
                            code:KODE.ERROR_IN_DB
                        },
                        data:null}); 
                    })

                }).catch((err)=>{
                    //send request error data setting
                    return next(err);
                })
            }
        }else{
            return res.json({ status:"false",
            errors:{
                message:"There is no setting call please call administrator ",
                code:KODE.ERROR_GENERAL_SETTING
            },
            data:null}); 
        }
    }catch(err){
        return res.json({ status:"false",
        errors:{
            message:"There is no setting call please call administrator ",
            code:KODE.ERROR_GENERAL_SETTING
        },
        data:null}); 
    }
}

exports.getLastCall=async function(req,res,next){

    let user_id=req.params.id;

    let lastOcall;
    let subscriber;
    let returnData={
        status:"",
        data:"",
    }
    try{
        lastOcall=await getLastOcall(user_id);

        if(isEmpty(lastOcall)){
            returnData.status="ready";
        }else{
            returnData.status="lock";
        }

        returnData.data=lastOcall;
        return res.json({ status:"true",errors:{},data:returnData});

    }catch(err){
        console.log(err);
        returnData.status="ready";
        lastOcall={};
        returnData.data=lastOcall;
        return res.json({ status:"false",
        errors:{
            message:"data not found",
            code:"ERR"
        },
        data:null}); 
    }   
}

function saveBroadcastMaster(campaign_name,ivr,setting){
    return new Promise((resolve,reject)=>{
        let call=new BroadcastCallMaster();
        call.uid=uuid.v4();;
        call.presence_id=call.uid;
        call.campaign_name=campaign_name; //this campaign id
        call.application=ivr;
        call.account_code=call.uid;
        call.call_setting=setting;
        call.call_status="one_app_call";
        call.save(async function(err){
            if(err){reject(err)};
            resolve({id:call._id,uuid:call.uid});
        })
    })
}


var insertSubscriber=function(subscriberData,broadcast,uid,call_setting_id){
    let dateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
    });
    return new Promise(function(resolve,reject){
        var subscriber=new Subscriber();
        subscriber.phone=subscriberData.phone;
        subscriber.created_at=dateNow
        subscriber.updated_at=dateNow
        subscriber.broadcast=broadcast;
        subscriber.uuid=uid;
        subscriber.tryCall=0;
        subscriber.call_setting=call_setting_id;
        subscriber.status='in_queue';//(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
        subscriber.save(function(err){
            if(err){
                console.log(err);
            }
            resolve("success");
        })
    }) 
 }

function getSetting(user_id,status){
    return new Promise((resolve,reject)=>{
        Setting.findOne({user_id:user_id,status:status}).sort({created_at:-1}).exec(function(err,setting){
            if (err) {
                reject(err);
            }
            
            resolve(setting)   
        })

    })
}

function getSubscriber(broadcast){
    return new Promise((resolve,reject)=>{
        Subscriber.find({broadcast:broadcast}).sort({created_at:1}).exec(function(err,setting){
            if (err) {
                reject(err);
            }
            
            resolve(setting)   
        })

    })
}


function getLastOcall(user_id){
    return new Promise(function(resolve,reject){
        BroadcastCall.findOne({call_status:"running",user_id:user_id}).sort({created_at:-1}).then(function(val){
            resolve(val);
        }).catch(err=>{
            reject(err);
        });
    });
}


function isEmpty(data){
    for(var key in data) {
        if(data.hasOwnProperty(key))
            return false;
    }
    return true;
  
}