const axios=require('axios');
const mongoose=require('mongoose');
const moment=require('moment-timezone');
var arrayToTree = require('array-to-tree');
var ObjectId = require('mongoose').Types.ObjectId; 
let DateNow=moment().tz('Asia/Jakarta');
var ContactList=mongoose.model("BroadcastContactList");
const Json2csvParser=require('json2csv').Parser;

const converter = require('json-2-csv');


const Office= mongoose.model('Office');
const User=mongoose.model('User');
const BroadcastCall=mongoose.model('BroadcastCall');
const BroadcastSms=mongoose.model('SmsBroadcast');
const SmsSubscriber=mongoose.model('Subscriber');
var ContactGroup=mongoose.model("BroadcastGroup");
const {Pool, Client} =require('pg'); 
//grab all data from php script 

//this function is aimed to get call detail records there are just two status SUCCESS (NORMAL_CLEARING) 
//and FAILED CALLED (IS NOT SAME AS ANSWER)
exports.getSuccessRate=async function(req,res,next){
    //collect data parameter (office id) 
    let officeID=req.body.officeID;
    let startDate=req.body.startDate;
    let endDate=req.body.endDate;

    // check if office existc

    let officeExist=await checkOffice(officeID);

    let data=[];

    let userModel={
        data:{},
        call:[],
    }
    // console.log(officeExist);
    if(officeExist.success){
        let dataBranch=await getDataBranch(officeID);
        if(dataBranch.success){
            await Promise.all(dataBranch.data.map(async function(val){
                let dataModel={
                    branch:{},
                    user:[]
                }

                dataModel.branch=val;
                let dataUser=await getDataUser(val._id);

                if(dataUser.success){
                    await Promise.all(dataUser.data.map(async function(uVal){
                        // userModel.data={};
                        // userModel.call=[];
                        // userModel.data=uVal;
                        //get data call from each user
                        let dataCall=await getCall(uVal._id,startDate,endDate);
                        let call;
                        if(dataCall.success){
                            call=dataCall.data;
                        }else{
                            call=[];
                        }

                        dataModel.user.push({data:uVal,call:call});
                    }))
                }
                data.push(dataModel);
            }));

            //get call detail report data from php backend
     
            let callDetailResults=await getCallDetailRecord(data,startDate,endDate);

            return res.json({success:true, message:callDetailResults})
           
        }else{
            return res.json({ success:false,message:"cannot find branch office"}); 
        }
    }else{
        return res.json({ success:false,message:"cannot find office"});     
    }



}

exports.getOfficeBranch=async function(req,res,next){
    let officeID=req.params.officeID;
    //set for sample using office ID consumer

    let dataBranch=await getDataBranch(officeID);
    if(dataBranch.success){
        return res.json({ success:true,message:dataBranch.data});  
    }else{
        return res.json({ success:false,message:"cannot find branch office"});     
    }
}



exports.getCallReport=async function(req,res,next){
    //get all call detail report (ANSWER, NO ANSWER, AND OTHERS);
    let userID=req.body.userID;
    let startDate=req.body.startDate;
    let endDate=req.body.endDate;
    let status=req.body.status;
    let call;
    let callDetailResults;


    if(status=="byDate"){
        try{
            let dataCall=await getCall(userID,startDate,endDate);
            if(dataCall.success){
                call=dataCall.data;
            }else{
                call=[];
            }
        }catch(err){

        }        
    }else{
        try{
            let dataCall=await getCallByUserID(userID);
           
            if(dataCall.success){
                call=dataCall.data;
            }else{
                call=[];
            }
        }catch(err){

        }
        
    }   


    //get result
    //get call detail report data from php backend
    callDetailResults=await getAllCallDetailRecord(call,startDate,endDate,status)
    return res.json({success:true, message:callDetailResults})
}


exports.getSmsReport=async function(req,res,next){
    //
    let userID=req.body.userID;

    let broadcastSms=new Promise(function(resolve,reject){
        BroadcastSms.find({user_id:userID}).then(val=>{
            resolve(val);
        }).catch(err=>{
            reject(err);
        })
    });

    let getDeliveredSms=function(broadcastId){
        return new Promise(function(resolve,reject){
            SmsSubscriber.countDocuments({broadcast:ObjectId(broadcastId),status:"delivered"}).exec().then(val=>{
                resolve(val);
            }).catch(err=>{
                reject(err);
            })
        });
    }

    let getFailedSms=function(broadcastId){

        return new Promise(function(resolve,reject){
            SmsSubscriber.countDocuments({broadcast:ObjectId(broadcastId),status:{$ne:"delivered"}}).exec().then(val=>{
                resolve(val);
            }).catch(err=>{
                reject(err);
            })
        });
    }

    //output data{
    //     delivered:0,
    //     failed:0,
    //     total_sms:0
    // }
    //get sms broadcast
    let dataResponse={
        total_delivered:0,
        total_failed:0,
        total_sms:0
    }
    try{
        let dataBroadacst=await broadcastSms;
       
        //count total subscriber
        let total_sms=0;
        let total_delivered=0;
        let total_failed=0;

        if(dataBroadacst.length>0){
            await Promise.all(dataBroadacst.map(async function(dt){
                let totalDelivered=await getDeliveredSms(dt._id);

                let totalFailed=await getFailedSms(dt._id);

                total_sms+=dt.broadcast_subscriber;
                total_delivered+=totalDelivered;
                total_failed=totalFailed;
            }))

            dataResponse.total_delivered=total_delivered;
            dataResponse.total_failed=total_failed;
            dataResponse.total_sms=total_sms;
        }
        
    }catch(err){
        console.log(err);
        return res.json({success:false, message:"err"})
    };

    return res.json({success:true, message:dataResponse})

}

exports.getWaReport=async function(req,res,next){

}
//this method is aim to get total call, sms, and whatsapp
exports.getTotalTransaction=async function(req,res,next){


}

//get all report survey

exports.getReportSurvey=async function(req,res,next){
    let userID=req.body.user_id;
    let ivr_menu_uid=req.body.ivr_menu_uid;

    let getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.find({user_id:userID,application:ivr_menu_uid}).then((val)=>{
            resolve(val);
        }).catch(err=>{
            reject(err);
        })
    })

    let dataResponse={
        data_survey:"",
        total_contact:"",
        account_code:""
    }

    try{
        let broadcastData=await getBroadcastCall;

        if(broadcastData.length>0){
            let collectAccountCode=await broadcastData.map(val=>{
                return "'"+val._id+"'";
            })
            let collectAccountCode1=await broadcastData.map(val=>{
                return val._id;
            })
      
            
            let dataSurvey=await getSurveyReport(ivr_menu_uid,collectAccountCode);

            var totalCall = broadcastData.reduce(function (accumulator, dt) {
                return accumulator + dt.contact_count;
              }, 0);
            //query database pgsql

            dataResponse.total_contact=totalCall;
            dataResponse.data_survey=dataSurvey;
            dataResponse.account_code=collectAccountCode1;

        }
    }catch(err){
        console.log(err);
        return res.json({success:false, message:"err"})
    }

    return res.json({success:true, message:dataResponse})

    //get all call that have userID and ivr_menu uid same as request
    //fet
}
exports.download=async function(req,res){

    let broadcast_call_id=req.body.broadcast_call_id.map(val=>{
        return "'"+val+"'";
    });

    
    let option_survey_id=req.body.option_survey_id;
    const pool = new Pool();
    const client=await pool.connect();

    

    var getBroadcastCall=function(broadcast_call_id){
        return new Promise(function(resolve,reject){
            BroadcastCall.findById(broadcast_call_id,function(err,res){
                resolve(res);
            })
        })
    }


    //get report from v_ivr_survey_report by ivr_menu_option_uuid and call_broadcast_uuid;
    var newQuery="";
    newQuery+="SELECT * FROM v_ivr_survey_report";
    newQuery+=" where call_broadcast_uuid in ("+broadcast_call_id.toString()+")";
    newQuery+=" and ivr_menu_option_uuid='"+option_survey_id+"'";
    newQuery+=" order by created_at asc";
    let ivrSurvey;
    let ivrMenu;
    let data=[];
    try{
        ivrSurvey =await client.query(newQuery);
        var newQuery="";
        newQuery+="SELECT * FROM v_ivr_menu_options";
        newQuery+=" where ivr_menu_option_uuid=$1";
        ivrMenu=await client.query(newQuery, [option_survey_id]);

    //get contact list by number phone contact group(get from broadcastCall);


        await Promise.all(ivrSurvey.rows.map(async (val)=>{
        
            try{

                let destiNumber=phoneFormat(val.destination_number);
                let dataBroadcast=await getBroadcastCall(val.call_broadcast_uuid);
            // console.log(destiNumber);
                let contact=await ContactList.findOne({phone:destiNumber,contact_group:dataBroadcast.phonebook}).exec();
                if(contact){
                
                    let dataParse=JSON.parse(JSON.stringify(contact.content_data));
                    let dataContact=await Object.keys(dataParse);
                    let dataDesc={};
                //broadcastName
                    //broadcastDate
                    dataDesc["broadcast_name"]=dataBroadcast.campaign_name;
                    dataDesc["nomor"]=val.destination_number;
                    dataDesc["name"]=contact.name;
                    dataDesc["additional_data"]=JSON.stringify(contact.content_data);
                    let newDate=moment(dataBroadcast.created_at).format('DD/MM/YYYY');
                    dataDesc["broadcast_date"]=newDate;
                    data.push(dataDesc);
                }
            }catch(err){
                console.log(err);
                res.send({success:false,message:"error"});
                console.log("cannot fetch contact list");
            }
        }))
        var getContactList="";
    }catch(err){
        console.log(err);
        console.log("cannot find ivr survey")
    }



    

    // var cursor=Subscriber.find(query).exec(function(err,response){
    //     res.send(response);
    // });

    let nameOfFile;
    if(ivrMenu.rows.length>0){
        nameOfFile='Result_Survey_of_'+ivrMenu.rows[0].ivr_menu_option_description.replace(/[ ,.]/g,'_')+'.csv';
    }else{
        nameOfFile='Result_Survey.csv';
    }

    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename='+nameOfFile);
    res.set('Content-Type', 'text/csv');
    res.type('blob');

    // const fields=["data"];
    
    // const json2csvParser = new Json2csvParser({ fields });
    // const csv = json2csvParser.parse(data);
    let json2csvCallback = function (err, csv) {
       
        res.send(csv);
    };

    converter.json2csv(data, json2csvCallback, {
        prependHeader: true      // removes the generated header of "value1,value2,value3,value4" (in case you don't want it)
    });
   
}

var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}

async function getSurveyReport(application,accountcode){
    const pool = new Pool();
    const client=await pool.connect();
    //data model to be send
    //get data from cdr where call hangup == no answer
    let dataPush=[];
    try{
        var newQuery="";
        newQuery+="SELECT * FROM v_ivr_survey_pivot";
        newQuery+=" where uuid=$1";
        newQuery+=" order by created_at asc";
 
        const ivrSurvey =await client.query(newQuery, [application]);


       // console.log(ivrSurvey.rows);

        await Promise.all(ivrSurvey.rows.map(async function(val){
            try{
    
               
                var newQuery="";
                newQuery+="SELECT * FROM v_ivr_menus";
                newQuery+=" where ivr_menu_uuid=$1";
                newQuery+=" and ivr_type='MULTI_CHOICE'";
                let ivrMenu=await client.query(newQuery, [val.ivr_menu_uuid]);
                


                if(ivrMenu.hasOwnProperty('rows')){
                    await Promise.all(ivrMenu.rows.map(async function(data){
                        let dataObject={menu_name:'',menu_id:'',survey:[]};
                        dataObject.menu_name=data.ivr_menu_description;
                        dataObject.menu_id=data.ivr_menu_uuid
                        var query="";
                        //join with 
                        query+="SELECT v_ivr_menu_options.*,coalesce(options.jml,0) as survey_count  FROM v_ivr_menu_options";
                        query+=" left join (select ivr_menu_option_uuid, count(*) as jml ";
                        query+="from v_ivr_survey_report where call_broadcast_uuid in ("+accountcode.toString()+") group by ivr_menu_option_uuid)";
                        query+=" options on CAST(options.ivr_menu_option_uuid as uuid)=v_ivr_menu_options.ivr_menu_option_uuid";
                        query+=" where v_ivr_menu_options.ivr_menu_uuid=$1";
                
                        try{
                            let ivrOptions=await client.query(query,[data.ivr_menu_uuid]);
                            if(ivrOptions.hasOwnProperty('rows')){
                                dataObject.survey=ivrOptions.rows;
                            }
                        }catch(err){
                            console.log("error here");
                            console.log(err);
                        }

                        dataPush.push(dataObject);
                    }))
                }

            }catch(err){
                return {succes:false,data:false};
                console.log(err);
            }
        }))

    }catch(err){
        return {succes:false,data:false};
        console.log(err);
        console.log("cannot fetch data call answers3");

    }

    return {succes:true,data:dataPush};
}

async function checkOffice(officeID){

    let dataReturn={success:false,data:{}};
    let getOffice= new Promise(function(resolve,reject){
        Office.findById(officeID,function(err,res){
            if(err){
                return reject(err);
            }

            resolve(res);
        })
    })

    await getOffice.then(function(val){
    
        if(val!=null){
            dataReturn.success=true;
            dataReturn.data=val;
        }else{
            dataReturn.success=false;
            dataReturn.data=[];
        }
       
    }).catch(err=>{
        dataReturn.success=false;
        dataReturn.data=[];
      
    })  
    return dataReturn;
    

}

async function getDataBranch(parentID){
    //check if have children or not
    //return data
    let dataReturn={success:false,data:{}};
    let getChildren= new Promise(function(resolve,reject){
        Office.find({parents:ObjectId(parentID)},function(err,res){
            if(err){
                return reject(err);
            }
            resolve(res);
        })
    })

    await getChildren.then(function(val){

        if(val.length>0){
            dataReturn.success=true;
            dataReturn.data=val;
            
        }else{
            dataReturn.success=false;
            dataReturn.data=[];
        }
        
    }).catch(err=>{
        dataReturn.success=false;
        dataReturn.data=[];
       
    })
    return dataReturn;

}

async function getDataUser(officeID){
    let dataReturn={success:false,data:{}};
    let getUser= new Promise(function(resolve,reject){
        User.find({office:ObjectId(officeID)},function(err,res){
            if(err){
                return reject(err);
            }
            resolve(res);
        })
    })
   await getUser.then(function(val){

        if(val.length>0){
            dataReturn.success=true;
            dataReturn.data=val;
            
        }else{
            dataReturn.success=false;
            dataReturn.data=[];
        }
       
    }).catch(err=>{
        dataReturn.success=false;
        dataReturn.data=[];
   
    })
    return dataReturn;
}

//getCall by user id and start date (using start date because we do not know when the call will finish)
async function getCall(userID, startDate,endDate){
    let startDates=moment(startDate).format();
    let endDates=moment(endDate).format();

    let dataReturn={success:false,data:{}};
    var getDataCall= new Promise(function(resolve,reject){
        //get contact List
        BroadcastCall.find({user_id:userID, created_at:{"$gte": startDates, "$lt": endDates}}).then(function(res){
            resolve(res);
        }).catch(function(err){
            reject(err);
        })
    })

    await getDataCall.then(function(val){

        if(val.length>0){
            dataReturn.success=true;
            dataReturn.data=val;
            
        }else{
            dataReturn.success=false;
            dataReturn.data=[];
        }
        
    }).catch(err=>{
        dataReturn.success=false;
        dataReturn.data=[];
       
    })

    return dataReturn;
}

async function getCallByUserID(userID){

    let dataReturn={success:false,data:{}};
    var getDataCall= new Promise(function(resolve,reject){
        //get contact List
        BroadcastCall.find({user_id:userID}).then(function(res){
            resolve(res);
        }).catch(function(err){
            reject(err);
        })
    })

    await getDataCall.then(function(val){
        
        if(val.length>0){
            dataReturn.success=true;
            dataReturn.data=val;
            
        }else{
            dataReturn.success=false;
            dataReturn.data=[];
        }
        
    }).catch(err=>{
        dataReturn.success=false;
        dataReturn.data=[];
       
    })

    return dataReturn;
}
//getCall by accountCode and start date and endDate

//fetch data from php backend using AXIOS library 
function getCallDetailRecord(dataBranch, startDate, endDate){
    let startDates=moment(startDate).format();
    let endDates=moment(endDate).format();
    //return data
    return new Promise(async function(resolve, reject){
        let dataReturn=[];
        await Promise.all(dataBranch.map(async function(val){

            let dataModelReturn ={
                branch:{},
                success_call:0,
                failed_call:0,
                success_rate:0,
                total_contact:0,
            }
            dataModelReturn.branch=val.branch;
            if(val.user.length>0){
                //request to php backend 
                await Promise.all(val.user.map(async function(uData){
                    if(uData.call.length>0){
                        //get account_code and send request to php backend 
                        //php backend will return data like this
                        // {   
                        //     account_code:"",
                        //     success_call:"",
                        //     failed_call:"",
                        // }
                        await Promise.all(uData.call.map(async function(cData){
                            //count total_contact
                            dataModelReturn.total_contact=dataModelReturn.total_contact+cData.contact_count;
                            await postStatus(cData._id,startDates,endDates).then(function(resData){
                               
                                if(resData.data){
                                    dataModelReturn.success_call=dataModelReturn.success_call+resData.data.success_call;
                                    dataModelReturn.failed_call=dataModelReturn.failed_call+resData.data.failed_call;
    
                                }
                             
                            }).catch(err=>{
                                
                                console.log("error request to php backend");
                            })
                        }))
                        
                    }
                }))
    
                //jika jumlah success_call + failed tidak sama dengan total contact > cari selisih dan tambahkan ke failed call
                //ini dikarenakan broadcast contact tidak tercatat ke call detail records atau ada terjadi error (status call finish) namun tidak ada telpon yang keluar di call detail records
                let total=dataModelReturn.success_call+dataModelReturn.failed_call;
                let selisih=dataModelReturn.total_contact-total;
                dataModelReturn.failed_call=dataModelReturn.failed_call+selisih;

                //cari success rate setiap branch nya
                let success_rate=(dataModelReturn.success_call/dataModelReturn.total_contact) * 100;
                dataModelReturn.success_rate=Math.round(success_rate);
  
                dataReturn.push(dataModelReturn); 
                //hitung succes rate dari total contact dibagi dengan success calls;
                //dataModelReturn.success_rate;
            }else{
                dataModelReturn.success_call=0;
                dataModelReturn.failed_call=0;
                dataModelReturn.success_rate=0;
                
               
                dataReturn.push(dataModelReturn); 
       
        
            }
            
        }))


        resolve(dataReturn);
    })


    // let dataReturn={success:false,data:{}}

    // //result must be like this (per account code)
    // // success_call


}

function getAllCallDetailRecord(dataCall, startDate, endDate,status){
    let startDates=moment(startDate).format();
    let endDates=moment(endDate).format();
    //return data
    return new Promise(async function(resolve, reject){
        let dataReturn=[];

        let dataModelReturn ={
            success_call:0,
            no_answer:0,
            others:0,
            total_contact:0,
        }
        if(dataCall.length>0){
            //get account_code and send request to php backend 
            //php backend will return data like this
            // {   
            //     success_call:"",
            //     no_answer:"",
            //     others:"",
            // }

            await Promise.all(dataCall.map(async function(cData){
                //count total_contact
         
                dataModelReturn.total_contact=dataModelReturn.total_contact+cData.contact_count;
                    await allCallDetail(cData._id,startDates,endDates,status).then(function(resData){
                        if(resData.data){
                            dataModelReturn.success_call=dataModelReturn.success_call+resData.data.success_call;
                            dataModelReturn.no_answer=dataModelReturn.no_answer+resData.data.no_answer;
                            dataModelReturn.others=dataModelReturn.others+resData.data.others;
                        }
                    }).catch(err=>{
                        
                        console.log("error request to php backend");
                    })
            }))
            

            //jika jumlah success_call + failed tidak sama dengan total contact > cari selisih dan tambahkan ke others call
            //ini dikarenakan broadcast contact tidak tercatat ke call detail records atau ada terjadi error (status call finish) namun tidak ada telpon yang keluar di call detail records
            let total=dataModelReturn.success_call+dataModelReturn.no_answer+dataModelReturn.others;
            let selisih=dataModelReturn.total_contact-total;
            dataModelReturn.others=dataModelReturn.others+selisih;
            //cari success rate setiap branch nya
            //hitung succes rate dari total contact dibagi dengan success calls;
            //dataModelReturn.success_rate;
        }

            resolve(dataModelReturn);
    })


    // let dataReturn={success:false,data:{}}

    // //result must be like this (per account code)
    // // success_call


}




function postStatus(accountCode,startDate,endDate){
    return new Promise(async function(resolve,reject){
        let dataRequest=await axios.post(process.env.PHP_BACKEND+'/call/success-rate', {account_code:accountCode,start_date:startDate,end_date:endDate}, {
            headers: {
                'Content-Type': 'application/json',
            }
        } );

        resolve(dataRequest);
    })
}

function allCallDetail(accountCode,startDate,endDate,status){
    return new Promise(async function(resolve,reject){
        let dataRequest=await axios.post(process.env.PHP_BACKEND+'/call/all-call-record', {account_code:accountCode,start_date:startDate,end_date:endDate,status:status}, {
            headers: {
                'Content-Type': 'application/json',
            }
        } );

        resolve(dataRequest);
    })
}















