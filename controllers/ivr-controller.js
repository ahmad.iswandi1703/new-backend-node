const axios=require('axios');
const mongoose=require('mongoose');
const moment=require('moment-timezone');
var arrayToTree = require('array-to-tree');
var ObjectId = require('mongoose').Types.ObjectId; 
let DateNow=moment().tz('Asia/Jakarta');
const IvrSubscriber=mongoose.model('IvrSubscriber');
exports.addIvr=async function(req,res,next){
    try{
        //get url frfcom environment
        //get url frfcom environment
        let postStatus=await axios.post(process.env.PHP_BACKEND+'/ivr-survey', req.body, {
            headers: {
                'Content-Type': 'application/json',
            }
        } );
        return res.json(postStatus.data);   
    }catch(err){
      //  console.log(err);

      return next(err);
  
    }
}

exports.ivrSurveyEdit=async function(req,res,next){
    try{
        //get url frfcom environment
        let postStatus=await axios.post(process.env.PHP_BACKEND+'/ivr-survey-edit', req.body, {
            headers: {
                'Content-Type': 'application/json',
            }
        } );


        return res.json(postStatus.data);   

    }catch(err){
      //  console.log(err);
      return next(err);
    }
}

exports.getUUID=async function(req,res,next){
    try{
        //get url frfcom environment
        let postStatus=await axios.get(process.env.PHP_BACKEND+'/common/uuid', {
            headers: {
                'Content-Type': 'application/json',
            }
        } );


        return res.json(postStatus.data);   

    }catch(err){
      //  console.log(err);
      return next(err);
    }
}

exports.getIvr=async function(req,res,next){
    try{
        //get url frfcom environment

        let postStatus=await axios.post(process.env.PHP_BACKEND+'/ivr-survey/view', req.body, {
            headers: {
                'Content-Type': 'application/json',
            }
        } );


        return res.json(postStatus.data);   

    }catch(err){
      //  console.log(err);
      return next(err);
    }
}

exports.getIvrEdit=async function(req,res,next){

    let uid=req.params.uid;
    try{
        //get url frfcom environment
        let postStatus=await axios.get(process.env.PHP_BACKEND+'/ivr-survey/view-edit/'+uid, {
            headers: {
                'Content-Type': 'application/json',
            }
        } );


        return res.json(postStatus.data);   

    }catch(err){
      //  console.log(err);
      return next(err);
    }
}

exports.getSubscriber=async function(req,resp,next){
    let ivr_id=req.params.uid;
    try{
        IvrSubscriber.find({ivr_menu_uuid:ivr_id}).populate('user').exec(function(err,res){
         
         if(res.length>0){
            return resp.send({ success: true, message: res });
          }else{
            return resp.send({ success: false, message: "" });
          }
         })
    }catch(err){
        return next(err);
    }
}

exports.addSubscriber=async function(req,res,next){
    let ivr_uid=req.body.ivr_menu_uid;
    let subscriber=req.body.subscriber;

    if(subscriber.length>0){
        let data=[];
        subscriber.map(function(res){
            let subs=new IvrSubscriber();
            subs.user=res.user_id;
            subs.ivr_menu_uuid=ivr_uid;
            subs.created_at=DateNow.format();
            subs.updated_at=DateNow.format();
            data.push(subs);
        })
        // save multiple documents to the collection referenced by Book Model
        IvrSubscriber.collection.insert(data, function (err, docs) {
            if (err){ 
                return next(err);
            } else {
                res.send({ success: true, message: "Multiple documents inserted to Collection"});
            }
        });
    }else{
        res.send({ success: false, message: "Cannot Find Subscriber in data"});
    }
}

exports.editSubscriber=async function(req,res,next){
    let ivr_uid=req.body.ivr_menu_uid;
    let subscriber=req.body.subscriber;

    if(subscriber.length>0){
        let data=[];
        subscriber.map(function(res){
            if(res.status=="new"){
                let subs=new IvrSubscriber();
                subs.user=res.user_id;
                subs.ivr_menu_uuid=ivr_uid;
                subs.created_at=DateNow.format();
                subs.updated_at=DateNow.format();
                data.push(subs);
            }else if(res.status=="delete"){
     
                IvrSubscriber.findOneAndRemove({ivr_menu_uuid:ivr_uid,user:ObjectId(res.user_id)},function(err){
                    if(err){
                        return res.send({ success: false, message: "Cannot Change Data"});
                    }
                });
            }

        })


        // save multiple documents to the collection referenced by Book Model

        if(data.length >0){
            IvrSubscriber.collection.insert(data, function (err, docs) {
                if (err){ 
                    return next(err);
                } else {
                    return res.send({ success: true, message: "Multiple documents inserted to Collection"});
                }
            });
        }else{
            return res.send({ success: true, message: "Multiple edited to Collection"});
        }

       
    }else{
        return res.send({ success: false, message: "Cannot Find Subscriber in data"});
    }
}

exports.getIvrPerUser=async function(req,res,next){
    let userID=req.params.uid;

    let getIvr=new Promise(function(resolve,reject){
        IvrSubscriber.find({user:userID}).then(function(val){
            resolve(val);
        }).catch(err=>{
            reject(err);
        })
    })
    let data=[];

    try{
        let dataIvr=await getIvr;
        if(dataIvr.length>0){
            //get data of ivr per ivr id;
            await Promise.all(dataIvr.map(async function(dt){
                //request to php backend
                let postStatus=await axios.get(process.env.PHP_BACKEND+'/ivr-survey/detail/'+dt.ivr_menu_uuid, {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                } );
                console.log(postStatus);
                if(postStatus.data.status=="success"){
                    data.push(postStatus.data.data);
                }
            })).catch(err=>{
                console.log(err);
            })
        }else{
            return res.send({ success: false, message: "Cannot Find IVR Subscriber for this user "});
        }
    }catch(err){
        return res.send({ success: false, message: "Cannot Find IVR Subscriber for this user "});
    }


    return res.send({ success: true, message: data});
}