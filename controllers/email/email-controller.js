var mongoose=require('mongoose');
var Broadcast=mongoose.model('EmailBroadcast');
var Subscriber=mongoose.model('EmailSubscriber');
var Attachment=mongoose.model('EmailAttachment');
const moment=require('moment-timezone');

var ObjectId = require('mongoose').Types.ObjectId; 
let DateNow=moment().tz('Asia/Jakarta');

let amqp = require('amqplib/callback_api');
let amqpurl= process.env.AMQP;

//use by socket upload
exports.makeBroadcast=function(data){
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
    // broadcast_name:String,
    // broadcast_status:String,
    // broadcast_date:Date,
    // broadcast_subscriber:Number,
    // created_at:Date,
    // updated_at:Date,
    // file_name:String,
    // content_desc:[{type:String}],
    // content_message:String,
    // content_from:String,
    // content_subject:String,
    // email_template:String,
    // user_id:String
    let content_from="";
    if(data.content_from==""){
        content_from="default";
    }
    var br=new Broadcast ({
        broadcast_name:data.broadcastName,
        broadcast_status:'starting',
        broadcast_date:nDate,
        broadcast_subscriber:0,
        updated_at:nDate,
        created_at:nDate,
        file_name:data.file_name,
        content_desc:data.contentDesc,
        content_message:data.message,
        content_from:data.content_from,
        content_subject:data.content_subject,
        email_template:"default",
        user_id:data.user_id
    });

    return new Promise((resolve,reject)=>{
        br.save(function(error){
            if(error){
                return reject(error);
            };
            resolve(br);
        })
    })
}

exports.makeAttachmentsBroadcast=function(broadcast_id,filename,user){
    let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();

    var br=new Attachment ({
        filename:filename,
        created_at:nDate,
        updated_at:nDate,
        broadcast:broadcast_id,
        user:user
    });

    return new Promise((resolve,reject)=>{
        br.save(function(error){
            if(error){
                return reject(error);
            };
            resolve(br);
        })
    })
}
exports.getEmailBroadcast=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { broadcast_date: -1 },
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit
    };
   
    Broadcast.paginate({'user_id':req.body.user_id,'broadcast_status':{$ne:"delete"}}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};


exports.getEmailSubscriber=function(req,res){
//-----Post request
    //data :{broadcast_id:''}
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { broadcast_date: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit
    };
//    Subscriber.find({"broadcast":req.body.data.broadcast_id}).exec(function(err,response){
//        res.send(response);
//    })
    Subscriber.paginate({"broadcast":req.body.data.broadcast_id}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
        
    });
}

exports.sendBroadcastToMessageQueue=function(broadcastData,templateId,filename){
    amqp.connect(amqpurl, function(err, conn) {
        conn.createChannel(function(err, ch) {
          var q = 'sendtoemailfetch';
          ch.assertQueue(q, {durable: true});
          // Note: on Node 6 Buffer.from(msg) should be used
          console.log(templateId);
            console.log(broadcastData);
          let data={
              broadcast_data:broadcastData,
              filename:filename,
              template_id:templateId
          }
          let string=JSON.stringify(data);
          ch.sendToQueue(q, Buffer.from(string));
          console.log(" [x] Sent 'Broadcast Message!'");
        });

        setTimeout(function() { conn.close();}, 1500);
      });
}

exports.sendManualBroadcastToMessageQueue=function(broadcastData,filename,template){
    amqp.connect(amqpurl, function(err, conn) {
        conn.createChannel(function(err, ch) {
          var q = 'sendtoemailfetch';
          ch.assertQueue(q, {durable: false});
          // Note: on Node 6 Buffer.from(msg) should be used
          console.log(template);
            console.log(broadcastData);
            // let template={
            //     message:"",
            //     variable:""
            // }
          let data={
              broadcast_data:broadcastData,
              filename:filename,
              template:template
          }
          let string=JSON.stringify(data);
          ch.sendToQueue(q, Buffer.from(string));
          console.log(" [x] Sent 'Broadcast Message!'");
        });

        setTimeout(function() { conn.close();}, 1500);
      });
}

exports.deleteEmailBroadcast=function(req,res){
    let broadcast_id=req.params.broadcast_id;

    Broadcast.findById(broadcast_id).then((val)=>{
        if(val){
            val.broadcast_status="delete";

            val.save((err)=>{
                if(err){
                    return res.json({
                        status:false,
                        errors:{
                            code:"DB ERROR",
                            message:"CANNOT DELETE DATA"
                        },data:{}
                    });  

                }
                return res.json({
                    status:true,
                    errors:{

                    },data:{}
                });  
            });
        }
    }).catch(err=>{
        return res.json({
            status:false,
            errors:{
                code:"DB ERROR",
                message:"CANNOT FIND BROADCAST"
            },data:{}
        });  
    })
};