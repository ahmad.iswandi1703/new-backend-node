const rp = require('request-promise');
// Requires fs to write synthesized speech to a file
const fs = require('fs');
// Requires readline-sync to read command line inputs

// Requires xmlbuilder to build the SSML body
const xmlbuilder = require('xmlbuilder');

function getAccessToken(subscriptionKey) {
    let options = {
        method: 'POST',
        uri: 'https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken',
        headers: {
            'Ocp-Apim-Subscription-Key': subscriptionKey
        }
    }
    return rp(options);
}

function textToSpeech(accessToken, text,res) {
    // Create the SSML request.
    let xml_body = xmlbuilder.create('speak')
        .att('version', '1.0')
        .att('xml:lang', 'id')
        .ele('voice')
        .att('xml:lang', 'id')
        .att('name', 'Microsoft Server Speech Text to Speech Voice (id-ID, Andika)')
        .txt(text)
        .end();
    // Convert the XML into a string to send in the TTS request.
    let body = xml_body.toString();

    let options = {
        method: 'POST',
        baseUrl: 'https://westus.tts.speech.microsoft.com/',
        url: 'cognitiveservices/v1',
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'cache-control': 'no-cache',
            'User-Agent': 'localhost',
            'X-Microsoft-OutputFormat': 'audio-16khz-32kbitrate-mono-mp3',
            'Content-Type': 'application/ssml+xml'
        },
        body: body
    }

    let request = rp(options)
        .on('response', (response) => {
            if (response.statusCode === 200) {
                request.pipe(res);
                console.log('\nYour file is ready.\n')
            }
        });
    return request;
}

async function main(textque,res) {
    // Reads subscription key from env variable.
    // You can replace this with a string containing your subscription key. If
    // you prefer not to read from an env variable.
    // e.g. const subscriptionKey = "your_key_here";
    const subscriptionKey ="8cb85d9f4198402cbea73f846736a439";
    if (!subscriptionKey) {
        throw new Error('Environment variable for your subscription key is not set.')
    };
    // Prompts the user to input text.
    const text = textque;
    try {
        const accessToken = await getAccessToken(subscriptionKey);
        await textToSpeech(accessToken, text,res);
    } catch (err) {
        console.log(`Something went wrong: ${err}`);
    }
}


exports.getAudioStream=function(req,res,next){
    let query=req.query.text;

    main(query,res).catch(err=>{
        return res.send('401');
    })
}