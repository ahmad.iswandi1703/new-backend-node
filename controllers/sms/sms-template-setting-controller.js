

const mongoose = require('mongoose');
let ObjectId=mongoose.Types.ObjectId
let SmsTemplate=mongoose.model('SmsTemplate');
let Subscriber=mongoose.model('SmsTemplateSubscriber');
let ERROR_CODE={
    UNDEFINED_VARIABLE:"UNDEFINED_VARIABLE",
    DB_ERROR:"CANNOT FIND DATA",
    FORMAT_ERROR:"FORMAT_ERROR",
}
var moment = require('moment-timezone');


//
exports.addSmsTemplate=async function(req,res,next){

    let created_by=req.body.user_id;
    let variabled=req.body.content_desc;
    let message=req.body.message;
    let template_name=req.body.template_name;


    if(created_by=='' || message=='' || variabled=='' || template_name==''){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.UNDEFINED_VARIABLE,
                message:"required request parameter: user_id, variabled, message, template name"
            },data:{}
        });  
    }
    try{
        //validate between message body and declared variabled.
        //matching declared variable with message bidy
        let matchingTemplate= message.match(/\[(.*?)\]/g); //["[No Tagihan]","[Wifi.id]",..etc]
        let countNotSameTemplate=0;

        let newText;

        for(let a=0; a<matchingTemplate.length; a++){
            let replaceContent= matchingTemplate[a].replace(/\[(.*?)\].*/g,"$1");
            let searchVariabled=variabled.filter(val=>{
                return val===replaceContent;
            })
            let wrongTextDefined;
            if(searchVariabled.length==0){
              wrongTextDefined="<i class='text-danger'>"+matchingTemplate[a]+"</i>";
              countNotSameTemplate++;
            }else{
              wrongTextDefined="<i class='text-success'>"+matchingTemplate[a]+"</i>";
            }

            //set newText and replace with 
            newText=message.replace(matchingTemplate[a],wrongTextDefined);
        }


        if(countNotSameTemplate>0){
            //send return error json to client with format error
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.FORMAT_ERROR,
                    message:newText
                },data:{}
            });  

        }else{
            //save data to db send return json to client
            let dateNow=moment().tz('Asia/Jakarta').format();
            let template=new SmsTemplate();
            template.template_name=template_name;
            template.template_variable=variabled;
            template.template_message=message;
            template.created_by=created_by;
            template.created_at=dateNow;
            template.updated_at=dateNow;
            template.save(function(err){
                if (err) {
                    throw Error("insert db");
                }

                return res.json({
                    status:true,
                    errors:{},data:template
                }); 
               
            })

        }
    }catch(err){
      //  console.log(err);

      if(err=="insert db"){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"Error When Inserting Data"
            },data:{}
        });  
      }else{
        return next(err);
      }
    }
}


exports.getSmsTemplate=async function(req,res,next){
    let template_id=req.params.template_id;
    
    SmsTemplate.findById(template_id).then((val)=>{
        if(val){

            return res.json({
                status:true,
                errors:{},data:val
            }); 
            
          }else{
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.DB_ERROR,
                    message:"data not found"
                },data:{}
            });  
          }
    }).catch(err=>{
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"data not found"
            },data:{}
        });  
    });

    
}

exports.deleteTemplate=async function(req,res,next){
    let template_id=req.params.template_id;

    SmsTemplate.findByIdAndRemove(template_id, function(err){
        if(err){
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.DB_ERROR,
                    message:"cannot deleted data"
                },data:{}
            });  
        }

        Subscriber.remove({ template: ObjectId(template_id) }, function(err) {
            if (err) {
                console.log(err);

            }
            return res.json({
                status:true,
                errors:{},data:{}
            }); 
        });


    });
}
exports.editTemplate=async function(req,res,next){
    let template_id=req.body.template_id;
    let message=req.body.message;
    let template_name=req.body.template_name;
    let content_desc=req.body.content_desc;

    SmsTemplate.findById(template_id).then((val)=>{
        val.template_message=message;
        val.template_name=template_name;
        val.template_variable=content_desc;
        val.save(function(err){
            if(err){
                return res.json({
                    status:false,
                    errors:{
                        code:ERROR_CODE.DB_ERROR,
                        message:"failed to save data"
                    },data:{}
                });
            }
            return res.json({
                status:true,
                errors:{
                },data:{}
            });
        })
    }).catch(err=>{
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"data not found"
            },data:{}
        });
    })

}
//get data sms template
//edit data sms template
//delete data sms template


exports.queryPagingSmsTemplate=async function(req,res,next){
    let options={
        sort:     { created_at:-1},
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit
      };

    let created_by=req.body.data.user_id;
    
    
      SmsTemplate.paginate({created_by:created_by}, options)
      .then(response => {
        /**
         * Response looks like:
         * {
         *   docs: [...] // array of Posts
         *   total: 42   // the total number of Posts
         *   limit: 10   // the number of Posts returned per page
         *   page: 2     // the current page of Posts returned
         *   pages: 5    // the total number of pages
         * }
        */
          if(response.docs.length >0){

            return res.json({
                status:true,
                errors:{},data:response
            }); 
            
          }else{
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.DB_ERROR,
                    message:"data not found"
                },data:{}
            });  
          }
         
      })
      .catch(function(err){
          //handle query error;
          // set loggin
          console.log(err);
          return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"data not found"
            },data:{}
        });  
      });
}

exports.getTemplateBySubscriber=async function(req,res,next){
    let userID=req.params.user_id;

    if(userID===''){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.UNDEFINED_VARIABLE,
                message:"required request parameter: user_id"
            },data:{}
        });  
    }

    let getSmsTemplate=new Promise(function(resolve,reject){
        Subscriber.find({user:userID}).populate('template').sort({created_at:-1}).then(function(val){
            resolve(val);
        }).catch(err=>{
            reject(err);
        })
    })
    let data=[];

    try{
        let dataSubscriber=await getSmsTemplate;
        if(dataSubscriber.length>0){
            //get data of ivr per ivr id;
            return res.json({
                status:true,
                errors:{},data:dataSubscriber
            });
        }else{
            
            throw Error("err");
        }
    }catch(err){

        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"Cannot Find Data"
            },data:{}
        });  
        
    }

}

exports.getSubscriberByTemplate=async function(req,res,next){

        let template_id=req.params.sms_template_id;
        try{
            Subscriber.find({template:template_id}).populate('user').exec(function(err,resp){
             if(resp.length>0){
                return res.json({
                    status:true,
                    errors:{},data:resp
                });
              }else{
                return res.json({
                    status:false,
                    errors:{
                        code:ERROR_CODE.DB_ERROR,
                        message:"Cannot Find Data"
                    },data:{}
                });  
              }
            })
        }catch(err){
            return res.json({
                status:false,
                errors:{
                    code:ERROR_CODE.DB_ERROR,
                    message:"Cannot Find Data"
                },data:{}
            });  
        }
    
    
}

exports.addSubscriber=async function(req,res,next){
    let template_id=req.body.sms_template_id;
    let subscriber=req.body.subscriber;
    let dateNow=moment().tz('Asia/Jakarta').format();

    if(subscriber.length>0){
        let data=[];
        subscriber.map(function(res){
            let subs=new Subscriber();
            subs.user=res.user_id;
            subs.template=template_id;
            subs.created_at=dateNow;
            subs.updated_at=dateNow;
            data.push(subs);
        })
        // save multiple documents to the collection referenced by Book Model
        Subscriber.collection.insert(data, function (err, docs) {
            if (err){ 
                throw Error("err");
            } else {        
                return res.json({
                    status:true,
                    errors:{},data:docs
                });
            }
        });
    }else{
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"Error When Inserting Data"
            },data:{}
        });  
    }
}

exports.editSubscriber=async function(req,res,next){
    let template_id=req.body.sms_template_id;
    let subscriber=req.body.subscriber;
    let dateNow=moment().tz('Asia/Jakarta').format();
    if(subscriber.length>0){
        let data=[];
        subscriber.map(function(res){
            if(res.status=="new"){
                let subs=new Subscriber();
                subs.user=res.user_id;
                subs.template=template_id;
                subs.created_at=dateNow;
                subs.updated_at=dateNow;
                data.push(subs);
            }else if(res.status=="delete"){
     
                Subscriber.findOneAndRemove({template:ObjectId(template_id),user:ObjectId(res.user_id)},function(err){
                    if(err){
                        throw Error("err");
                    }
                });
            }

        })


        // save multiple documents to the collection referenced by Book Model

        if(data.length >0){
            Subscriber.collection.insert(data, function (err, docs) {
                if (err){ 
                    throw Error("err");
                } else {
                    return res.json({
                        status:true,
                        errors:{},data:{}
                    });
                }
            });
        }else{
            return res.json({
                status:true,
                errors:{},data:{}
            });
           
        }

       
    }else{
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.DB_ERROR,
                message:"Error When Inserting Data"
            },data:{}
        });  
    }
}