const Subscriber=require('../model/subscriberSms.model');
const moment=require('moment');
exports.endpoint=function(req,res){
    const messageSid = req.body.MessageSid;
    const messageStatus = req.body.MessageStatus;
	console.log(messageSid);
	let dateUpd=new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
          });
        Subscriber.findOneAndUpdate({sid: messageSid}, {$set:{status:messageStatus, updated_at:dateUpd}}, {returnOriginal:false}, (err, doc) => {
            if (err) {
                console.log("Something wrong when updating data!");
            }

            console.log(doc);
        });
 

    res.sendStatus(200);
}
