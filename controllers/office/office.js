const mongoose = require('mongoose');
let ObjectId=mongoose.Types.ObjectId

let Office=mongoose.model("Office");

module.exports={
    officeByParents:function(parentID){
        let params={};
        params.parents=ObjectId(parentID);

        return new Promise(async (resolve,reject)=>{
            let returnData={
                status:false,
                data:null,
            }
           await  Office.find(params).sort({created_at:1})
            .then(response => {
                if(response.length >0){
                    returnData.status=true;
                    returnData.data=response;
                }
            })
            .catch(function(err){
                //handle query error;
                // set loggin
                return reject(returnData);
            });
            return resolve(returnData);
        })

    },officeDetail:function(officeId){
        return new Promise(async (resolve,reject)=>{
            let returnData={
                status:false,
                data:null,
            }
           await  Office.findById(officeId)
            .then(response => {
                if(response){
                    returnData.status=true;
                    returnData.data=response;
                }
            })
            .catch(function(err){
                //handle query error;
                // set loggin
                return reject(returnData);
            });
            return resolve(returnData);
        })
    }
}


function isEmpty(data){
    for(var key in data) {
        if(data.hasOwnProperty(key))
            return false;
    }
    return true;
  
}