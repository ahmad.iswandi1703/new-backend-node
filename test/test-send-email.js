const mongoose = require('mongoose');
let dev_db_url = 'mongodb://ahmad_iswandi1703:RJjI61kXj6wzParc@178.128.87.178:27017/main';
let mongoDB = dev_db_url;
mongoose.connect(mongoDB,{ useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));



require('../model/EmailSetting.model');

//email broadcast model
require('../model/Broadcast.email.model');
require('../model/Broadcast.email.subscriber.model');
const BroadcastEmail=mongoose.model("EmailBroadcast");
const BroadcastSubscriber=mongoose.model("EmailSubscriber");
const uuid=require('uuid');
var moment = require('moment-timezone');

const {Pool, Client} =require('pg'); 

const job=require('../job/agenda-run-email.js');
const SettingController=require('../controllers/email-setting-controller.js');

function sendEmail(){
    let DateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
    });


      //set broadcast email and run the job scheduler
    let Broadcast =new BroadcastEmail();
    Broadcast.broadcast_name="Email Test";
    Broadcast.broadcast_status="Preparation";
    Broadcast.broadcast_date=DateNow;
    Broadcast.broadcast_subscriber=0;
    Broadcast.created_at=DateNow;
    Broadcast.updated_at=DateNow;
    Broadcast.file_name="coba-kontak-email.csv";
    Broadcast.content_desc=["Email","Name"];
    Broadcast.content_message="Hello [Name]";
    Broadcast.content_from="ahmad.iswandi1703@gmail.com";
    Broadcast.content_subject="Oba Broadcast";
    Broadcast.user_id="5c3c00a8d7038626d8c1da24";
    Broadcast.email_template="default";
    //run job from here

    Broadcast.save(async function(err){
        if (err) throw err;
        broadcast_id=Broadcast._id;

        //get setting data;
        try{
            let settingData=await SettingController.getEmailSetting(user_id);
            job.running(Broadcast,settingData);
        }catch(err){
            //update broadcast that this broadcast mail is broken
            console.log("there is no email setting");

            Broadcast.findById(Broadcast._id,function(err,br){
                if (err){
                    console.log(err);
                }
      
                br.broadcast_status="Failed";
           
                br.updated_at=new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Jakarta'
                  });
                
                br.save(function(err){
                    if (err) console.log(err);
                    console.log("success update sms broadcast");
                })
            })
        }
        
    });
}

db.on('connected',function(){
    // console.log("cinnec");
    // sendEmail();
    console.log("connect to db");
})