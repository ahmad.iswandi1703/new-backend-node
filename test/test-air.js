function delay() {
    return new Promise(resolve => setTimeout(resolve, 3000));
}

async function delayedLog(item) {
    // notice that we can await a function
    // that returns a promise
    console.log('sebleum delay');
    //await delay();
    console.log(item);
}

async function processArray() {
    // map array to promises
    let array=new Array(30).fill(30);
    let i=0;
    const promises = array.map(function(val){
        console.log(i);
        if(i==10){
            return null;
        }
        i++;

    });
    // wait until all promises are resolved
    await Promise.all(array.map(function(val){
        console.log(i);
        if(i==10){
            
        }
        i++;
    })).then(val=>{

    });
    console.log('Done!');
}

processArray();

// for(let i=0; i<100000; i++){
//     console.log(i);
// }

// let index=1000;
// let batchNumber=5*1000;

// console.log(index%batchNumber);

// console.log("done");