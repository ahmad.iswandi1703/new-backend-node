const moment=require('moment-timezone');
const _=require('lodash');
const axios=require('axios');
let date=10;
let year=2019;
let month=03
let hours=10;
let minute=11;
let dated=new Date(year,month-1,date);
let dated2=new Date(2019,03-1,10);


// console.log(dated.getTime());
// console.log(dated.toString());
// console.log(moment(dated.getTime()).tz('Asia/Jayapura').toString())
let d1=moment(dated).tz('Asia/Jakarta');
let d2=moment(dated2).tz('Asia/Jakarta');

console.log(moment(new Date('2019-07-10')).tz('Asia/Jakarta').format(), 'here om');
let dif=d2.diff(d1,'days');
// for(let i=0; i<dif; i++){
//     if(i>0){
//         console.log(i+1);
//     }else{
//         console.log("1");
//     }
// }



//make validate function date 
function validate(schedule){
    let schdFromDate=schedule.schedule_date_from;
    let schdToDate=schedule.schedule_date_to;
    let timezone=schedule.schedule_timezone;

    switch (true) {
        case  timezone=="wib" || timezone=="WIB":
            timezone='Asia/Jakarta';
            break;
        case timezone=="wit" || timezone=="WIT":
            timezone='Asia/Jayapura';
            break;
        case timezone=="wita" || timezone=="WITA":
            timezone='Asia/Makassar';
            break; 
        
        default:
            break;
    }


    let timeStart=schedule.schedule_start_time;
    let timeEnd=schedule.schedule_end_time;
    //check if sched to Date null then set to from date;
    if((schdToDate.date=="" || schdToDate.date==null || schdToDate.date==undefined)){
        schdToDate=schdFromDate;
    }

    let dFrom=moment(new Date(parseInt(schdFromDate.year),parseInt(schdFromDate.month)-1,parseInt(schdFromDate.date))).tz(timezone);
    let dTo=moment(new Date(parseInt(schdToDate.year),parseInt(schdToDate.month)-1,parseInt(schdToDate.date))).tz(timezone);
    let daysDiff=dTo.diff(dFrom,'days');

    let dateCollection=[];
    let newDate=dFrom;
    for(let i=0; i<=daysDiff; i++){
        
        let dateData={
            date:"",
            startTime:"",
            endTime:"",
            timezone:""
        }
        dateData.startTime=timeStart;
        dateData.endTime=timeEnd;
        dateData.timezone=timezone;
        if(i>0){
            let nDateFrom=newDate.add(1,'days');
            dateData.date=nDateFrom.format();
            dateCollection.push(dateData);
            newDate=null;
            newDate=nDateFrom;
        }else{
            dateData.date=dFrom.format();
            dateCollection.push(dateData);
        }
       
    }
    return dateCollection;
    // console.log(dateCollection);
    // //to start scheduler;
    // let dt=moment(dateCollection[0]);
    // let time=moment.utc('06:08','hh:mm');
    // console.log(dt.set({hours:time.hours(),minutes:time.minutes()}).toString());
    // //validat same date.
}

function validateDate(dateCollection, newDateCollection){
    // date Collection
    // [{
    //     date:"",
    //     startTime:"",
    //     endTime:"",
    //     timezone:""
    // }]
    //find same date and check if any date run in same range time (startTime and endTime)
    let joinDate=[];
    _.each(newDateCollection,function(nDate,index,nDates){
        let dateFound=_.find(dateCollection,function(o){
            return o.date==nDate.date && o.timezone==nDate.timezone;
        })
        let newStartTime=moment.utc(nDate.startTime.hour+':'+nDate.startTime.minute,'hh:mm').format('hh:mm');
        let newEndTime=moment.utc(nDate.endTime.hour+':'+nDate.endTime.minute,'hh:mm').format('hh:mm');
        //this status date will be given true value if it passed the characteristic of start and end time handler below
        let statusDate=true;
        //characteristic handler
        if(dateFound){
            console.log('here1');
            let collStartTime=moment.utc(dateFound.startTime.hour+':'+dateFound.startTime.minute,'hh:mm').format('hh:mm');
            let collEndTime=moment.utc(dateFound.endTime.hour+':'+dateFound.endTime.minute,'hh:mm').format('hh:mm')
            //check the criteria to match 
            /**
             * check the data 
             */
            //result 
            /**
             * each date scheduler cannot have same criteria like same timezone, range start time and end time
             */
            //check start and end time in date and compare 
            if(newStartTime < collStartTime){
                console.log('here2.1');
                if(newEndTime>collStartTime){
                    console.log('here2.2');
                    statusDate=false;
                }
            }else if(newStartTime > collStartTime ){
                console.log('here3.1');
                //check the end time if in range of collection start time and end time
                if(newEndTime<=collEndTime){
                    console.log('here3.2')
                    statusDate=false;
                }
            }else if(newStartTime == collStartTime){
                console.log('here4.1');
                statusDate=false;
            }
        }

        if(statusDate){
            joinDate.push(nDate);
        }
    });

    let newResult=_.concat(dateCollection,joinDate);
    return newResult
    //merge array between newDateCollection and dateCollection
}


let dataSchedule={
    "schedule_date_from":{"date":"10","year":"2019","month":"06"},
    "schedule_date_to":{"date":"13","year":"2019","month":"06"},
    "schedule_timezone":"wib",
    "schedule_start_time":{"hour":"10","minute":"00"},
    "schedule_end_time":{"hour":"11","minute":"00"}
}

let dateScheduleTwo={
    "schedule_date_from":{"date":"10","year":"2019","month":"06"},
    "schedule_date_to":{"date":"14","year":"2019","month":"06"},
    "schedule_timezone":"wib",
    "schedule_start_time":{"hour":"09","minute":"00"},
    "schedule_end_time":{"hour":"10","minute":"00"}
}


let schMaker=require('../lib/schedule-maker');
let dateSc1=schMaker.parseScheduler(dataSchedule);
let dateSc2=schMaker.parseScheduler(dateScheduleTwo);

let returnDate=schMaker.validateScheduler(dateSc1,dateSc2);
//console.log(returnDate);
let sorting=_.orderBy(returnDate,['date','startTime.hour','startTime.minute'],['asc']);


//save data scheduler to database;


let newReturn=validate(dataSchedule);
console.log(newReturn);

let time=moment.utc('08:08','hh:mm').format('hh:mm');
let start=moment.utc('07:08','hh:mm').format('hh:mm');

console.log("am")
console.log(moment('2019-08-11T03:00:00.000+00:00').toLocaleString());

console.log(moment())
// console.log(time);
// console.log(start);
// console.log(time>start);
// start to validate if any same date that have same range in db
//

// let a=["a","b","c","d","e"];

// let found=_.find(a,function(o){
//     return o=="a";
// });


// let bullshit=[];

// let key=[{id:1,name:"ahmad"},{id:1,name:"ahmad"}];
// let key2=[{id:1,name:"ahmad Iswandi"},{id:1,name:"jokowidodo"}];

// bullshit=[...key, ...key2];
// console.log(bullshit);

// let an={
//     name:"ahmad",
//     giv:'',
// }

// let newm={...an};
// newm['last']="asdf";
// console.log(newm);
// 'use strict';
// (async function(){
//     try{
//         let login=await axios.post('http://r.dev.udata.id:8081/login',{username:'oca',password:'oca$01'})
//         let totalProspect=await axios.get('http://r.dev.udata.id:8081/api/filters');
//         console.log(totalProspect);
//     }catch(err){
//         console.log(err);
//     }

// })();

// console.log(found);

// if(found){
//     console.log('ye');
// }else{
//     console.log('no');
// }

//lets create scheduler for this advanced scheduler for this smart profiling
//
//


//make sms billing setting that can count all sms operation from the user
//
// 
// let a={a:null};
// console.log(a.a);
// validate();
// let date2=moment();

// var toLocalTime = function(time) {
//     var d = new Date(time);
//     var offset = (new Date().getTimezoneOffset() / 60) * -1;
//     var n = new Date(d.getTime() + offset);
//     return n;
// };

// console.log(new Date("2019-05-31T10:45:39+07:00").toLocaleString());
// console.log(moment("2019-03-01T15:00:00.000Z").format());
// console.log(moment(date).format());


// var timeEnded=moment("22:00", "HH:mm").format();
// let d="09:00";
// var timeStarted=moment(d,"HH:mm").tz('Asia/Jakarta').format();
// var timeNow=moment(date).tz('Asia/Jakarta').format();


// // console.log(timeNow.isAfter(timeEnded));
// // console.log(timeNow.isBefore(timeStarted));
// console.log(timeEnded);
// console.log(timeStarted,"timestarted");

// console.log(timeNow<=timeStarted);
// console.log(timeNow>=timeEnded);

// var newtimeStarted=moment(date).tz('America/Chicago');

// console.log(newtimeStarted.format(),"America");
// let newNa=newtimeStarted.clone().tz('Asia/Jakarta').format()
// console.log(newNa,"new time started");

        
// let parse=d.trim().split(":");
// let dNow=new Date();
// let dNewNow=moment(dNow);
// dNewNow.month(3).set({h: parse[0], m:parse[1]})
// console.log(dNewNow.format(),"new form");
// console.log(dNewNow);


// let hours=moment(dNow).format("HH:mm").toString();
// console.log(hours);
// let parses=hours.trim().split(":");
// console.log(parses);
// let dNewNows=moment().tz('Asia/Jakarta');
// dNewNows.set({h})
// dNewNows.set({h: parses[0], m:parses[1]})
// let timeNows=dNewNows;
