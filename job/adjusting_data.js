const Agenda = require('agenda');

var twilio = require('twilio');
var client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
var Subscriber=require('../model/subscriberSms.model');
var Broadcast=require('../model/smsbroadcast.model');
const moment =require('moment');

async function run(){

    // console.log(data);
    // console.log(broadcastid);


    //const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
    const mongoConnectionString=process.env.MONGODB_URI;
    const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});

    agenda.define('adjustment-data',function(job,done){
        var cursor=Subscriber.find({}).cursor();

        cursor.on("data",function(subscriber){
            client.messages(subscriber.sid)
            .fetch()
            .then(message =>{
                console.log(message.status);
                subscriber.status=message.status;
                subscriber.updated_at=new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Jakarta'
                  });
                subscriber.price=message.price;
                subscriber.save(function(err){
                    if(err) throw err;
                   
                })
            })
            .done();
        })

        cursor.on("close",function(){
            done();
        })

    
    })
    await new Promise(resolve => agenda.once('ready', resolve));

    agenda.schedule(new Date(Date.now() + 1000), 'adjustment-data',{});
    agenda.start();
}

exports.running=run;