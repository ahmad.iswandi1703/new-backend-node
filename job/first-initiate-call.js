const Agenda = require('agenda');

const mongoose=require('mongoose');
require('../model/broadcast.contact-list.model');
require('../model/setting.call.model');
require('../model/BroadcastJobNotif');
require('../model/Broadcast.call.model');
require('../model/Broadcast.call.subscriber.model');

var ContactList=mongoose.model('BroadcastContactList');
var Setting=mongoose.model('CallSetting');
var JobNotif=mongoose.model('BroadcastJobNotif');
var Call=mongoose.model('BroadcastCall');
var Subscriber=mongoose.model('BroadcastSubscriber');
const axios=require('axios');



async function run(broadcast,agenda){
    //get setting by broadcast.settin
    var setting={};

    console.log(broadcast);

    
    var settingById =new Promise((resolve,reject)=>{
        Setting.findById(broadcast.call_setting,function(err,res){
            console.log(res);
            resolve(res);
        })
    });
    


    try{
        setting=await settingById;
        let dateNow=new Date().toLocaleString('en-US', {
            timeZone: setting.timezone
        });
        if(setting!=null){
            console.log("ada setting");
            let broadcast_name="call"+broadcast._id;
    
          
            var moment = require('moment-timezone');
            //var nDate = moment().tz('Asia/Jakarta'); 
            let schedule= moment().tz(setting.timezone).add(1,"seconds").format();
            let status="Starting";
            if(broadcast.call_immediately=="false"){
                schedule=broadcast.call_schedule;
                console.log(broadcast.call_immediately);
            }
            console.log(setting);
    
            let newSchedule=determineSchedule(setting,schedule,broadcast.call_immediately);
    
            console.log(schedule);
            console.log("nnew schedule");
            console.log(newSchedule);
    
            //update broadcast call
            var updateCallBroadcast=function(broadcast,date){
                return new Promise(function(resolve,reject){
                    Call.findById(broadcast,function(err,res){
                        var moment = require('moment-timezone');
                        var nDate = moment().tz(setting.timezone); 
                        res.updated_at=nDate.format();
                        res.call_schedule=date;
                        res.call_immediately=false;
                       
                        res.save(function(err){
                            if (err) {
                                console.log(err);
                            };
                            console.log("uppp");
                            resolve("success");
                        })
                    })
                })
            }
    
            try{
                await updateCallBroadcast(broadcast._id,newSchedule);
    
            }catch(err){
                console.log("cannot update call schedule in broadcast");
            }
           
            await agenda.schedule(newSchedule, "Call_broadcast3",{status:status,additional:broadcast,setting:setting,nextRow:0});
          
        }else{
            //update status of broadcast call
            await Call.findById(broadcast._id,function(err,res){
                res.updated_at=dateNow;
                res.call_status="Error";
                res.save(function(err){
                    if (err) throw err;
                })
            }).catch(function(err){
                console.log(err);
            })
    
        }
    }catch(err){
        let dateNow=moment().tz('Asia/Jakarta').format();
        var jobWarning=new JobNotif();
        jobWarning.broadcast=broadcast._id;
        jobWarning.status="Error";
        jobWarning.message="Cannot Find Setting for call";
        jobWarning.updated_at=dateNow;
        jobWarning.created_at=dateNow;
        jobWarning.save(function(error){
            if(error){
                throw error;
            }
        });
    };


    //job setting
    //-- job will be continoued if contact batch finish to be called

    //time to start broadcast (cek)
    //-- using effective immediatly (this will run by on seconds after this job created)
    //-- using time (Run at 23 january 5 12:30pm)
    //-- if there is a stop jp

    //rule for stopping this job
    //-- if time showing at end of the day (EOD set to 5pm everyday)
    //-- if all call contact in this broadcast already called
    
    // make model to save temporary broadcast contact
    // -- how many row that have called
    // -- next row

    // set setting data
    // -- how many contact that will be sent every minute (max 100)
    // -- start call every day ( 8 am)
    // -- stop call every day (5pm)


    // console.log(data);
    // console.log(broadcastid);

    //rule 

    //run job later in any given point time and repeat 2 minutes 
    //schedule if user want to start the job at any given point time
    //agenda.every('*/5 * * * * *', 'test-data-2');
    // agenda.every('3 seconds','test-data-2',{rowNumberFinish:0,nextRow:10});

}
var changeHoursOfDateNow=function(dateNow,time){
    var startDay=time;//get from setting
    var split=startDay.split(":");
    var newDate=dateNow.set({
        hours:split[0],
        minutes:split[1]
    }).format();

    return newDate;
}

let getdateNow=function(timeToStart,timezone){
    let hours=moment(timeToStart).format("HH:mm").toString();
    let parseDate=moment(timeToStart).format("DD.MM.YYYY").toString().trim().split('.');
    let parse=hours.trim().split(":");
    let dNewNow=moment().tz(setting.timezone);
    dNewNow.set({'year': parseDate[2], 'month': parseDate[1]-1,'date':parseDate[0],h: parse[0], m:parse[1]})
    return dNewNow;
}
var determineSchedule=function(setting,timeToStart,callSoon){
    var moment = require('moment-timezone');
    var nDate = moment().tz(setting.timezone); 
    //
    let hourss=moment(timeToStart).format("HH:mm").toString();
    let parseDates=moment(timeToStart).format("DD.MM.YYYY").toString().trim().split('.');
    let parses=hourss.trim().split(":");
    let dNewNows=moment().tz(setting.timezone);
    dNewNows.set({'year': parseDates[2], 'month': parseDates[1]-1,'date':parseDates[0],h: parses[0], m:parses[1]})
    //dNewNow.set({h: parse[0], m:parse[1]}

    //
    var dateNow=dNewNows;
    var numOfDday=dateNow.day();//sunday=0, monday=1,......, saturday=6
    var nextRun=new Date(Date.now() + 2000);
    var weekDays=setting.weekdays;
    
    console.log(dateNow,"datenowwwww");
    var timeEnded=changeHoursOfDateNow(dateNow,setting.daily_end_time);
    var timeStarted=changeHoursOfDateNow(dateNow,setting.daily_start_time)
    // var timeEnded=moment(setting.daily_end_time, "HH:mm").tz(setting.timezone).format();
    // var timeStarted=moment(setting.daily_start_time,"HH:mm").tz(setting.timezone).format();
    var timeNow;
    if(callSoon=='true'){
        console.log("call immediatelys");
        timeNow=moment(timeToStart).tz(setting.timezone).format();
        //timeEnded=changeHoursOfDateNow(moment(timeToStart).tz(setting.timezone),setting.daily_end_time);
        //timeStarted=changeHoursOfDateNow(moment(timeToStart).tz(setting.timezone),setting.daily_start_time)

    }else{
        console.log("call Schedue");
        console.log(timeToStart,"time original");
        //make new date with hours and minute from schedule
        let hours=moment(timeToStart).format("HH:mm").toString();
        let parseDate=moment(timeToStart).format("DD.MM.YYYY").toString().trim().split('.');
        let parse=hours.trim().split(":");
        let dNewNow=moment().tz(setting.timezone);
        dNewNow.set({'year': parseDate[2], 'month': parseDate[1]-1,'date':parseDate[0],h: parse[0], m:parse[1]})
        //dNewNow.set({h: parse[0], m:parse[1]})
        timeNow=dNewNow.format();
        //add new 5/13/2019
       // timeNow=moment(timeToStart).tz('Asia/Jakarta').format();
    }
    console.log(moment(timeToStart).tz(setting.timezone).format("HH:mm"),"startwit timezon");
    console.log(moment(timeToStart).format("HH:mm"),"startwithout timezon");
    console.log(timeNow,"now");
    console.log(timeStarted,"started");
    console.log(timeEnded,"ended");
    console.log(setting.daily_start_time);
    console.log(setting.daily_end_time);
    let newDateQ=timeNow;
    if(timeNow<=timeStarted){
        var newtimeStarted=moment(setting.daily_start_time,"HH:mm").tz(setting.timezone).format("HH:mm");
        let parse=setting.daily_start_time.trim().split(":");
        let dNow=new Date();
        let dNewNow=moment().tz(setting.timezone);
        dNewNow.set({h: parse[0], m:parse[1]})
        console.log(dNewNow.format(),"new form");
        console.log(dNow,"form javascript data");

        let newNa=dNewNow.format();
        console.log(newNa,"new time started");
        //df
        //cek apakah hari ini merupakan hari libur atau tidak, kalau iya maka alokasikan ke hari berikutnya;
        if(weekDays[numOfDday].length!=0 && weekDays[numOfDday]!=null){
            console.log("kurang dari daily start time dan hari ini tidak libur");
            if(dateNow.format()>=timeStarted){
                nextRun=determineScheduleTime(dateNow,numOfDday,weekDays,setting.daily_start_time,setting.timezone);
                newDateQ=nextRun;
            }else{
                newDateQ= newNa;
            }
        }else{
            console.log("kurang dari daily start time dan hari libut");
            nextRun=determineScheduleTime(dateNow,numOfDday,weekDays,setting.daily_start_time,setting.timezone);
            newDateQ=nextRun;
        }
        
    }

    //jika call immediately, cek apakah waktu sekarang sudah lewat dari daily end time atau belum daily start time
    if(timeNow>=timeEnded){
        console.log("lewat dari daily end time");
        //call determineScheduleTime
        nextRun=determineScheduleTime(dateNow,numOfDday,weekDays,setting.daily_start_time,setting.timezone);
        newDateQ=nextRun;
    }
    console.log(newDateQ);
    return newDateQ;

    //cek apakah time now kecil dari daily start time


    //kalau lebih kecil start schedule di daily start time


    // tambah penjagaan untuk memvalidasi tanggal ketika 
}

var determineScheduleTime=function(dateNow,numOfDday,weekDays,daily_start_time,timezone){
            //stop the job and set nextRun At to tommorrow at start the day
            let nextRunAt="";
            //it mean weeken or saturday
            let daysBetween=0;
            if(numOfDday == 6){
                //it means saturday  add to the next day
                let foundNextDay=false;
                let indexDay=0; 
                //console.log(weekDays[0].length);
                while(!foundNextDay){
                    if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                        //check if next day is the day that forbided in weekdays
                        nextRunAt= weekDays[indexDay];
                        foundNextDay=true;
                    }
        
                    indexDay++;
                    
                }
                daysBetween=indexDay;
                //if there is no next day then stop the broadcast
            }else{
                let foundNextDay=false;
                let indexDay=(numOfDday)+1; 
                let sumDay=0;
                
                //console.log(weekDays[0].length);
                while(!foundNextDay){
                    if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                        nextRunAt= weekDays[indexDay];
                        foundNextDay=true;
                    }
                    if(indexDay==weekDays.length-1){
                        indexDay=0;
                    }else{
                        indexDay++;
                    }
                    sumDay=sumDay+1;
                }
                daysBetween=sumDay;
    
                //if there is no next day then stop the broadcast
            }
            var dateNext=dateNow.add(daysBetween,'days').tz(timezone);
            var startDay=daily_start_time;//get from setting
            var split=startDay.split(":");
            var newDate=dateNext.set({
                hours:split[0],
                minutes:split[1]
            }).format();

            return newDate;
}
exports.runnig=run;