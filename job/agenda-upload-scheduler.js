const Agenda = require('agenda');
//const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
const mongoConnectionString=process.env.MONGODB_URI;
const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});
const mongoose = require('mongoose');
const jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [];

const BroadcastCtrl=require('../controllers/broadcast-controller');
const ContactGroup=mongoose.model('BroadcastGroup');
var csv=require('csvtojson');
var parseData=require('../lib/parse-number');
var fs=require('fs');

agenda.define("upload_contact_call", async function(job,done){
    //console.log("hello wirld"+job.attrs.data.broadcast_id);
    let broadcastId=job.attrs.data.data.broadcastContactGroupId;
    let colDesc=job.attrs.data.data.colDesc;
    let filePath=job.attrs.data.data.filePath;
    let user_id=job.attrs.data.data.user_id;
    let broadcastContactGroupId=job.attrs.data.broadcastContactGroupId;
    console.log("start");
    console.log(broadcastId,'id contact');
    console.log(colDesc,'col desc');
    console.log(filePath,'file Path');
    console.log(user_id,'user id');
    //done if all message send
    var sumRow=0;
    const converter=csv({
        noheader:true,
        trim:true,
        delimiter:";",
        quote:"off"
    });
    let dataContact=[];
    let number=[];
    converter.on("data",(async (jsonObj)=>{
       
        const jsonStr= jsonObj.toString('utf8')
        const json=JSON.parse(jsonStr);
       
        try{
            let returnData
            parseData(json,"field",colDesc,async function(err,data){
                if(err){
                    throw new Error(err);
                }
                let findData=number.filter(function(val){
                    return val==data.phone;
                })

                if(findData.length==0){
                    let contact={};
                    contact.phone=data.phone;
                    contact.name=data.name;
                    contact.messageError=data.message_error;
                    contact.status="check";
                    contact.enabled=data.enabled;    
                    contact.content_data=data.content_data;
                    contact.contact_group=broadcastId;
                    contact.user_id=user_id;
                    try{
                        await BroadcastCtrl.saveContactList(contact);
                    }catch(err){
                        console.log("error when saving contact list to database");
                    }
                    number.push(data.phone);
                }

            })
        }catch(err){
            console.log(err);
        }
        // //save data to Database;

    }));

    converter.on('done',async function(err){
        if(err){

        }else{
            console.log("finish parsing the data");
        }
    })
    console.log("baca data");
    await  fs.createReadStream(filePath,{ encoding: 'utf8',highWaterMark: 1024 }).pipe(converter);
    await fs.unlinkSync(filePath);

    console.log("simpan data");

    

    dataContact=[];

    await ContactGroup.findById(broadcastId, function (err, doc) {
        if (err) console.log(err);
        doc.status = 'Contact Ready';
        doc.save(function(){
            console.log("ok");
        });
    });
    console.log("finish");
    //update 
    done();
})

agenda.on("ready", async function () {
    agenda.start();
})
module.exports = agenda;