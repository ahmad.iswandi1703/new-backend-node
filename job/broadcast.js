const Agenda = require('agenda');
const { MongoClient } = require('mongodb');

var fs=require('fs');
var parse=require('csv-parse');
var twilio = require('twilio');
var client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

var mongoose=require('mongoose');
var Subscriber=require('../model/subscriberSms.model');
var Broadcast=mongoose.models.SmsBroadcast || require('../model/smsbroadcast.model');
const moment=require('moment');
const mTimezone=require('moment-timezone');
const SettingController=require('../controllers/Sms-setting.controller');

async function run(data,broadcastid){

    // console.log(data);
    // console.log(broadcastid);
    let broadcastName="a"+broadcastid;

    const filePath=appRoot+"/contact/"+data.fileName;
    var message=data.message;
    var colDesc=data.contentDesc;

    var user_id=data.user_id;

    var settingData;
    

    const mongoConnectionString=process.env.MONGODB_URI;

    const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});
    
    agenda.define(broadcastName, async function(job,done){
        //console.log("hello wirld"+job.attrs.data.broadcast_id);
        let broadcastId=job.attrs.data.broadcast_id;
        //done if all message send
        var sumRow=0;

        //get sms setting by call 
        let setting=job.attrs.data.setting;
        console.log(setting);

        var parseCsvFile=parse({delimiter:';'}, function(err,data){
            // if(err) throw new Error('listId does not exist');
            let errorFormat="";
            try{
                sumRow=data.length;
                data.forEach(el => {
                    var dateCreate=new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Jakarta'
                      });;
                    var newMessage='';
                    var phone=el[0].trim();
                    var name='';
                    var sid='';
           
                    //add guard to check if phone number is in the right format, contains digit 0-9
                    if(matchPhoneFormat(phone)){
                        phone=phoneFormat(phone);
                        name=el[1];
                        explodeStr=message.split("[");
                        
                        for(var i=0; i< explodeStr.length;i++){
                            $col=0;
                            colDesc.forEach(dt=>{
                                regex=new RegExp(dt+"\]","g");
                                var matchCol=explodeStr[i].match(regex);
                                
                                if(matchCol!=null){
                                    //replace column with data from csv file
                                    explodeStr[i]=explodeStr[i].replace(dt+"]",el[$col]);
                                }
                                $col++;
                            })
                            newMessage+=(explodeStr[i]);
                        }

                       
                        var send=new Promise(function(resolve,reject){
                            require('./lib/' + setting.functionName)(newMessage,phone,broadcastId,setting,function(err,message){
                               if(err) reject(err);

                                //return from message is come with this data structures
                                // {
                                //     sid:"",
                                //     status:"",
                                //     price:"",
                                //     priceUnit:"",
                                // }
                                resolve(message);  
                            });
                            // sendTwilioMessage(newMessage,phone,broadcastId,function(err,message){
                            //     if(err) reject(err);
                            //     //return from message is come with this data structures
                            //     // {
                            //     //     sid:"",
                            //     //     status:"",
                            //     //     price:"",
                            //     //     priceUnit:"",
                            //     // }
                            //     resolve(message);

                            // });
                        })

                        send.then(function(message){
                            var sub=new Subscriber({
                                sid:message.sid,
                                phone:phone,
                                name:name,
                                status:message.status,
                                messsage:newMessage,
                                price:message.price,
                                priceUnit:message.priceUnit,
                                broadcast:broadcastId,
                                created_at:dateCreate,
                                updated_at:dateCreate,
                                user_id:user_id
                            })
                            sub.save(function(error){
                                if(error) throw error;   
                            });
                        }).catch(function(err){
                            //error log
                            console.log(err);
                            var sub=new Subscriber({
                                sid:"message.sid",
                                phone:phone,
                                name:name,
                                status:"failed",
                                messsage:newMessage,
                                price:0,
                                priceUnit:0,
                                broadcast:broadcastId,
                                created_at:dateCreate,
                                updated_at:dateCreate,
                                user_id:user_id
                            })
                            sub.save(function(error){
                                if(error) throw error;   
                            });
                        })

                        
                    }
                });
            }catch(er){
                
                errorFormat="error in format";
            }
            getBroadcast(broadcastId,sumRow,errorFormat);
               // sending data to twilio if twilio succes and get sid
        });


        var parseCsvFileError=parse({delimiter:';'}, function(err,data){
            // if(err) throw new Error('listId does not exist');
            let errorFormat="";
            try{
                sumRow=data.length;
                data.forEach(el => {
                    var dateCreate=new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Jakarta'
                      });;
                    var newMessage='';
                    var phone=el[0].trim();
                    var name='';
                    var sid='';
           
                    //add guard to check if phone number is in the right format, contains digit 0-9
                    if(matchPhoneFormat(phone)){
                        phone=phoneFormat(phone);
                        name=el[1];
                        explodeStr=message.split("[");
                        
                        for(var i=0; i< explodeStr.length;i++){
                            $col=0;
                            colDesc.forEach(dt=>{
                                regex=new RegExp(dt+"\]","g");
                                var matchCol=explodeStr[i].match(regex);
                                
                                if(matchCol!=null){
                                    //replace column with data from csv file
                                    explodeStr[i]=explodeStr[i].replace(dt+"]",el[$col]);
                                }
                                $col++;
                            })
                            newMessage+=(explodeStr[i]);
                        }

                       

                            //error log
                        var sub=new Subscriber({
                            sid:"message.sid",
                            phone:phone,
                            name:name,
                            status:"failed",
                            messsage:newMessage,
                            price:0,
                            priceUnit:0,
                            broadcast:broadcastId,
                            created_at:dateCreate,
                            updated_at:dateCreate,
                            user_id:user_id
                        })
                        sub.save(function(error){
                            if(error) throw error;   
                        });
                    

                        
                    }
                });
            }catch(er){
                errorFormat="error in format";
            }

            getBroadcast(broadcastId,sumRow,errorFormat);
               // sending data to twilio if twilio succes and get sid
        });

        if(setting.hasOwnProperty('gateway_name')){
            await fs.createReadStream(filePath).pipe(parseCsvFile);
        }else{
            await fs.createReadStream(filePath).pipe(parseCsvFileError);
        }
  
 
   
        
        await fs.unlinkSync(filePath);
        //update 


        done();

    })

    await new Promise(resolve => agenda.once('ready', resolve));
    try{
        settingData=await SettingController.getSmsSetting(user_id);
        console.log(settingData);
        agenda.schedule(new Date(Date.now() + 1000), broadcastName,{
            broadcast_id:broadcastid,
            setting:settingData
        });
    }catch(err){
        agenda.schedule(new Date(Date.now() + 1000), broadcastName,{
            broadcast_id:broadcastid,
            setting:{}
        });
    }

    agenda.start();
}


var getBroadcast=function(broadcast_id,sumRow,status){

    Broadcast.findById(broadcast_id,function(err,br){
        if (err) throw err;
        //edit 
        if(status=="error in format"){
            br.broadcast_status="error in format";
        }else if(status=="internal error"){
            br.broadcast_status="internal error";
        }
        else{
            br.broadcast_status="finish";
        }

        br.broadcast_subscriber=sumRow;
    
   
        br.updated_at=new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
          });
        
        br.save(function(err){
            if (err) throw err;
            console.log("success update sms broadcast");
        })
    })
}


var matchPhoneFormat=function(number){
    let match= number.match(/^[0-9]*$/g);
    return match!=null?true:false;
}

var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}


var sendTwilioMessage=function(message,phone,broadcast_id,callback){
    client.messages.create({
        body: message,
        to: phone,  // Text this number
        from: process.env.TWILIO_MESSAGING_SERVICE_SID //'+17372487799' // From a valid Twilio number
    })
    .then(message => {
    
        callback(null,message);
    },function(err){
        
        callback(err,null);
    })
    .done();
}





exports.running=run;