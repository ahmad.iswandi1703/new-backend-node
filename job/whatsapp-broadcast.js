const Agenda = require('agenda');
const { MongoClient } = require('mongodb');

var fs=require('fs');
var parse=require('csv-parse');
var twilio = require('twilio');
var client = new twilio("dfdf", "dfdf");

var mongoose=require('mongoose');
require('../model/whatsap.broadcast.subcscriber.model');
require('../model/whatsapp.broadcast.model');
require('../model/whatsap.contact-list.model');
require('../model/whatsapp.contact-group.model');

var Subscriber=mongoose.model('WhatsappSubscriber');
var Broadcast=mongoose.model('WhatsappBroadcast');
var Contact=mongoose.model('WhatsappContactList');
var Group=mongoose.model('WhatsappGroup');


async function run(data,broadcastid){

    console.log(data);

    // console.log(data);
    // console.log(broadcastid);
    let broadcastName="a"+broadcastid;

    //const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
    const mongoConnectionString=process.env.MONGODB_URI;
    const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});

    agenda.define(broadcastName, async function(job,done){
        let broadcastId=job.attrs.data.broadcast_id;
        let data=job.attrs.data.data;


        var message=data.text;
   
        //get contentDesc from 
        var contentDesc=[];
        await Group.findById(data.contact_group,function(err,res){
            if(err) throw err;
            contentDesc=res.content_desc;
        })
        var user_id=data.user_id;    

        //done if all message send
        var sumRow=0;
        let dataContact=[]
        await Contact.find({'contact_group':data.contact_group},function(err,res){
            if(err) throw err;

            dataContact=res;
        });
        sumRow=dataContact.length;
       


        dataContact.forEach(function(result){
           
            var newMessage='';
            var phone=result.phone.trim();
            var name='';
                //add guard to check if phone number is in the right format, contains digit 0-9
   
              
                name=result.name;
                //body message
                let body={};
                if(data.message_type=="Text"){
                    explodeStr=message.split("[");
                    console.log(explodeStr);
                    console.log(contentDesc);
    
          
                    var x =JSON.parse(JSON.stringify(result.content_data));
                   
                    for(var i=0; i< explodeStr.length;i++){
                        $col=0;
                        contentDesc.forEach(dt=>{
                            regex=new RegExp(dt+"\]","g");
                            var matchCol=explodeStr[i].match(regex);
                            
                    
                            if(matchCol!=null){
                                //replace column with data from csv file
                                explodeStr[i]=explodeStr[i].replace(dt+"]",x[dt]);
                            }
                            $col++;
                        })
                        newMessage+=(explodeStr[i]);
                    }

                    body={
                        body: newMessage,
                        to: "whatsapp:"+phone,  // Text this number
                        from: process.env.WHATSAPP_PHONE
                    }
                }else{
                    body={
                        body: '',
                        to: "whatsapp:"+phone,  // Text this number
                        from: process.env.WHATSAPP_PHONE,
                        mediaUrl:process.env.URL_MEDIA+data.media_url
                    }

                    if(data.media_description!==''){
                        body.body=data.media_description;   
                    }
                }

     
                sendTwilioMessage(body,function(err,message){
                    if (err) { throw err};
                    var dateCreate=new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Jakarta'
                      });;
                    var sub=new Subscriber({
                        sid:message.sid,
                        phone:phone,
                        name:name,
                        status:message.status,
                        messsage:newMessage,
                        price:message.price,
                        priceUnit:message.priceUnit,
                        broadcast:broadcastId,
                        created_at:dateCreate,
                        updated_at:dateCreate,
                        user_id:user_id
                    })
                    sub.save(function(error){
                        if(error) throw error; 

                    });
                });
            
        })

           

        //sending data to twilio if twilio succes and get sid

        //update atributes agenda to save proggress
        updateBroadcast(broadcastId,sumRow);


        //update 
        done();

    })
    await new Promise(resolve => agenda.once('ready', resolve));

    agenda.schedule(new Date(Date.now() + 1000), broadcastName,{
        broadcast_id:broadcastid,
        data:data
    });
    agenda.start();
}


var updateBroadcast=function(broadcast_id,sumRow){

    Broadcast.findById(broadcast_id,function(err,br){
        if (err) throw err;
        //edit 
        br.broadcast_status="finish";
        br.broadcast_subscriber=sumRow;
        br.updated_at=new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
          });
        
        br.save(function(err){
            if (err) throw err;
            console.log("success update sms broadcast");
        })
    })
}


var matchPhoneFormat=function(number){
    let match= number.match(/^[0-9]*$/g);
    return match!=null?true:false;
}

var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}


var sendTwilioMessage=function(body,callback){
    client.messages.create(body)
    .then(message => {
    
        callback(null,message);
    },function(err){
        callback(err,null);
    })
    .done();
}




exports.running=run;