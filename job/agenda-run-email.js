const Agenda = require('agenda');

async function run(data,setting){
    
    // console.log(data);
    // console.log(broadcastid);
    const filePath=appRoot+"/contact/"+data.file_name;    
    //const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
    const mongoConnectionString=process.env.MONGODB_URI
    const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});

    await new Promise(resolve => agenda.once('ready', resolve));
    agenda.schedule(new Date(Date.now() + 1000), "send-email-scheduler",{
        filePath:filePath,
        data:data,
        setting:setting,
    });
}

exports.running=run;