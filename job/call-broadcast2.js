const mongoose=require('mongoose');
require('../model/broadcast.contact-list.model');
require('../model/setting.call.model');
require('../model/BroadcastJobNotif');
require('../model/Broadcast.call.model');
require('../model/Broadcast.call.subscriber.model');
var ObjectId = require('mongoose').Types.ObjectId; 
var errorCode=require('../model/Model-Code-Error');

var ContactList=mongoose.model('BroadcastContactList');
var Setting=mongoose.model('CallSetting');
var JobNotif=mongoose.model('BroadcastJobNotif');
var Call=mongoose.model('BroadcastCall');
var Subscriber=mongoose.model('BroadcastSubscriber');
const axios=require('axios');



module.exports=function(agenda){
    agenda.define("Call_broadcast2",{lockLifetime: 10000},async function(job,done){
        console.log("start this call broadcast2");
    
        let dateNow=new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
        });
        //update status call to Starting if next row ==1;
        let setting=job.attrs.data.setting;
        let status=job.attrs.data.status;
        let nextRow=job.attrs.data.nextRow;
        let additionalData=job.attrs.data.additional;

        var updateCallStatus=function(broadcast,status){
            return new Promise(function(resolve,reject){
                Call.findById(broadcast,function(err,res){
                    var moment = require('moment-timezone');
                    var nDate = moment().tz('Asia/Jakarta'); 
                    res.updated_at=nDate.format();
                    res.call_status=status;
                    res.save(function(err){
                        if (err) {
                            console.log("error");
                        };
                        resolve("success");
                    })
                })
            })
        }
    

        if(nextRow==1){
            try{
                let updateBroadcastCall= await updateCallStatus(additionalData._id,"Starting");
                if(updateBroadcastCall=="successs"){
                    console.log("call "+additionalData._id+" is starting");
                }
            }catch(err){
                console.log(err);
            }
        }

        //count data call subscriber according to call broadcast id;
        var countSubscriber=new Promise(function(resolve,reject){
            Subscriber.countDocuments({broadcast:additionalData._id}).exec().then(function(res){
                resolve(res);
            }).catch(function(err){
                reject(err);
            })
        });


        //count data contact in contact list according to contact group;
        var countContactList=new Promise(function(resolve,reject){
            //set query where contact_same as broadcast._id (broadcast_group)
            ContactList.countDocuments({contact_group:additionalData.phonebook}).exec().then(function(res){
                resolve(res);
            }).catch(function(err){
                reject(err);
            })
        });

        let options={
            lean:     true,
            page:   nextRow, //initialize 
            limit: setting.concurent_limit    //set from setting 
        }
        var getDataContact= new Promise(function(resolve,reject){
            console.log("get data contact row ke"+nextRow);
                //get contact List
                ContactList.paginate({contact_group:additionalData.phonebook},options).then(function(res){
                    resolve(res);
                }).catch(function(err){
                    reject(err);
                })
        })
        

        var getNumber=function(val){
            //set to database
            //cek if number already exist in db
            broadcastId=additionalData._id;
            let returnData={status:"",number:""};
            return new Promise(resolve=>{
                Subscriber.find({broadcast:broadcastId,phone:val.phone},function(err,result){
                    if(err){
                        resolve({status:"error",number:""})
                    }
                    if(result.length==0){
                        resolve({status:"ready",number:val.phone});
                    }else{
                        resolve({status:"exist",number:""});
                    }
                })
            })
            

        }


        var insertJobNotif=function(message,code){
            return new Promise(function(resolve,reject){
                var jobWarning=new JobNotif();
                jobWarning.broadcast=additionalData._id;
                jobWarning.status="Error";
                jobWarning.message=message;
                jobWarning.updated_at=dateNow;
                jobWarning.created_at=dateNow;
                jobWarning.code=code;
                jobWarning.save(function(error){
                    if(error){
                        console.log(error);
                    }
                    resolve("success");
                });
            })
        }

        var insertSubscriber=function(subscriberData){
           return new Promise(function(resolve,reject){
               var subscriber=new Subscriber();
               subscriber.phone=subscriberData.phone;
               subscriber.created_at=dateNow
               subscriber.updated_at=dateNow
               subscriber.broadcast=additionalData._id;
               subscriber.tryCall=0;
               subscriber.status='in_queue';//(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
               subscriber.save(function(err){
                   if(err){
                       console.log(err);
                   }
                   resolve("success");
               })
           }) 
        }
        Promise.all([countSubscriber,countContactList,getDataContact]).then(async function(result){
            //check if data exist in subscriber collection
            let subscriber=result["0"];
            let contactList=result["1"];
            let contact=result["2"];
            let numberCollection=[];

            if(contactList!=0 && contact.total!=0){
                await Promise.all(contact.docs.map(async function(x){
   
                    try{
                        let number
                        try{
                           number =await getNumber(x);
                        }catch(err){
                            console.log(err,"get NUmber from subscriber");
                        }
                        if(number.status=="ready"){
                            try{

                                let insertSubcriber=await insertSubscriber({phone:x.phone});
                                numberCollection.push(x.phone);
                            }catch(err){
                                console.log(err);
                                console.log("error when insert subscriber");
                            }
                            
                        }
                    }catch(err){
                        //log error;
                        console.log(err);
                    }
                }));
                return {numbers:numberCollection,status:true}
            }else{
                status="Stop"; //cause there is no contact in list
                // log to job notif
                return {numbers:[],status:false};
            }
            
        }).then(async function(result){
            //send data to php backend
          
            if(result.status){
                let statusCall="";


                //set nexrun job;
                job.attrs.nextRunAt=settingNextrun(setting);
                //if all subscriber data has finished
                let newCountSubscriber=await Subscriber.countDocuments({broadcast:ObjectId(additionalData._id),$or:[{status:'finish'},{status:'answer'},{status:'error'},{status:'fail'}]}).exec();
                let newCountContactList= await countContactList;

                //console.log(newCountSubscriber);
                //console.log(newCountContactList);
                //set inside show channels
                var updateCallStatus=function(broadcast,status){
                    return new Promise(function(resolve,reject){
                        Call.findById(broadcast,function(err,res){
                            var moment = require('moment-timezone');
                            var nDate = moment().tz('Asia/Jakarta'); 
                            res.updated_at=nDate.format();
                            res.call_status=status;

                            res.save(function(err){
                                if (err) {
                                    console.log("error");
                                };
                                resolve("success");
                            })
                        })
                    })
                }

                console.log(newCountSubscriber);
                console.log(newCountContactList);
                if(newCountSubscriber==newCountContactList){
                    job.attrs.nextRunAt=null;
                    job.attrs.data.status="Finish";
                    // update broadcast that all number are already finished
                    try{

                        let updateBroadcastCall= await updateCallStatus(additionalData._id,"Finish");
                        if(updateBroadcastCall=="successs"){
                            console.log("call "+additionalData._id+" is finished");
                        }
                    }catch(err){
                        console.log(err);
                    }
                   
                }else{
                    //cek apakah next row ad
                    if(result.numbers.length!=0){
                        job.attrs.data.nextRow=nextRow+1;
                    }
                    
                    job.save();
                    console.log(job.attrs.data.nextRow);
                    console.log("job finish");
                    done();
                }
                //set next running if this scheduler pause or stop manually
                if(status=="Pause" || status=="Stop" || status=="Finish"){
                    // job.unique({'data.rowNumberFinish':job.attrs.data.rowNumberFinish+1, 'data.nextRow':'12', 'nextRunAt':date});
                    //job.attrs.data.rowNumberFinish=job.attrs.data.rowNumberFinish+1;
                    //set condition to the next run at
                    job.attrs.nextRunAt=null;
                }
            }else{
                console.log("Contact is not found or there is no contact");
                job.attrs.nextRunAt=null;
                job.attrs.data.status="Stop";
                try{
                    let insertJobNotifQ= await insertJobNotif("There is no contact in list",errorCode.error_call_code.CONTACT);
                    if(insertJobNotifQ=="success"){
                        console.log("insertJobNotif success");
                    }
                 }catch(err){
                     console.log("cannot write job notif")
                 }
                job.save();
                done();
            }                
        }).catch(async function(err){
            //log job Notif;
            console.log(err);
            //next run at
            job.attrs.nextRunAt=null;
            job.attrs.data.status="Stop";
            try{
                let insertJobNotif= await insertJobNotif("Call Setting Is not setting Properly",errorCode.error_call_code.SETTING);
                if(insertJobNotif=="success"){
                    console.log("insertJobNotif success");
                }
             }catch(err){
                 console.log("cannot write job notif")
             }
             job.save();
             done();
        })
        //insert call broadcast subscriber
        //let broadcastId=job.attrs.data.rowNumberFinish;
        //console.log(job.attrs.data.rowNumberFinish);
        //var job1=agenda.create('test-data-2',{rowNumberFinish:job.attrs.data.rowNumberFinish+1,nextRow:12});

        // job.repeatEvery('*/5 * * * * *', {
        //     timezone: 'Asia/Jakarta'
        // });

        //setting for ending the job

        var settingNextrun=function(setting){
            var moment = require('moment-timezone');
            var nDate = moment().tz('Asia/Jakarta'); 
            var dateNow=moment().tz('Asia/Jakarta');
            var numOfDday=dateNow.day();//sunday=0, monday=1,......, saturday=6
            var nextRun=new Date(Date.now() + 2000);
            var weekDays=setting.weekdays;

            var timeEnded=moment(setting.daily_end_time, "HH:mm").format("HH:mm");
            var timeNow=moment().tz('Asia/Jakarta').format("HH:mm");
            //alocated to another day
            if(timeNow>=timeEnded){
                //stop the job and set nextRun At to tommorrow at start the day
                let nextRunAt="";
                //it mean weeken or saturday
                let daysBetween=0;
                if(numOfDday == 6){
                    //it means saturday  add to the next day
                    let foundNextDay=false;
                    let indexDay=0; 
                    //console.log(weekDays[0].length);
                    while(!foundNextDay){
                        if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                            //check if next day is the day that forbided in weekdays
                            nextRunAt= weekDays[indexDay];
                            foundNextDay=true;
                        }
        
                        indexDay++;
                    }
                    daysBetween=indexDay;
                    //if there is no next day then stop the broadcast
                }else{
                    let foundNextDay=false;
                    let indexDay=(numOfDday)+1; 
                    let sumDay=0;
                  
                    //console.log(weekDays[0].length);
                    while(!foundNextDay){
                        if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                            nextRunAt= weekDays[indexDay];
                            foundNextDay=true;
                        }
                        if(indexDay==weekDays.length-1){
                            indexDay=0;
                        }else{
                            indexDay++;
                        }
                        sumDay=sumDay+1;
                    }
                    daysBetween=sumDay;

                    //if there is no next day then stop the broadcast
                }
            
                var dateNext=dateNow.add(daysBetween,'days').tz('Asia/Jakarta');
                var startDay=setting.daily_start_time;//get from setting
                var split=startDay.split(":");
                var newDate=dateNext.set({
                    hours:split[0],
                    minutes:split[1]
                }).format();
                nextRun=newDate;

            }else{
                //lewat 1 menit, Allocated to nex schedule minute
                var nextSchedule=setting.schedule_minute_per;
                nextRun=dateNow.add(nextSchedule,'minutes').format();
            }

            return nextRun;
        }

        // if(countSubscriber==countContactList){
        //     job.attrs.nextRunAt=null;
        // }
        //set job atributs for next row 

    })
}

