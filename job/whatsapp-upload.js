const Agenda = require('agenda');
const { MongoClient } = require('mongodb');

var fs=require('fs');
var parse=require('csv-parse');
var twilio = require('twilio');
var client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

var mongoose=require('mongoose');
const moment=require('moment');
const mTimezone=require('moment-timezone');
const WhatsappCtrl=require('../controllers/whatsapp-controller');


async function run(data,whatsappContactGroupId){

    // console.log(data);
    // console.log(broadcastid);
    
    let jobName="a"+whatsappContactGroupId;
    var colDesc=data.content_desc;
    console.log(colDesc);
    const filePath=appRoot+"/contact/"+data.file_name;

    var user_id=data.user_id;
    
    //const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
    const mongoConnectionString=process.env.MONGODB_URI;
    const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});

    agenda.define(jobName, async function(job,done){
        //console.log("hello wirld"+job.attrs.data.broadcast_id);
        let broadcastId=job.attrs.data.whatsappContactGroupId;
        //done if all message send
        var sumRow=0;
        //cek if file using , or ;

        var parseCsvFile=parse({delimiter:';'}, function(err,data){
            sumRow=data.length;
       
            data.forEach(el => {
                let contact={};
                var phone=el[0].trim();
                var name=el[1];
                var enabled=false;
                var messageError="valid";
                var status='check';
                var content_data={};
                
                //add guard to check if phone number is in the right format, contains digit 0-9
                if(matchPhoneFormat(phone)){
                    phone=phoneFormat(phone);
                
                    if(el.length!=colDesc.length){
                        enabled=false;
                        messageError="content description is not the same";
                    }else{
                        $col=0;
                        enabled=true;
                        
                        colDesc.forEach(dt=>{
                            content_data[dt]=el[$col];
                            $col++;
                        })
                    }
                }else{
                    enabled=false;
                    messageError="phone number is not valid";
                }

                contact.phone=phone;
                contact.name=name;
                contact.messageError=messageError;
                contact.status=status;
                contact.enabled=enabled;    
                contact.content_data=content_data;
                contact.contact_group=whatsappContactGroupId;
                contact.user_id=user_id;
            
                try{
                    WhatsappCtrl.saveContactList(contact);
                }catch{
                    console.log("error input contact list");
                    //loging to database;
                }
                
            });

        });

        await fs.createReadStream(filePath).pipe(parseCsvFile);
        await fs.unlinkSync(filePath);
        //update 
        done();

    })

    await new Promise(resolve => agenda.once('ready', resolve));

    agenda.schedule(new Date(Date.now() + 1000), jobName,{
        whatsappContactGroupId:whatsappContactGroupId
    });
    agenda.start();
}





var matchPhoneFormat=function(number){
    let match= number.match(/^[0-9]*$/g);
    return match!=null?true:false;
}

var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}



exports.running=run;