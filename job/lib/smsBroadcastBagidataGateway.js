let axios=require('axios');

module.exports=async function(message,phone,broadcast_id,setting,callback){

    //action to send message to bagidata gateway;
    console.log("right here bagidata");

    let url=setting.gateway_url || process.env.BAGIDATA_URL ;
    let token=setting.gateway_token || process.env.BAGIDATA_TOKEN;
    let secret=setting.gateway_secret || process.env.BAGIDATA_SECRET;
    let data={
        "action":"send",
        "nohp":phone,
        "text":message
    }
    try{
        await axios.post(url, data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization':'{"token":"'+token+'","secret":"'+secret+'"}',
            }
        }).then(val=>{
      
            let data={
                sid:"defaultError",
                status:"failed",
                price:0,
                priceUnit:0
            }

            if(!val.data.success){
              if(val.data.msg=="failed"){
                  data.sid=val.data.detail.id_sms
                  data.status=val.data.msg;
                  data.price=0;
                  data.priceUnit=0;
              }  
            }else{
                //set status to delivered
                if(val.msg="ok"){
                    data.sid=val.data.detail.id_sms
                    data.status="delivered";
                    data.price=0;
                    data.priceUnit=0;
                }
            }
            callback(null,data);
            console.log(val);
        }).catch(err=>{
            callback(err,null);
            console.log(val);
        })
    }catch(err){
        callback(err,null);
    }

}