const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');

let SmsBroadcastSchema= new Schema({
   
    broadcast_name:String,
    broadcast_status:String,
    broadcast_date:Date,
    broadcast_subscriber:Number,
    error_number:Number,
    created_at:Date,
    updated_at:Date,
    content_desc:[{type:String}],
    user_id:String
});

SmsBroadcastSchema.plugin(paginate);

module.exports=mongoose.model('SmsBroadcast',SmsBroadcastSchema);