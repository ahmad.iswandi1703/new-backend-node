const mongoose=require('mongoose');
const Schema=mongoose.Schema;

//this for setting one page call and monitoring
// how many call can make in one campaign
// ivr to use in call
// determine ivr_menu_options or result to view to user
// status of setting
let OcallSetting= new Schema({
    ivr_survey_id:String,
    ivr_survey_name:String,
    ivr_menu_options:[String],
    call_per_campaign:Number,
    status:String, //active, not active
    user_id:String,
    created_at:Date,
    updated_at:Date
});


mongoose.model('OcallMonSetting',OcallSetting);