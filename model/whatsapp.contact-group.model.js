const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');

let whatsappContactGroupSchema= new Schema({
   
    contact_group_name:String,
    contact_group_file:String,
    content_desc:[String],
    created_at:Date,
    updated_at:Date,
    user_id:String
});

whatsappContactGroupSchema.plugin(paginate);

mongoose.model('WhatsappGroup',whatsappContactGroupSchema);