const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let BroadcastSubscriberSchema= new Schema({
    phone:String,
    created_at:Date,
    updated_at:Date,
    uuid:String,
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastCall',
        require:true
    },
    call_setting:String,
    status:String, //(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
    tryCall:Number,
    data:Map
});
BroadcastSubscriberSchema.plugin(paginate);
mongoose.model('BroadcastSubscriber',BroadcastSubscriberSchema);