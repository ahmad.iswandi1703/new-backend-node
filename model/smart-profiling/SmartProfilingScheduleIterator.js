const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let SmartProfilingScheduleIteratorSchema= new Schema({
    schedule_date:Date,
    schedule_start_time:{
        hours:{ type: Number },
        minutes:{type:Number}
    },
    schedule_end_time:{
        hours:{ type: Number },
        minutes:{type:Number}
    },
    schedule_running_time_in:String,
    schedule_timezone:String,
    scheduler_order:Number,
    schedule_status:String,
    created_at:Date,
    updated_at:Date
});

SmartProfilingScheduleIteratorSchema.plugin(paginate);
mongoose.model('SmartProfillingScheduleIterator',SmartProfilingScheduleIteratorSchema);