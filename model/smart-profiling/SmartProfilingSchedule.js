const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let SmartProfilingScheduleSchema= new Schema({
    schedule_date_from:{
        date:{ type: Number },
        month:{type:Number},
        year:{type:Number}
    },
    schedule_date_to:{
        date:{ type: Number },
        month:{type:Number},
        year:{type:Number} 
    },
    schedule_start_time:{
        hours:{ type: Number },
        minutes:{type:Number}
    },
    schedule_end_time:{
        hours:{ type: Number },
        minutes:{type:Number}
    },
    schedule_timezone:String, //wib, wita, wit.. Asia/Jakarta, Asia/Jayapura, Asia/Makassar
    scheduler_order:Number,
    schedule_status:String,
    profiling:{
        type:Schema.Types.ObjectId,
        ref:'SmartProfilling',
        required:true
    },
    created_at:Date,
    updated_at:Date
});

SmartProfilingScheduleSchema.plugin(paginate);
mongoose.model('SmartProfillingSchedule',SmartProfilingScheduleSchema);