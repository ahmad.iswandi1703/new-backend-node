const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let SmartProfilingSchema= new Schema({
    uid:String,
    campaign_name:String,
    campaign_type:String,
    general_setting:[Map], //
    filter:[Map],
    //[{id:value_id,title:value_title,order:value_order},{},{},{}]
    // location:[Map],
    // //[{id:value_id,title:value_title,order:value_order},{},{},{}]
    // behavior:[Map],
    // //[{id:value_id,title:value_title,order:value_order},{},{},{}]
    // demographic:[Map],
    // //[{id:value_id,title:value_title,order:value_order},{},{},{}]
    // product_subscription:[Map],
    //[{id:value_id,title:value_title,order:value_order},{},{},{}]
    status:String,
    total_prospect:Number,
    //draft, active, completed
    user:{
        type:Schema.Types.ObjectId,
        ref:'User',
        required:true
    },
    created_at:String,
    //[{id:value_id,title:value_title,order:value_order},{},{},{}]
    updated_at:String
    //[{id:value_id,title:value_title,order:value_order},{},{},{}]
});

SmartProfilingSchema.plugin(paginate);
mongoose.model('SmartProfilling',SmartProfilingSchema);