const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let BroadcastCall= new Schema({
    uid:String,
    presence_id:String,
    campaign_name:String,
    account_code:String,
    application:String,
    call_setting:{
        type:Schema.Types.ObjectId,
        ref:'CallSetting',
        required:true
    },
    call_status:String, //running, pause, finish
    number:[String],
    user_id:String,
    created_at:Date,
    updated_at:Date
});

BroadcastCall.plugin(paginate);
mongoose.model('OCallMon',BroadcastCall);