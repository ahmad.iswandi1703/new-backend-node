const mongoose=require('mongoose');
const Schema=mongoose.Schema;


//consumer is high order of bussiness tree (Jakarta pusat)

//TREG is second order of bussines tree;

//witel is third order of bussinse tree;

let Office= new Schema({
    name:String,
    address:String,
    phone_number:String,
    whatsapp:String,
    parents:{
        type:Schema.Types.ObjectId,
        ref:'Office',
        require:false
    },
    created_at:Date,
    updated_at:Date
});
//office model has branching

//create office 
//-- consumer
//-- TREG


mongoose.model('Office',Office);