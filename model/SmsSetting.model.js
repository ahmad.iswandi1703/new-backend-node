const mongoose=require('mongoose');
const Schema=mongoose.Schema;

let SmsSetting= new Schema({
    gateway_name:String,
    gateway_url:String,
    gateway_token:String,
    gateway_secret:String,
    gateway_number:String,
    gateway_specific_sid:String,
    status:String,
    functionName:String,
    type:String, /// all user (default) specific user if all user
    user_id:String,
    created_by:String,
    created_at:Date,
    updated_at:Date
});

mongoose.model('SmsSetting',SmsSetting);