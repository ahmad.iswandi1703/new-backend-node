const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
var paginate=require('mongoose-paginate');
const { Schema } = mongoose;

require('./Office.model');
const Office=mongoose.model('Office');
//create user with hierarchy

// OFFICE (TREG 1, TREG 2, TREG 3, TREG 4)
// BRANCH (WITEL JAKPUS, WITEL JAKBAR) 
// TREG (TELKOM REGIONAL) -> HAS WITEL
// -- WITEL (JAKPUS, JAKBAR, JAKPUS)

// office model
// -- name
// -- created_at
// -- address
// -- phone number
// -- whatsapp

// TYPE
// -- SUPER ROOT (admin that manage all activities in system for OCA admin only);
// -- ADMIN ROOT (admin that manage all activities in system) Consumer
// -- USER (user biasa)


// model menu
// -- menu_name
// -- menu_link
// -- menu_icon
// -- menu_

// user_privelage
// -- user_type;
// -- privelage_name;
// -- menu_id:Array;

//menu privelage
//privelage_id
//menu_id
// user_privelage_menu


const UsersSchema = new Schema({
  branch_name:String,
  email: String,
  user_type:String,//
  user_privelage: String, // privelage id
  office: {
    type:Schema.Types.ObjectId,
    ref:'Office',
    require:false
  },
  hash: String,
  salt: String,
  created_at:Date,
  updated_at:Date,
});
UsersSchema.plugin(paginate);

UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};


UsersSchema.methods.validatePassword = function(password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UsersSchema.methods.generateJWT = function(expiredTime) {


  return jwt.sign({
    email: this.email,
    id: this._id,
   // exp: parseInt(expirationDate.getTime() / 1000, 10),
    exp:expiredTime
  }, 'secret');
}



UsersSchema.methods.toAuthJSON =function() {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
    let time=Math.floor(Date.now() / 1000) + (60 * 60);
  return {
    _id: this._id,
    email: this.email,
    user_type:this.user_type,
    office_id:this.office._id,
    expired_at:time,
    token: this.generateJWT(time),
  };
};

mongoose.model('User', UsersSchema);