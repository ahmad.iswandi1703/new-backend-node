const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');

let CallSetting= new Schema({
    caller_id_name:String,
    caller_id_number:String,
    dialing_timeout:Number,
    max_call_duration:Number,
    concurent_limit:Number,//default 10
    retries_call:Number,
    retries_call_between:Number,//in minutes;
    daily_start_time:String,
    daily_end_time:String,
    schedule_minute_per:Number,
    status:String,
    weekdays:[String],
    type:String, /// all user (default) specific user if all user 
    user_id:String,
    updated_id:String,
    created_at:Date,
    updated_at:Date,
    timezone:String,
    gateway_id:String, //get from v_gateway from postgredb,
    setting_name:String
});
CallSetting.plugin(paginate);
mongoose.model('CallSetting',CallSetting);