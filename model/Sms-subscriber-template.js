const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');


let SmsTemplateSubscriber=new Schema({
    user: {
        type:Schema.Types.ObjectId,
        ref:'User',
        require:false
    },
    template:{
        type:Schema.Types.ObjectId,
        ref:'SmsTemplate',
        require:true
    },
    created_at:Date,
    updated_at:Date,

})

mongoose.model('SmsTemplateSubscriber',SmsTemplateSubscriber);