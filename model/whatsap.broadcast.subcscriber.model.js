const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let SubscriberBroadcastSchema= new Schema({
 
    sid:String,
    phone:String,
    name:String,
    messsage:String,
    status:String,
    price:Number,
    priceUnit:String,
    created_at:Date,
    updated_at:Date,
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'WhatsappBroadcast',
        require:true
    },
    user_id:String
});
SubscriberBroadcastSchema.plugin(paginate);
module.exports=mongoose.model('WhatsappSubscriber',SubscriberBroadcastSchema);