const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');


let EmailTemplateSubscriber=new Schema({
    user: {
        type:Schema.Types.ObjectId,
        ref:'User',
        require:false
    },
    template:{
        type:Schema.Types.ObjectId,
        ref:'EmailTemplate',
        require:true
    },
    created_at:Date,
    updated_at:Date,

})

mongoose.model('EmailTemplateSubscriber',EmailTemplateSubscriber);