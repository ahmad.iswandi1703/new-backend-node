const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let EmailBroadcastSchema= new Schema({
    email:String,
    name:String,
    messsage:String,
    status:String,
    created_at:Date,
    updated_at:Date,
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'EmailBroadcast',
        require:true
    },
    user_id:String
});

EmailBroadcastSchema.plugin(paginate);
mongoose.model('EmailSubscriber',EmailBroadcastSchema);

//change model