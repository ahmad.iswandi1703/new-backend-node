const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');

let EmailBroadcast= new Schema({
    broadcast_name:String,
    broadcast_status:String,
    broadcast_date:Date,
    broadcast_subscriber:Number,
    created_at:Date,
    updated_at:Date,
    file_name:String,
    content_desc:[{type:String}],
    content_message:String,
    content_from:String,
    content_subject:String,
    email_template:String,
    user_id:String
});

EmailBroadcast.plugin(paginate);
mongoose.model('EmailBroadcast',EmailBroadcast);


