const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let BroadcastContactList= new Schema({

    phone:String,
    name:String,
    messageError:String,
    status:String, //not whatsapp number, or anything
    enabled:Boolean,
    created_at:Date,
    updated_at:Date,
    contact_group:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastGroup',
        require:true
    },
    content_data:Map,
    user_id:String
});
BroadcastContactList.plugin(paginate);


BroadcastContactList.index({ contact_group: 1 });
mongoose.model('BroadcastContactList',BroadcastContactList);