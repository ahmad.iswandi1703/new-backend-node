const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');


let WhatsappTemplateSubscriber=new Schema({
    user: {
        type:Schema.Types.ObjectId,
        ref:'User',
        require:false
    },
    template:{
        type:Schema.Types.ObjectId,
        ref:'WhatsappTemplate',
        require:true
    },
    created_at:Date,
    updated_at:Date,

})

mongoose.model('WhatsappTemplateSubscriber',WhatsappTemplateSubscriber);