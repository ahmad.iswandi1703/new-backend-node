const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');


let WhatsappTemplate=new Schema({
    template_name:String,
    template_code_id:String,
    template_variable:[String],
    template_message:String,
    created_by:String,
    created_at:Date,
    updated_at:Date,
})

WhatsappTemplate.plugin(paginate);
mongoose.model('WhatsappTemplate',WhatsappTemplate);