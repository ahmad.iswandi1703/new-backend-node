const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');
let WhatsappBillingHistorySchema= new Schema({
    user_id:String,
    phone:String,
    sid:String,
    status:String, //"failed/success"
    created_at:Date,
    updated_at:Date,
    message:String,
    total_character:Number,
});

WhatsappBillingHistorySchema.plugin(paginate);
mongoose.model('WhatsappBillingHistory',WhatsappBillingHistorySchema);