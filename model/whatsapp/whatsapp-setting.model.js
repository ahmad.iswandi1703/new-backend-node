const mongoose=require('mongoose');
const Schema=mongoose.Schema;

let WhatsappSetting= new Schema({
    gateway_name:String,
    gateway_host:String,
    gateway_key:String,
    gateway_secret:String,
    gateway_number:String,
    gateway_app_sid:String,
    gateway_private_key_path:String,
    status:String,
    function_name:String,
    type:String, /// all user (default) specific user if all user
    user_id:String,
    created_by:String,
    created_at:Date,
    updated_at:Date
});

mongoose.model('WhatsappSetting',WhatsappSetting);