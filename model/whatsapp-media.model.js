const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let WhatsapMedia= new Schema({
    media_name:String,
    file_name:String,
    created_at:Date,
    updated_at:Date,
    user_id:String
});
WhatsapMedia.plugin(paginate);
module.exports=mongoose.model('WhatsappMedia',WhatsapMedia);