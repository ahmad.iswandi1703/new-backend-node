const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');


let SmsTemplate=new Schema({
    template_name:String,
    template_variable:[String],
    template_message:String,
    created_by:String,
    created_at:Date,
    updated_at:Date,
})
SmsTemplate.plugin(paginate);
mongoose.model('SmsTemplate',SmsTemplate);