const mongoose=require('mongoose');
const Schema=mongoose.Schema;

let SmsBilling= new Schema({
    character:{
        tipe:{ type: String, lowercase: true, trim: true }, //limited,unlimited
        length:{type:Number}
    },
    quota:{
        tipe:{type:String}, //limited, unlimited
        token:{type:Number}
    },
    expired:{
        tipe:{type:String}, //limited, unlimited
        date:{type:Date}
    },
    status:String, //enabled,disabled
    type:String, /// all user (default) specific user if all user (global, user);
    user:{
        type:Schema.Types.ObjectId,
        ref:'User',
        require:false
    },
    effective_date:Date,
    created_by:String,
    created_at:Date,
    updated_at:Date
});

mongoose.model('SmsBilling',SmsBilling);