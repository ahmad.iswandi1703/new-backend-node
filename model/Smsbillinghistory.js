const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');
let SmsBillingHistorySchema= new Schema({
    user_id:String,
    phone:String,
    sid:String,
    status:String, //"failed/success"
    created_at:Date,
    updated_at:Date,
    message:String,
    total_character:Number,
});

SmsBillingHistorySchema.plugin(paginate);
mongoose.model('SmsBillingHistory',SmsBillingHistorySchema);