var fs=require('fs');

var csv=require('csvtojson');
//create phone number
//match phone format
var parseData=function(row,nameFieldIdentifier,contentDesc,callback){

    let cDLength=0;
    
    let returnData={
        success:"",//failed or success parse row
        content_data:"",
        email:"",
        name:"",
        message_error:""
    }
    if(Array.isArray(contentDesc)){
        cDLength=contentDesc.length;
    }
    
    let contentData={};
    for(let i=0; i<cDLength; i++){
        let field=nameFieldIdentifier+(i+1);
        if(row.hasOwnProperty(field)){
            contentData[contentDesc[i]]=row[field];
        }else{
            contentData[contentDesc[i]]="";
        }
    }

    if(typeof row != "object"){
        returnData.success=false;
        returnData.message_error="object row is not defined";
        callback(returnData.message_error,null);
    }

    let iField=nameFieldIdentifier;
    //konten Default
    let email=row.hasOwnProperty(iField+"1")?row[iField+"1"].trim():"";
    let name=row.hasOwnProperty(iField+"2")?row[iField+"2"]:"";

    if(matchEmailFormat(email)){


        returnData.success=true;
        returnData.content_data=contentData;
        returnData.phone=email;
        returnData.name=name;
        returnData.message_error="valid";

        callback(null,returnData);
    }else{
        //contact broken
        returnData.success=false;
        returnData.message_error="error format in email";
        callback(returnData.message_error,null);
    }

    

}

//generate email body
var emailBodyParse=function(contentDesc,text,element){
    let newMessage="";
    explodeStr=text.split("["); 
        
    let status="passess";        
    for(var i=0; i< explodeStr.length;i++){
        $col=0;
        contentDesc.forEach(dt=>{
            regex=new RegExp(dt+"\]","g");
            var matchCol=explodeStr[i].match(regex);
            
            if(matchCol!=null){
                //replace column with data from csv file

                if(typeof element[dt] === 'undefined') {
                    status="failed";
                    explodeStr[i]=explodeStr[i].replace(dt+"]",'undefined');
                }else{
                    explodeStr[i]=explodeStr[i].replace(dt+"]",element[dt]);
                }
                    // color is undefined)
            }
            $col++;
        })
        newMessage+=(explodeStr[i]);
    }



    return {status:status,message:newMessage};

}
var matchEmailFormat=function(email){
    let match= email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/g);
    return match!=null?true:false;
}

//module.exports=parseData;
module.exports={
    parseData:parseData,
    bodyParse:emailBodyParse
}

emailBodyParse(["Email","Name"],"<h1>[Email]</h2> Selamat datang <b>[Name]</b>",{Esmail:"ahmad.iswandi1703@gmail.com",Name:"Ahmad"})