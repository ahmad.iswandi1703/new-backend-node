const moment=require('moment-timezone');
const _=require('lodash');
/**
 * 
 * @param {Object} schedule 
 * Object scheduleData :
    schedule_date_from:{
        date:{ type: Number },
        month:{type:Number},
        year:{type:Number}
    },
    schedule_date_to:{
        date:{ type: Number },
        month:{type:Number},
        year:{type:Number} 
    },
    schedule_start_time:{
        hours:{ type: Number },
        minutes:{type:Number}
    },
    schedule_end_time:{
        hours:{ type: Number },
        minutes:{type:Number}
    },
    schedule_timezone:String, //wib, wita, wit.. Asia/Jakarta, Asia/Jayapura, Asia/Makassar
    scheduler_order:Number,
    schedule_status:String,
    profiling:{
        type:Schema.Types.ObjectId,
        ref:'SmartProfilling',
        required:true
    },
    created_at:Date,
    updated_at:Date

 * it will generate schedule date collection from date begining to date to for the ending of date
 * ex: generate date from 2019-06-10 until 2019-06-12 it will generate iteration (2019-06-10,2019-06-11,2019-06-12);
 * Output / Return value
 * Array Object
 *  [{  date:"",
        startTime:"",
        endTime:"",
        timezone:""
    }]
 */
function parseScheduler(schedule){
    let schdFromDate=schedule.schedule_date_from;
    let schdToDate=schedule.schedule_date_to;

    let timezone=schedule.schedule_timezone;
    switch (true) {
        case  timezone=="wib" || timezone=="WIB":
            timezone='Asia/Jakarta';
            break;
        case timezone=="wit" || timezone=="WIT":
            timezone='Asia/Jayapura';
            break;
        case timezone=="wita" || timezone=="WITA":
            timezone='Asia/Makassar';
            break; 
        
        default:
            break;
    }


    let timeStart=schedule.schedule_start_time;
    let timeEnd=schedule.schedule_end_time;
    //check if sched to Date null then set to from date;
    if((schdToDate.date=="" || schdToDate.date==null || schdToDate.date==undefined)){
        schdToDate=schdFromDate;
    }

    let dFrom=moment(new Date(parseInt(schdFromDate.year),parseInt(schdFromDate.month)-1,parseInt(schdFromDate.date))).tz(timezone);
    let dTo=moment(new Date(parseInt(schdToDate.year),parseInt(schdToDate.month)-1,parseInt(schdToDate.date))).tz(timezone);
    let daysDiff=dTo.diff(dFrom,'days');

    let dateCollection=[];
    let newDate=dFrom;
    for(let i=0; i<=daysDiff; i++){
        
        let dateData={
            date:"",
            startTime:"",
            endTime:"",
            timezone:""
        }
        dateData.startTime=timeStart;
        dateData.endTime=timeEnd;
        dateData.timezone=timezone;
        if(i>0){
            let nDateFrom=newDate.add(1,'days');
            dateData.date=nDateFrom.format();
            dateCollection.push(dateData);
            newDate=null;
            newDate=nDateFrom;
        }else{
            dateData.date=dFrom.format();
            dateCollection.push(dateData);
        }
       
    }
    return dateCollection;
}

/**
 * 
 * @param {Object} dateCollection The date collection (it is Object Array)
 * @param {Object} newDateCollection The New Date collection (it will be compared with date collection and validate and then merge the date with date collection)
 * Array Object
 *  [{  date:"",
        startTime:"",
        endTime:"",
        timezone:""
    }]
 * Output / Return Value
 * concat date collection with newDateCollection that already validated
 */
function validateScheduler(dateCollection, newDateCollection){
    //new Date collection
    let joinDate=[];
    _.each(newDateCollection,function(nDate,index,nDates){
        let dateFound=_.find(dateCollection,function(o){
            return o.date==nDate.date && o.timezone==nDate.timezone;
        })
        let newStartTime=moment.utc(nDate.startTime.hour+':'+nDate.startTime.minute,'hh:mm').format('hh:mm');
        let newEndTime=moment.utc(nDate.endTime.hour+':'+nDate.endTime.minute,'hh:mm').format('hh:mm');
        //this status date will be given true value if it passed the characteristic of start and end time handler below
        let statusDate=true;
        //characteristic handler
       
        if(dateFound){
            console.log('here1');
            let collStartTime=moment.utc(dateFound.startTime.hour+':'+dateFound.startTime.minute,'hh:mm').format('hh:mm');
            let collEndTime=moment.utc(dateFound.endTime.hour+':'+dateFound.endTime.minute,'hh:mm').format('hh:mm')

            //check the criteria to match 
            /**
             * check the data 
             */
            //result 
            /**
             * each date scheduler cannot have same criteria like same timezone, range start time and end time
             */
            //check start and end time in date and compare 
            if(newStartTime < collStartTime){
                console.log('here2.1');
                if(newEndTime>collStartTime){
                    console.log('here2.2');
                    statusDate=false;
                }
            }else if(newStartTime > collStartTime ){
                console.log('here3.1');
                //check the end time if in range of collection start time and end time
                if(newEndTime<=collEndTime){
                    console.log('here3.2')
                    statusDate=false;
                }

                if(newStartTime < collEndTime){
                    statusDate=false;
                    console.log('here3.3');
                }
            }else if(newStartTime == collStartTime){
                console.log('here4.1');
                statusDate=false;
            }
        }

        if(statusDate){
            joinDate.push(nDate);
        }
    });

    //let newResult=_.concat(dateCollection,joinDate);
    return joinDate;
}

/**
 * 
 * @param {Object} collection 
 * @param {Array} propertyFilter ['date','time.start']
 * @param {String} sort asc/desc;
 */
function sorting_collection(collection, propertyFilter, sort){
    //let sorting=_.orderBy(returnDate,['date','startTime.hour','startTime.minute'],['asc']);
    let sorting=_.orderBy(collection,propertyFilter,[sort]);

    return sorting;
}

module.exports={
    parseScheduler:parseScheduler,
    validateScheduler:validateScheduler,
    sorting:sorting_collection
}